<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Location\Location */

$this->title = Yii::t("app", 'Creează locaţie');
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t("app", 'Locaţii'),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="location-create">

	<h1><?= Html::encode($this->title) ?></h1>

    <?=$this->render ( '_form', [ 'model' => $model,'langsModels' => $langsModels,'imagesModels' => $imagesModels,'locationCategoryModels' => $locationCategoryModels,'categoryModels' => $categoryModels ] )?>

</div>
