<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Location\Location */

$this->title = $model->ID;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Locaţii' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="location-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
        <?= Html::a(Yii::t('app', 'Editează'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?=Html::a ( Yii::t ( 'app', 'Șterge' ), [ 'delete','id' => $model->ID ], [ 'class' => 'btn btn-danger','data' => [ 'confirm' => Yii::t ( 'app', 'Sunteți sigur că doriți să ștergeți această locaţie?' ),'method' => 'post' ] ] )?>
    </p>

    <?=DetailView::widget ( [ 'model' => $model,'attributes' => [ 'ID','CountryID','CountryRegionID' ] ] )?>

</div>
