<?php
use yii\bootstrap\Tabs;
use kartik\select2\Select2;
use app\models\Country\Country;
use kartik\depdrop\DepDrop;
use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;

?>
<br />

<div class="row">
	<div class="col-md-6">
        <?=$form->field ( $model, 'CountryID' )->widget ( Select2::className (), [ 'data' => Country::listForDropdown (),'options' => [ 'id' => 'country-id' ] ] );?>
        <?=$form->field ( $model, 'CountryRegionID' )->widget ( DepDrop::classname (), [ 'options' => [ 'id' => 'subcat-id' ],'data' => ArrayHelper::map ( ( array ) $model->countryRegion, 'ID', 'countryRegionLangs.Name' ),'type' => DepDrop::TYPE_SELECT2,'pluginOptions' => [ 'depends' => [ 'country-id' ],'placeholder' => Yii::t('app', 'Selectează...'),'url' => Url::to ( [ '/admin/location/location/region-list' ] ) ] ] );?>
        <?= $form->field($model, 'Link')->textInput(); ?>
    </div>
	<div class="col-md-5 col-md-offset-1">
        <?= Html::label(Yii::t("app", "Categorii")) ?>
        <?=Html::checkboxList ( 'LocationCategories[]', ArrayHelper::map ( $locationCategoryModels, 'CategoryID', 'CategoryID' ), ArrayHelper::map ( $categoryModels, 'ID', 'currentLang.Title' ), [ 'class' => 'checkbox','separator' => '<br>' ] );?>
        <?= $form->field($model, 'Price')->textInput(['type' => 'number']); ?>
    </div>
</div>

<br />

<?php foreach ($langsModels as $key => $lmodel) { ?>
    <?php
	
$items [] = [ 
			'label' => strtoupper ( Yii::$app->params ['siteLanguages'] [$key] ),
			'content' => $this->render ( '_lang_form', [ 
					'lmodel' => $lmodel,
					'form' => $form,
					'key' => $key 
			] ),
			'active' => $key == 0 
	];
	?>
<?php } ?>

<?=Tabs::widget ( [ 'items' => $items ] )?>