<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Location\Location */

$this->title = Yii::t("app", 'Editează locaţie').': ' . reset ( $langsModels )->Title;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t("app", 'Locaţii'),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = [ 
		'label' => reset ( $langsModels )->Title 
];
?>
<div class="location-update">

	<h1><?= Html::encode($this->title) ?></h1>

    <?=$this->render ( '_form', [ 'model' => $model,'langsModels' => $langsModels,'imagesModels' => $imagesModels,'locationCategoryModels' => $locationCategoryModels,'categoryModels' => $categoryModels ] )?>

</div>
