<?php
use kartik\widgets\FileInput;
use yii\helpers\Url;

$initialPreview = [ ];
$initialPreviewConfig = [ ];
foreach ( $imagesModels as $imageModel ) {
	$initialPreview [] = Url::to ( '/uploads/location/' . $imageModel->image->Thumb, true );
	$initialPreviewConfig [] = [ 
			'caption' => '&nbsp;',
			'size' => '&nbsp;',
			'url' => Url::to ( [ 
					'/admin/location/location/delete-image' 
			] ),
			'key' => $imageModel->ID 
	];
}

?>

<br />

<?=FileInput::widget ( [ 'name' => 'Images[]','options' => [ 'multiple' => true,'accept' => '.png,.jpg,.jpeg' ],'pluginOptions' => [ 'initialPreview' => $initialPreview,'initialPreviewAsData' => true,'initialPreviewConfig' => $initialPreviewConfig,'overwriteInitial' => false,'showRemove' => true,'showUpload' => false,'otherActionButtons' => '<button type="button" class="btn btn-sm btn-default" data-url="test" {dataKey} title="Set main"><i class="glyphicon glyphicon-star text-warning"></i></button>' ],'pluginEvents' => [ 'filesorted' => new yii\web\JsExpression ( "function(event, params) {
            var data = [];
            $.each(params.stack, function(key, val){
                data.push(val.key);
            });
            $.post('" . Url::to ( [ 'image-sort' ] ) . "', {order: data, locationID: " . ( int ) $model->ID . "});
        }" ) ] ] );?>

<br />