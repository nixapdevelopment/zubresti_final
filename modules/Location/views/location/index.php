<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Location\LocationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t ( 'app', 'Locaţii' );
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="location-index">

	<h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Creează locaţie'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?=GridView::widget ( [ 'dataProvider' => $dataProvider,// 'filterModel' => $searchModel,'columns' => [ [ 'class' => 'yii\grid\SerialColumn' ],[ 'label' => 'Name','value' => function ($model) {return reset ( $model->locationLangs )->Title;} ],[ 'label' => 'Country','value' => function ($model) {return reset ( $model->country->countryLangs )->Name;} ],[ 'label' => 'Region','value' => function ($model) {$region = $model->countryRegion->countryRegionLangs;return empty ( $region ) ? null : $region->Name;} ],[ 'class' => 'yii\grid\ActionColumn','options' => [ 'width' => '65px' ],'template' => '{update}&nbsp;&nbsp;&nbsp;&nbsp;{delete}' ] ] ] );?>
<?php Pjax::end(); ?></div>
