<?php

namespace app\modules\Location\controllers;

use Yii;
use app\models\Location\Location;
use app\models\Location\LocationSearch;
use app\models\LocationLang\LocationLang;
use app\models\LocationImage\LocationImage;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\controllers\BackendController;
use yii\base\Model;
use app\models\Image\Image;
use app\models\LocationCategory\LocationCategory;

/**
 * LocationController implements the CRUD actions for Location model.
 */
class LocationController extends BackendController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [ 
				'verbs' => [ 
						'class' => VerbFilter::className (),
						'actions' => [ 
								'delete' => [ 
										'POST' 
								] 
						] 
				] 
		];
	}
	
	/**
	 * Lists all Location models.
	 * 
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new LocationSearch ();
		$dataProvider = $searchModel->search ( Yii::$app->request->queryParams );
		
		return $this->render ( 'index', [ 
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider 
		] );
	}
	
	/**
	 * Displays a single Location model.
	 * 
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render ( 'view', [ 
				'model' => $this->findModel ( $id ) 
		] );
	}
	
	/**
	 * Creates a new Location model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * 
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Location ();
		
		$langsModels = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $lang ) {
			$langsModels [] = new LocationLang ( [ 
					'LangID' => $lang 
			] );
		}
		
		$imagesModels = [ ];
		
		$locationCategoryModels = [ ];
		$categoryModels = \app\models\Category\Category::find ()->with ( 'currentLang' )->all ();
		
		if ($model->load ( Yii::$app->request->post () ) && $model->validate ()) {
			if (Model::loadMultiple ( $langsModels, Yii::$app->request->post () ) && Model::validateMultiple ( $langsModels )) {
				$model->save ();
				
				foreach ( $langsModels as $i => $lmodel ) {
					$lmodel->LangID = \Yii::$app->params ['siteLanguages'] [$i];
					$lmodel->LocationID = $model->ID;
					$lmodel->save ();
				}
				
				$images = \yii\web\UploadedFile::getInstancesByName ( 'Images' );
				
				foreach ( $images as $imageIns ) {
					$image = md5 ( microtime ( true ) ) . '.' . $imageIns->extension;
					$imagePath = Yii::getAlias ( '@webroot/uploads/location/' . $image );
					$imageIns->saveAs ( $imagePath );
					
					$thumb = 'thumb_' . $image;
					$thumbPath = Yii::getAlias ( '@webroot/uploads/location/' . $thumb );
					\yii\imagine\Image::thumbnail ( $imagePath, 263, 200 )->save ( $thumbPath );
					
					$imageModel = new Image ();
					$imageModel->Thumb = $thumb;
					$imageModel->Image = $image;
					$imageModel->save ();
					
					$locationImageModel = new LocationImage ( [ 
							'LocationID' => $model->ID,
							'ImageID' => $imageModel->ID 
					] );
					$locationImageModel->save ();
				}
				
				\Yii::$app->session->setFlash ( 'success', 'Locaţia a fost salvată' );
				
				return $this->redirect ( [ 
						'update',
						'id' => $model->ID 
				] );
			}
		}
		
		return $this->render ( 'create', [ 
				'model' => $model,
				'langsModels' => $langsModels,
				'imagesModels' => $imagesModels,
				'locationCategoryModels' => $locationCategoryModels,
				'categoryModels' => $categoryModels 
		] );
	}
	
	/**
	 * Updates an existing Location model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * 
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel ( $id );
		$langsModels = $model->locationLangs;
		$imagesModels = LocationImage::find ()->where ( [ 
				'LocationID' => $model->ID 
		] )->with ( 'image' )->orderBy ( 'Position' )->all ();
		$locationCategoryModels = \app\models\LocationCategory\LocationCategory::findAll ( [ 
				'LocationID' => $model->ID 
		] );
		$categoryModels = \app\models\Category\Category::find ()->with ( 'currentLang' )->all ();
		
		if ($model->load ( Yii::$app->request->post () ) && $model->validate ()) {
			if (Model::loadMultiple ( $langsModels, Yii::$app->request->post () ) && Model::validateMultiple ( $langsModels )) {
				// save model
				$model->save ();
				
				// save langs
				foreach ( $langsModels as $i => $lmodel ) {
					$lmodel->save ();
				}
				
				// save images
				$images = \yii\web\UploadedFile::getInstancesByName ( 'Images' );
				
				foreach ( $images as $imageIns ) {
					$image = md5 ( microtime ( true ) ) . '.' . $imageIns->extension;
					$imagePath = Yii::getAlias ( '@webroot/uploads/location/' . $image );
					$imageIns->saveAs ( $imagePath );
					
					$thumb = 'thumb_' . $image;
					$thumbPath = Yii::getAlias ( '@webroot/uploads/location/' . $thumb );
					\yii\imagine\Image::thumbnail ( $imagePath, 263, 200 )->save ( $thumbPath );
					
					$imageModel = new Image ();
					$imageModel->Thumb = $thumb;
					$imageModel->Image = $image;
					$imageModel->save ();
					
					$locationImageModel = new LocationImage ( [ 
							'LocationID' => $model->ID,
							'ImageID' => $imageModel->ID 
					] );
					$locationImageModel->save ();
				}
				
				// save categories relations
				$checkedCategories = ( array ) Yii::$app->request->post ( 'LocationCategories' );
				LocationCategory::deleteAll ( [ 
						'LocationID' => $model->ID 
				] );
				foreach ( $checkedCategories as $cc ) {
					$locationCategoryModel = new LocationCategory ( [ 
							'LocationID' => $model->ID,
							'CategoryID' => $cc 
					] );
					$locationCategoryModel->save ();
				}
				
				\Yii::$app->session->setFlash ( 'success', 'Locaţia a fost salvată' );
				
				return $this->redirect ( [ 
						'update',
						'id' => $model->ID 
				] );
			}
		}
		
		return $this->render ( 'update', [ 
				'model' => $model,
				'langsModels' => $langsModels,
				'imagesModels' => $imagesModels,
				'locationCategoryModels' => $locationCategoryModels,
				'categoryModels' => $categoryModels 
		] );
	}
	
	/**
	 * Deletes an existing Location model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * 
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel ( $id )->delete ();
		
		return $this->redirect ( [ 
				'index' 
		] );
	}
	
	/**
	 * Finds the Location model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * 
	 * @param integer $id        	
	 * @return Location the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Location::findOne ( $id )) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
	public function actionRegionList() {
		$params = Yii::$app->request->post ( 'depdrop_all_params' );
		
		$list = \app\models\CountryRegion\CountryRegion::listForDropdown ( $params ['country-id'], false );
		
		$out ['output'] = [ ];
		foreach ( $list as $key => $val ) {
			$out ['output'] [] = [ 
					'id' => $key,
					'name' => $val 
			];
		}
		
		return json_encode ( $out );
	}
	public function actionDeleteImage() {
		$key = Yii::$app->request->post ( 'key' );
		Yii::$app->db->createCommand ()->delete ( 'LocationImage', [ 
				'ID' => $key 
		] )->execute ();
		
		return json_encode ( [ 
				'key' => $key 
		] );
	}
	public function actionImageSort() {
		$order = Yii::$app->request->post ( 'order' );
		$locationID = Yii::$app->request->post ( 'locationID' );
		
		$position = 0;
		foreach ( $order as $locationImageID ) {
			Yii::$app->db->createCommand ( "UPDATE `LocationImage` SET `Position` = '$position' WHERE `ID` = $locationImageID" )->query ();
			$position ++;
		}
	}
}
