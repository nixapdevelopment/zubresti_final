<?php

namespace app\modules\News;

/**
 * Page module definition class
 */
class News extends \yii\base\Module {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\News\controllers';
	
	/**
	 * @inheritdoc
	 */
	public function init() {
		parent::init ();
		
		// custom initialization code goes here
	}
}
