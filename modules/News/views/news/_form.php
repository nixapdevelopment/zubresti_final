<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\select2\Select2;
use app\models\News\News;
use kartik\widgets\DatePicker;

?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
		<div class="col-md-6">
            <?=$form->field ( $model, 'Date' )->widget ( DatePicker::className (), [ 'type' => DatePicker::TYPE_COMPONENT_APPEND,'options' => [ 'value' => date ( 'd.m.Y', empty ( $model->Date ) ? time () : strtotime ( $model->Date ) ) ],'pluginOptions' => [ 'autoclose' => true,'format' => 'dd.mm.yyyy','todayHighlight' => true ] ] )?>
        </div>
		<div class="col-md-6">
            <?=$form->field ( $model, 'Status' )->widget ( Select2::className (), [ 'data' => News::getStatusList () ] )?>
        </div>

	</div>
    
    <?php foreach ($pageLangModels as $key => $lmodel) { ?>
    
        <?php
					
$items [] = [ 
							'label' => strtoupper ( Yii::$app->params ['siteLanguages'] [$key] ),
							'content' => $this->render ( '_lang_form', [ 
									'lmodel' => $lmodel,
									'form' => $form,
									'key' => $key 
							] ),
							'active' => $key == 0 
					];
					?>
    
    <?php } ?>
    
    <?=Tabs::widget ( [ 'items' => $items ] )?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t("app", 'Creează') : Yii::t("app", 'Editează'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
