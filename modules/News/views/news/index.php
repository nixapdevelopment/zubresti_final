<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\News\NewsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t("app", 'Noutăţi');
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="category-index">

	<h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t("app", 'Creează noutate'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
        <?=GridView::widget ( [ 'dataProvider' => $dataProvider,// 'filterModel' => $searchModel,'columns' => [ [ 'class' => 'yii\grid\SerialColumn','options' => [ 'width' => '50px' ] ],[ 'label' => 'Name','value' => function ($model) {return $model->currentLang->Title;},'filter' => false ],[ 'attribute' => 'Date','filter' => \kartik\widgets\DatePicker::widget ( [ 'name' => 'Date','pluginOptions' => [ 'autoclose' => true,'format' => 'dd.mm.yyyy','todayHighlight' => true ],'value' => Yii::$app->request->get ( 'Date' ) ] ),'value' => function ($model) {return date ( 'd.m.Y', strtotime ( $model->Date ) );},'options' => [ 'width' => '200px' ] ],[ 'attribute' => 'Status','filter' => false,'options' => [ 'width' => '150px' ] ],[ 'class' => 'yii\grid\ActionColumn','options' => [ 'width' => '100px' ],'template' => '<div class="text-center">{update}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{delete}</div>' ] ] ] );?>
    <?php Pjax::end(); ?>
</div>