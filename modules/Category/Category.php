<?php

namespace app\modules\Category;

/**
 * Category module definition class
 */
class Category extends \yii\base\Module {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\Category\controllers';
	
	/**
	 * @inheritdoc
	 */
	public function init() {
		parent::init ();
		
		// custom initialization code goes here
	}
}
