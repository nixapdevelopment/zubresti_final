<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Category\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t ( "app", 'Categorii' );
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="category-index">

	<h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t("app", 'Creează categorie'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    <?=GridView::widget ( [ 'dataProvider' => $dataProvider,// 'filterModel' => $searchModel,'columns' => [ [ 'class' => 'yii\grid\SerialColumn' ],[ 'label' => Yii::t ( "app", 'Nume' ),'value' => function ($model) {return reset ( $model->categoryLangs )->Title;},'filter' => false ],[ 'attribute' => 'Image','value' => function ($model) {return Html::img ( '/uploads/category/' . $model->image->Thumb, [ 'style' => 'height:50px;' ] );},'format' => 'raw','filter' => false ],[ 'attribute' => 'Status','filter' => false ],[ 'class' => 'yii\grid\ActionColumn' ] ] ] );?>
<?php Pjax::end(); ?></div>
