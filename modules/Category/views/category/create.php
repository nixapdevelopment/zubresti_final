<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Category\Category */

$this->title = Yii::t ( "app", 'Creează categorie' );
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( "app", 'Categorii' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="category-create">

	<h1><?= Html::encode($this->title) ?></h1>

    <?=$this->render ( '_form', [ 'model' => $model,'langsModels' => $langsModels ] )?>

</div>
