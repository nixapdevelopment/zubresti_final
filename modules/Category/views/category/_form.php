<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\widgets\FileInput;
use kartik\select2\Select2;
use yii\helpers\Url;

?>

<div class="category-form">

    <?php
				
$form = ActiveForm::begin ( [ 
						'options' => [ 
								'enctype' => 'multipart/form-data' 
						] 
				] );
				?>
    
    
    
    <div class="row">
		<div class="col-md-6">
            <?=$form->field ( $model, 'Image' )->widget ( FileInput::className (), [ 'options' => [ 'multiple' => false ],'pluginOptions' => [ 'showRemove' => false,'showUpload' => false,'initialPreviewAsData' => true,'initialPreview' => [ empty ( $model->image->Thumb ) ? null : Url::to ( '/uploads/category/' . $model->image->Thumb, true ) ] ] ] )?>
        </div>
		<div class="col-md-6">
            <?= $form->field($model, 'Link')->textInput() ?>
            
            <?=$form->field ( $model, 'Status' )->widget ( Select2::className (), [ 'data' => app\models\Category\Category::getStatusList () ] )?>
        </div>
	</div>

	<br />
    
    <?php foreach ($langsModels as $key => $lmodel) { ?>
    
        <?php
					
$items [] = [ 
							'label' => strtoupper ( Yii::$app->params ['siteLanguages'] [$key] ),
							'content' => $this->render ( '_lang_form', [ 
									'lmodel' => $lmodel,
									'form' => $form,
									'key' => $key 
							] ),
							'active' => $key == 0 
					];
					?>
    
    <?php } ?>
    
    <?=Tabs::widget ( [ 'items' => $items ] )?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t("app", "Creează") : Yii::t("app", "Editează"), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
