<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryLang\CategoryLang */

$this->title = Yii::t ( "app", 'Creează limba p/u categorie' );
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Limbile categoriilor' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="category-lang-create">

	<h1><?= Html::encode($this->title) ?></h1>

    <?=$this->render ( '_form', [ 'model' => $model ] )?>

</div>
