<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategoryLang\CategoryLangSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t ( 'app', 'Limbile categoriilor' );
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="category-lang-index">

	<h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t("app", 'Creează limba p/u categorie'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?=GridView::widget ( [ 'dataProvider' => $dataProvider,'filterModel' => $searchModel,'columns' => [ [ 'class' => 'yii\grid\SerialColumn' ],'ID','CategoryID','LangID','Title','Text:ntext',// 'SeoTitle',// 'Keywords',// 'Description',[ 'class' => 'yii\grid\ActionColumn' ] ] ] );?>
</div>
