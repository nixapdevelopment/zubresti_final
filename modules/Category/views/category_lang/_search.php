<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryLang\CategoryLangSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-lang-search">

    <?php
				
$form = ActiveForm::begin ( [ 
						'action' => [ 
								'index' 
						],
						'method' => 'get' 
				] );
				?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'CategoryID') ?>

    <?= $form->field($model, 'LangID') ?>

    <?= $form->field($model, 'Title') ?>

    <?= $form->field($model, 'Text') ?>

    <?php // echo $form->field($model, 'SeoTitle') ?>

    <?php // echo $form->field($model, 'Keywords') ?>

    <?php // echo $form->field($model, 'Description') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t("app",'Caută'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t("app", 'Resetează'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
