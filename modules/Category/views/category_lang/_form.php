<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CategoryLang\CategoryLang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="category-lang-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CategoryID')->textInput() ?>

    <?= $form->field($model, 'LangID')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Text')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'SeoTitle')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Keywords')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'Description')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t("app", "Creează") : Yii::t("app", "Editează"), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
