<?php

namespace app\modules\Category\controllers;

use Yii;
use app\models\Category\Category;
use app\models\Category\CategorySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;
use app\models\CategoryLang\CategoryLang;
use app\models\Image\Image;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends \app\controllers\BackendController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [ 
				'verbs' => [ 
						'class' => VerbFilter::className (),
						'actions' => [ 
								'delete' => [ 
										'POST' 
								] 
						] 
				] 
		];
	}
	
	/**
	 * Lists all Category models.
	 * 
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new CategorySearch ();
		$dataProvider = $searchModel->search ( Yii::$app->request->queryParams );
		
		return $this->render ( 'index', [ 
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider 
		] );
	}
	
	/**
	 * Displays a single Category model.
	 * 
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionView($id) {
		$model = $this->findModel ( $id );
		$langsModels = $model->categoryLangs;
		return $this->render ( 'view', [ 
				'model' => $model,
				'langsModels' => $langsModels 
		] );
	}
	
	/**
	 * Creates a new Category model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * 
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Category ();
		
		$langsModels = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $lang ) {
			$langsModels [] = new CategoryLang ( [ 
					'LangID' => $lang 
			] );
		}
		
		if ($model->load ( Yii::$app->request->post () ) && $model->validate ()) {
			if (Model::loadMultiple ( $langsModels, Yii::$app->request->post () ) && Model::validateMultiple ( $langsModels )) {
				$uploadedFile = \yii\web\UploadedFile::getInstance ( $model, 'Image' );
				
				if ($uploadedFile) {
					$image = md5 ( microtime ( true ) ) . '.' . $uploadedFile->extension;
					$imagePath = \Yii::getAlias ( '@webroot/uploads/category/' . $image );
					$uploadedFile->saveAs ( $imagePath );
					
					$thumb = 'thumb_' . $image;
					$thumbPath = \Yii::getAlias ( '@webroot/uploads/category/' . $thumb );
					\yii\imagine\Image::thumbnail ( $imagePath, 350, 350 )->save ( $thumbPath );
					
					$imageModel = new Image ();
					$imageModel->Thumb = $thumb;
					$imageModel->Image = $image;
					$imageModel->save ();
					
					$model->Image = $imageModel->ID;
				} else {
					$model->Image = 0;
				}
				
				$model->save ();
				
				foreach ( $langsModels as $i => $lmodel ) {
					$lmodel->LangID = \Yii::$app->params ['siteLanguages'] [$i];
					$lmodel->CategoryID = $model->ID;
					$lmodel->save ();
				}
				
				\Yii::$app->session->setFlash ( 'success', Yii::t ( "app", 'Categoria a fost salvată' ) );
				
				return $this->redirect ( [ 
						'update',
						'id' => $model->ID 
				] );
			}
		}
		
		return $this->render ( 'create', [ 
				'model' => $model,
				'langsModels' => $langsModels 
		] );
	}
	
	/**
	 * Updates an existing Category model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * 
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel ( $id );
		$langsModels = $model->categoryLangs;
		
		$oldImage = $model->Image;
		
		if ($model->load ( Yii::$app->request->post () ) && $model->validate ()) {
			if (Model::loadMultiple ( $langsModels, Yii::$app->request->post () ) && Model::validateMultiple ( $langsModels )) {
				$uploadedFile = \yii\web\UploadedFile::getInstance ( $model, 'Image' );
				
				if (! empty ( $uploadedFile->size )) {
					$image = md5 ( microtime ( true ) ) . '.' . $uploadedFile->extension;
					$imagePath = \Yii::getAlias ( '@webroot/uploads/category/' . $image );
					$uploadedFile->saveAs ( $imagePath );
					
					$thumb = 'thumb_' . $image;
					$thumbPath = \Yii::getAlias ( '@webroot/uploads/category/' . $thumb );
					\yii\imagine\Image::thumbnail ( $imagePath, 350, 350 )->save ( $thumbPath );
					
					$imageModel = new Image ();
					$imageModel->Thumb = $thumb;
					$imageModel->Image = $image;
					$imageModel->save ();
					
					$model->Image = $imageModel->ID;
				} else {
					$model->Image = $oldImage;
				}
				
				$model->save ();
				
				foreach ( $langsModels as $i => $lmodel ) {
					$lmodel->LangID = \Yii::$app->params ['siteLanguages'] [$i];
					$lmodel->CategoryID = $model->ID;
					$lmodel->save ();
				}
				
				\Yii::$app->session->setFlash ( 'success', Yii::t ( "app", 'Categoria a fost salvată' ) );
				
				return $this->redirect ( [ 
						'update',
						'id' => $model->ID 
				] );
			}
		}
		
		return $this->render ( 'update', [ 
				'model' => $model,
				'langsModels' => $langsModels 
		] );
	}
	
	/**
	 * Deletes an existing Category model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * 
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel ( $id )->delete ();
		
		return $this->redirect ( [ 
				'index' 
		] );
	}
	
	/**
	 * Finds the Category model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * 
	 * @param integer $id        	
	 * @return Category the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Category::find ()->where ( [ 
				'ID' => $id 
		] )->with ( [ 
				'image',
				'categoryLangs' 
		] )->one ()) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
	public function actionDeleteImage($id) {
		return;
	}
}
