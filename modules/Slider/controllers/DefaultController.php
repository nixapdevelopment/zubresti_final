<?php

namespace app\modules\Slider\controllers;

use Yii;
use yii\web\Controller;
use app\models\Slider\Slider;
use yii\data\ActiveDataProvider;
use yii\base\Model;
use app\models\SliderItem\SliderItem;
use app\models\SliderItemLang\SliderItemLang;

/**
 * Default controller for the `Slider` module
 */
class DefaultController extends Controller {
	/**
	 * Renders the index view for the module
	 * 
	 * @return string
	 */
	public function actionIndex() {
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => Slider::find () 
		] );
		
		return $this->render ( 'index', [ 
				'dataProvider' => $dataProvider 
		] );
	}
	public function actionCreate() {
		$sliderModel = new Slider ();
		
		if (Yii::$app->request->isPost) {
			if ($sliderModel->load ( Yii::$app->request->post () ) && $sliderModel->validate ()) {
				$sliderModel->save ();
			}
		}
		
		return $this->render ( 'create', [ 
				'sliderModel' => $sliderModel 
		] );
	}
	public function actionUpdate($id) {
		$sliderModel = Slider::findOne ( $id );
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => SliderItem::find ()->where ( [ 
						'SliderID' => $sliderModel->ID 
				] ) 
		] );
		
		if (Yii::$app->request->isPost) {
			if ($sliderModel->load ( Yii::$app->request->post () ) && $sliderModel->validate ()) {
				$sliderModel->save ();
				
				Yii::$app->session->setFlash ( 'success', Yii::t("app", 'Sliderul a fost salvat') );
				
				return $this->redirect ( [ 
						'update',
						'id' => $sliderModel->ID 
				] );
			}
		}
		
		return $this->render ( 'update', [ 
				'sliderModel' => $sliderModel,
				'dataProvider' => $dataProvider 
		] );
	}
	public function actionGetItem() {
		$id = ( int ) Yii::$app->request->post ( 'id' );
		
		$sliderItem = $id > 0 ? SliderItem::findOne ( $id ) : new SliderItem ();
		
		$sliderItemLangs = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $i => $lang ) {
			$sliderItemLangs [$i] = isset ( $sliderItem->langs [$lang] ) ? $sliderItem->langs [$lang] : new SliderItemLang ( [ 
					'LangID' => $lang 
			] );
		}
		
		return $this->renderAjax ( 'slider-item', [ 
				'sliderItem' => $sliderItem,
				'sliderItemLangs' => $sliderItemLangs 
		] );
	}
	public function actionSaveSliderItem() {
		$id = Yii::$app->request->post ( 'SliderItem' ) ['ID'];
		
		$sliderItem = $id > 0 ? SliderItem::findOne ( $id ) : new SliderItem ();
		
		$sliderItemLangs = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $i => $lang ) {
			$sliderItemLangs [$i] = isset ( $sliderItem->langs [$lang] ) ? $sliderItem->langs [$lang] : new SliderItemLang ( [ 
					'LangID' => $lang 
			] );
		}
		
		if ($sliderItem->load ( Yii::$app->request->post () )) {
			$imageIns = \yii\web\UploadedFile::getInstanceByName ( 'SliderItem[Image]' );
			
			$imageName = md5 ( microtime ( true ) ) . '.' . $imageIns->extension;
			$imagePath = Yii::getAlias ( '@webroot/uploads/slider/' . $imageName );
			
			if (! empty ( $imageIns->extension ) && $imageIns->saveAs ( $imagePath )) {
				\yii\imagine\Image::thumbnail ( $imagePath, 1400, 600 )->save ( $imagePath );
				$sliderItem->Image = $imageName;
			}
			
			$sliderItem->SliderID = 1;
			$sliderItem->Position = 999999;
			$sliderItem->save ();
			
			Model::loadMultiple ( $sliderItemLangs, Yii::$app->request->post () );
			
			foreach ( $sliderItemLangs as $sil ) {
				$sil->SliderItemID = $sliderItem->ID;
				$sil->save ();
			}
			
			\yii\caching\TagDependency::invalidate ( Yii::$app->cache, 'slider' );
		}
	}
	public function actionDeleteSliderItem($id) {
		$model = SliderItem::findOne ( $id );
		
		SliderItem::deleteAll ( [ 
				'ID' => $id 
		] );
		
		\yii\caching\TagDependency::invalidate ( Yii::$app->cache, 'slider' );
		
		$this->redirect ( [ 
				'/admin/slider/default/update',
				'id' => $model->SliderID 
		] );
	}
}
