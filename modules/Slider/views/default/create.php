<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Slider\Slider */

$this->title = Yii::t("app", 'Creare slider');
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t("app", 'Slidere'),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="slider-create">

	<h1><?= Html::encode($this->title) ?></h1>

    <?=$this->render ( '_form', [ 'sliderModel' => $sliderModel ] )?>

</div>
