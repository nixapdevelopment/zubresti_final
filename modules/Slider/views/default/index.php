<?php
use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t("app", 'Slider');
$this->params ['breadcrumbs'] [] = $this->title;
?>

<div class="slider-index">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t("app", 'Creează slider'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
    <?=GridView::widget ( [ 'dataProvider' => $dataProvider,'columns' => [ [ 'class' => 'yii\grid\SerialColumn','options' => [ 'width' => '50px' ] ],[ 'label' => 'Name','value' => function ($model) {return $model->Name;} ],[ 'class' => 'yii\grid\ActionColumn','options' => [ 'width' => '100px' ],'buttons' => [ 'update' => function ($url, $model) {return Html::a ( '<span class="glyphicon glyphicon-pencil"></span>', $url, [ 'title' => 'Edit' ] );},'delete' => function ($url, $model) {return Html::a ( '<span class="glyphicon glyphicon-trash text-danger"></span>', $url, [ 'title' => 'Delete','onclick' => "return confirm('Delete?')" ] );} ],'template' => '<div class="text-center">{update}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{delete}</div>' ] ] ] );?>

</div>