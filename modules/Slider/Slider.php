<?php

namespace app\modules\Slider;

/**
 * Slider module definition class
 */
class Slider extends \yii\base\Module {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\Slider\controllers';
	
	/**
	 * @inheritdoc
	 */
	public function init() {
		parent::init ();
		
		// custom initialization code goes here
	}
}
