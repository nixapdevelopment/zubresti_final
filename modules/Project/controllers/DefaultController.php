<?php

namespace app\modules\Project\controllers;

use app\controllers\BackendController;

/**
 * Default controller for the `Project` module
 */
class DefaultController extends BackendController {
	/**
	 * Renders the index view for the module
	 * 
	 * @return string
	 */
	public function actionIndex() {
		return $this->render ( 'index' );
	}
	public function permissions() {
		return [ 
				'index' => [ 
						'permission' => 'view_projects',
						'label' => Yii::t("app", 'Vezi proiecte') 
				] 
		];
	}
}
