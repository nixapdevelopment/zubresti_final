<?php

namespace app\modules\Project;

class Project extends \app\core\CoreModule {
	public $controllerNamespace = 'app\modules\Project\controllers';
	public $name = 'Projects';
	public function init() {
		parent::init ();
	}
}