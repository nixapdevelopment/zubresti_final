<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Project\Project */

$this->title = $model->Name;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t('app', 'Proiecte'),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="project-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
        <?= Html::a(Yii::t("app", 'Editează'), ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?=Html::a ( Yii::t("app", 'Șterge'), [ 'delete','id' => $model->ID ], [ 'class' => 'btn btn-danger','data' => [ 'confirm' => 'Are you sure you want to delete this item?','method' => 'post' ] ] )?>
    </p>

    <?=DetailView::widget ( [ 'model' => $model,'attributes' => [ 'ID','Creator','Name','Created','StartDate','EndDate','DueDate','Price','Priority','GitRepository','Status' ] ] )?>

</div>
