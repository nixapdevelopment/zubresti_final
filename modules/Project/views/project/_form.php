<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Project\Project;
use kartik\date\DatePicker;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Project\Project */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="project-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
		<div class="col-md-9">
            <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>
        </div>
		<div class="col-md-3">
            <?= $form->field($model, 'Price')->textInput(['maxlength' => true, 'type' => 'number']) ?>
        </div>
	</div>

	<div class="row">
		<div class="col-md-4">
            <?=$form->field ( $model, 'StartDate' )->widget ( DatePicker::className (), [ 'type' => DatePicker::TYPE_COMPONENT_APPEND,'options' => [ 'value' => date ( 'd.m.Y', empty ( $model->StartDate ) ? time () : strtotime ( $model->StartDate ) ) ],'pluginOptions' => [ 'autoclose' => true,'format' => 'dd.mm.yyyy','todayHighlight' => true ] ] )?>
        </div>
		<div class="col-md-4">
            <?=$form->field ( $model, 'EndDate' )->widget ( DatePicker::className (), [ 'type' => DatePicker::TYPE_COMPONENT_APPEND,'options' => [ 'value' => date ( 'd.m.Y', empty ( $model->EndDate ) ? strtotime ( '+1 month' ) : strtotime ( $model->EndDate ) ) ],'pluginOptions' => [ 'autoclose' => true,'format' => 'dd.mm.yyyy','todayHighlight' => true ] ] )?>
        </div>
		<div class="col-md-4">
            <?=$form->field ( $model, 'DueDate' )->widget ( DatePicker::className (), [ 'type' => DatePicker::TYPE_COMPONENT_APPEND,'options' => [ 'value' => date ( 'd.m.Y', empty ( $model->DueDate ) ? strtotime ( '+1 month' ) : strtotime ( $model->DueDate ) ) ],'pluginOptions' => [ 'autoclose' => true,'format' => 'dd.mm.yyyy','todayHighlight' => true ] ] )?>
        </div>
	</div>

    <?= $form->field($model, 'GitRepository')->textInput(['maxlength' => true]) ?>
    
    <div class="row">
		<div class="col-md-4">
            <?=$form->field ( $model, 'Client' )->widget ( Select2::className (), [ 'data' => \app\models\User\UserSearch::getList ( true ) ] )?>
        </div>
		<div class="col-md-4">
            <?= $form->field($model, 'Priority')->dropDownList(Project::getPriorityList()) ?>
        </div>
		<div class="col-md-4">
            <?= $form->field($model, 'Status')->dropDownList(Project::getStatusList()) ?>
        </div>
	</div>

	<div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t("app", 'Creează') : Yii::t("app", 'Editează'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
