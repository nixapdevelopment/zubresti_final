<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Project\ProjectSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Proiecte');
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="project-index">

	<h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t("app", 'Creează proiect'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    
        <?=GridView::widget ( [ 'tableOptions' => [ 'class' => 'table table-striped table-bordered' ],'dataProvider' => $dataProvider,'filterModel' => $searchModel,'columns' => [ [ 'class' => 'yii\grid\SerialColumn' ],'Name',[ 'attribute' => 'Price','filter' => '' ],[ 'attribute' => 'Created','filter' => '','value' => function ($model) {return date ( 'd.m.Y H:i', strtotime ( $model->Created ) );} ],// 'StartDate:date',// 'EndDate:date',[ 'attribute' => 'DueDate','filter' => '','value' => function ($model) {return date ( 'd.m.Y', strtotime ( $model->DueDate ) );} ],// 'Priority',// 'GitRepository',[ 'label' => 'Creator','attribute' => 'creator.Email' ],[ 'attribute' => 'Priority','filter' => app\models\Project\Project::getPriorityList () ],[ 'attribute' => 'Status','filter' => app\models\Project\Project::getStatusList () ],[ 'class' => 'yii\grid\ActionColumn','template' => '<div class="text-center">{update}&nbsp;&nbsp;&nbsp;{view}&nbsp;&nbsp;&nbsp;{delete}</div>' ] ] ] );?>
    <?php Pjax::end(); ?>
</div>
