<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Project\Project */

$this->title = Yii::t("app", 'Creează proiect');
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t('app', 'Proiecte'),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="project-create">

	<h1><?= Html::encode($this->title) ?></h1>

    <?=$this->render ( '_form', [ 'model' => $model ] )?>

</div>
