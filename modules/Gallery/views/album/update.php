<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery\Gallery */

$this->title = Yii::t ( 'app', 'Editează galeria : ' ) . $model->ID;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Galerii' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = [ 
		'label' => $model->ID,
		'url' => [ 
				'view',
				'id' => $model->ID 
		] 
];
$this->params ['breadcrumbs'] [] = Yii::t ( 'app', 'Editează' );
?>
<div class="gallery-update">

	<h1><?= Html::encode($this->title) ?></h1>

    <?=$this->render ( '_form', [ 'model' => $model ] )?>

</div>
