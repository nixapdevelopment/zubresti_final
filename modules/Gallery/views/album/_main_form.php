<?php
use yii\helpers\Html;
use yii\bootstrap\Tabs;
use app\models\Gallery\Gallery;
use yii\helpers\ArrayHelper;

?>

<?php $form = ActiveForm::begin(); ?>

<?=$form->field ( $model, 'ParentID' )->dropDownList ( ArrayHelper::merge ( [ '0' => '-' ], ArrayHelper::map ( Gallery::find ()->with ( 'lang' )->all (), 'ID', 'lang.Title' ) ), [ 'class' => 'hidden' ] )->label ( false )?>

<?=$form->field ( $model, 'Position' )->hiddenInput ( [ 'value' => 0 ] )->label ( false )?>

<?php ActiveForm::end(); ?>

<?php foreach ($modelLangs as $key => $lmodel) { ?>
    <?php
	
$items [] = [ 
			'label' => strtoupper ( Yii::$app->params ['siteLanguages'] [$key] ),
			'content' => $this->render ( '_lang_form', [ 
					'lmodel' => $lmodel,
					'form' => $form,
					'key' => $key 
			] ),
			'active' => $key == 0 
	];
	?>
<?php } ?>

<?=Tabs::widget ( [ 'items' => $items ] )?>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Creează') : Yii::t('app', 'Editează'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    and 
    <?=Html::dropDownList ( 'Redirect', NULL, [ 'remain' => Yii::t('app', 'Rămîne'),'list' => 'Înapoi la listă' ], [ 'class' => 'form-control','style' => 'width: 150px; display: inline-block;' ] )?>
</div>