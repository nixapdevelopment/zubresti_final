<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;

?>

<?php $form = ActiveForm::begin(); ?>

    <?php foreach ($galleryItemLangs as $key => $lmodel) { ?>

        <?= $form->field($lmodel, "[$key]Title")->textInput(['maxlength' => true])->label('Titlu (' . strtoupper($lmodel->LangID) . ')') ?>
        <?= $form->field($lmodel, "[$key]GalleryItemID")->hiddenInput()->label(false) ?>
        <?= $form->field($lmodel, "[$key]LangID")->hiddenInput()->label(false) ?>

    <?php } ?>

    <?= Html::hiddenInput('ID', $id) ?>

<div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Editează'), ['class' => 'btn btn-success']) ?>
    </div>

<?php ActiveForm::end(); ?>