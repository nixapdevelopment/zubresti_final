<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery\Gallery */

$this->title = Yii::t ( 'app', 'Creează galerie' );
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Galerii' ),
		'url' => $this->context->url ( [ 
				'index' 
		] ) 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="gallery-create">

	<h1><?= Html::encode($this->title) ?></h1>

    <?=$this->render ( '_form', [ 'model' => $model,'modelLangs' => $modelLangs ] )?>

</div>
