<?php
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\Gallery\Gallery */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="gallery-form">

    <?php
				$tabItems = [ ];
				$tabItems [] = [ 
						'label' => Yii::t ( 'app', 'Datele galeriei' ),
						'content' => $this->render ( '_main_form', [ 
								'model' => $model,
								'modelLangs' => $modelLangs,
								'form' => $form,
								'key' => $key 
						] ),
						'active' => true 
				];
				
				if ($model->ID) {
					$tabItems [] = [ 
							'label' => Yii::t ( 'app', 'Itemii galeriei' ),
							'content' => $this->render ( '_items_form', [ 
									'model' => $model 
							] ) 
					];
				}
				?>
    
    <?=Tabs::widget ( [ 'items' => $tabItems ] )?>

</div>
