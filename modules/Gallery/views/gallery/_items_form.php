<?php
use yii\helpers\Html;
use kartik\sortable\Sortable;
use yii\widgets\Pjax;
use yii\helpers\Url;
use lesha724\youtubewidget\Youtube;

// " . Html::img($item->Thumb, [
// 'width' => 200,
// 'height' => 200
// ]) . "

?>
<br />

<?=$this->render ( 'upload-images-form', [ 'model' => $model ] )?>

<?php

Pjax::begin ( [ 
		'id' => 'gallery-item-list' 
] )?>
    <?php $items = []; ?>
    <?php
				
foreach ( $model->items as $item ) {
					if ($item->Type == app\models\Gallery\Gallery::TypeVideo) {
						$items [] ['content'] = "
                <div style=\"position:relative;\" class=\"gallery-item\" data-id=\"" . $item->ID . "\">
                    <div style=\"position: absolute; top: 25px; width: 100%; text-align:center;\">
                        " . $item->lang->Title . "
                    </div>
                    <div  class=\"gallery-item-image\">
                        " . Youtube::widget ( [ 
								'video' => $item->Value,
								'height' => 200,
								'width' => 200 
						] ) . "
                    </div>
                    <div style=\"position:absolute; left:0; bottom:0; width:100%; background:#fff;\" class=\"gallery-item-options text-center\">
                        <a class=\"edit-gallery-item\" data-id=\"" . $item->ID . "\" style=\"cursor:pointer; font-size: 20px;\"><i class=\"fa fa-pencil text-success\"></i></a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a class=\"delete-gallery-item\" data-id=\"" . $item->ID . "\" style=\"cursor:pointer; font-size: 20px;\"><i class=\"fa fa-trash text-danger\"></i></a>
                    </div>
                </div>
            ";
					} else {
						$items [] ['content'] = "
                <div style=\"position:relative;\" class=\"gallery-item\" data-id=\"" . $item->ID . "\">
                    <div style=\"position: absolute; top: 25px; width: 100%; text-align:center;\">
                        " . $item->lang->Title . "
                    </div>
                    <div class=\"gallery-item-image\">
                        " . Html::img ( '@web/uploads/gallery/' . $item->Thumb, [ 
								'width' => 200,
								'height' => 200 
						] ) . "
                    </div>
                    <div style=\"position:absolute; left:0; bottom:0; width:100%; background:#fff;\" class=\"gallery-item-options text-center\">
                        <a class=\"edit-gallery-item\" data-id=\"" . $item->ID . "\" style=\"cursor:pointer; font-size: 20px;\"><i class=\"fa fa-pencil text-success\"></i></a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a class=\"delete-gallery-item\" data-id=\"" . $item->ID . "\" style=\"cursor:pointer; font-size: 20px;\"><i class=\"fa fa-trash text-danger\"></i></a>
                    </div>
                </div>
            ";
					}
				}
				?>

    <?=Sortable::widget ( [ 'type' => 'grid','items' => $items,'itemOptions' => [ 'width' => '200','height' => '200' ] ] );?>

    <?php
				
$this->registerJs ( '
        $(".sortable.grid").on("sortupdate", function(e) {
            var ids = [];
            $(".sortable.grid > li .gallery-item").each(function(){
                ids.push($(this).attr("data-id"));
            });
            $.post("' . Url::to ( [ 
						'/admin/gallery/gallery/sort-images' 
				] ) . '", {ids: ids});
        });
    ' )?>

<?php Pjax::end() ?>

<?php

yii\bootstrap\Modal::begin ( [ 
		'id' => 'galery-lang-modal' 
] )?>

<?php yii\bootstrap\Modal::end() ?>

<?php

$this->registerCss ( "
    .gallery-form .sortable-placeholder {
        width: 218px;
        height: 218px;
    }
" )?>

<?php

$this->registerJs ( '
    $(document).on("click", ".delete-gallery-item", function(){
        $.post("' . Url::to ( [ 
		'/admin/gallery/gallery/delete-image' 
] ) . '", {id: $(this).attr("data-id")}, function(){
            $.pjax.reload({container: "#gallery-item-list"});
        });
    });
    
    $(document).on("click", ".edit-gallery-item", function(){
        $.post("' . Url::to ( [ 
		'/admin/gallery/gallery/get-image-langs' 
] ) . '", {id: $(this).attr("data-id")}, function(html){
            $("#galery-lang-modal .modal-body").html(html);
            $("#galery-lang-modal").modal();
        });
    });
    
    $(document).on("submit", "#galery-lang-modal form", function(e){
        e.preventDefault();
        $.post("' . Url::to ( [ 
		'/admin/gallery/gallery/save-image-langs' 
] ) . '", $("#galery-lang-modal form").serialize(), function(){
            $("#galery-lang-modal").modal("hide");
            $.pjax.reload({container: "#gallery-item-list"});
        });
    });
    
    $(document).on("pjax:success", "#gallery-item-list", function(){
        onYouTubePlayerAPIReady();
    });
' )?>