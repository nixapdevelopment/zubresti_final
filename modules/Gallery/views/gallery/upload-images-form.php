<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use app\models\Gallery\Gallery;

?>

<?php

Pjax::begin ( [ 
		'id' => 'gallery-image-form-pjax',
		'enablePushState' => false 
] )?>

    <?=Html::beginForm ( Url::to ( [ '/admin/gallery/gallery/upload-images' ] ), 'post', [ 'id' => 'gallery-image-form','data-pjax' => '' ] )?>
<div class="row">
            <?php if (Yii::$app->request->get('type') == Gallery::TypeDefault) { ?>
            <div class="col-md-4">
		<div class="form-group">
                    <?=Html::fileInput ( 'Images[]', '', [ 'class' => 'form-control','required' => 'required','multiple' => true ] )?>
                </div>
	</div>
            <?php } else { ?>
            <div class="col-md-4">
		<div class="form-group">
                    <?=Html::textInput ( 'Video', '', [ 'class' => 'form-control','required' => 'required','placeholder' => 'Youtube link' ] )?>
                </div>
	</div>
            <?php } ?>
            <div class="col-md-2">
		<div class="form-group">
                    <?= Html::hiddenInput('GalleryID', $model->ID) ?>
                    <?=Html::submitButton ( Yii::t ( 'app', 'Trimite' ), [ 'class' => 'btn btn-success' ] )?>
                </div>
	</div>
</div>
<?= Html::endForm() ?>

<?php Pjax::end(); ?>

<?php

$this->registerJs ( '
    $(document).on("pjax:success", "#gallery-image-form-pjax",  function(event){
        $.pjax.reload({container: "#gallery-item-list"});
    });
' )?>