<?php

namespace app\modules\Gallery\controllers;

use Yii;
use app\models\Gallery\Gallery;
use app\models\GalleryLang\GalleryLang;
use app\models\Gallery\GallerySearch;
use app\controllers\BackendController;
use yii\web\NotFoundHttpException;
use yii\base\Model;
use app\models\GalleryItem\GalleryItem;

/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class GalleryController extends BackendController {
	
	/**
	 * Lists all Gallery models.
	 * 
	 * @return mixed
	 */
	public function actionIndex($type = 'Default') {
		$searchModel = new GallerySearch ();
		$dataProvider = $searchModel->search ( $type );
		
		return $this->render ( 'index', [ 
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider 
		] );
	}
	
	/**
	 * Creates a new Gallery model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * 
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Gallery ();
		$modelLangs = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $key => $lang ) {
			$modelLangs [$key] = new GalleryLang ( [ 
					'LangID' => $lang 
			] );
		}
		
		if (Yii::$app->request->isPost) {
			if ($model->load ( Yii::$app->request->post () ) && $model->validate () && Model::loadMultiple ( $modelLangs, Yii::$app->request->post () ) && Model::validateMultiple ( $modelLangs )) {
				$model->save ();
				
				foreach ( $modelLangs as $ml ) {
					$ml->GalleryID = $model->ID;
					$ml->save ();
				}
				
				Yii::$app->session->setFlash ( 'success', Yii::t("app", 'Galeria a fost salvată'));
				
				if (Yii::$app->request->post ( 'Redirect' ) == 'list') {
					return $this->redirect ( [ 
							'index' 
					] );
				}
				
				return $this->redirect ( $this->url ( [ 
						'update',
						'id' => $model->ID 
				] ) );
			}
		}
		
		return $this->render ( 'create', [ 
				'model' => $model,
				'modelLangs' => $modelLangs 
		] );
	}
	
	/**
	 * Updates an existing Gallery model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * 
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel ( $id );
		
		$modelLangs = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $key => $lang ) {
			$modelLangs [$key] = isset ( $model->langs [$lang] ) ? $model->langs [$lang] : new GalleryLang ( [ 
					'LangID' => $lang 
			] );
		}
		
		if (Yii::$app->request->isPost) {
			if ($model->load ( Yii::$app->request->post () ) && $model->validate () && Model::loadMultiple ( $modelLangs, Yii::$app->request->post () ) && Model::validateMultiple ( $modelLangs )) {
				$model->save ();
				
				foreach ( $modelLangs as $ml ) {
					$ml->GalleryID = $model->ID;
					$ml->save ();
				}
				
				Yii::$app->session->setFlash ( 'success', Yii::t("app", 'Galeria a fost salvată'));
				
				if (Yii::$app->request->post ( 'Redirect' ) == 'list') {
					return $this->redirect ( $this->url ( [ 
							'index' 
					] ) );
				}
				
				return $this->redirect ( $this->url ( [ 
						'update',
						'id' => $model->ID 
				] ) );
			}
		}
		
		return $this->render ( 'create', [ 
				'model' => $model,
				'modelLangs' => $modelLangs 
		] );
	}
	
	/**
	 * Deletes an existing Gallery model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * 
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionDelete($id, $type) {
		$this->findModel ( $id )->delete ();
		
		return $this->redirect ( [ 
				'index',
				'type' => Yii::$app->request->get ( 'type' ) 
		] );
	}
	
	/**
	 * Finds the Gallery model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * 
	 * @param integer $id        	
	 * @return Gallery the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Gallery::find ()->with ( 'langs' )->where ( [ 
				'ID' => $id 
		] )->one ()) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
	public function actionUploadImages() {
		$galleryID = ( int ) Yii::$app->request->post ( 'GalleryID' );
		
		$model = Gallery::findOne ( $galleryID );
		
		$maxPosition = GalleryItem::find ()->where ( [ 
				'GalleryID' => $galleryID 
		] )->max ( 'Position' );
		$newPosition = $maxPosition == NULL ? 0 : $maxPosition ++;
		
		if (isset ( $_FILES ['Images'] )) {
			$images = \yii\web\UploadedFile::getInstancesByName ( 'Images' );
			
			foreach ( $images as $imageIns ) {
				$image = md5 ( microtime ( true ) ) . '.' . $imageIns->extension;
				$imagePath = Yii::getAlias ( '@webroot/uploads/gallery/' . $image );
				
				if ($imageIns->saveAs ( $imagePath )) {
					$newPosition ++;
					\yii\imagine\Image::thumbnail ( $imagePath, 1200, 900 )->save ( $imagePath );
					
					$thumb = 'thumb_' . $image;
					$thumbPath = Yii::getAlias ( '@webroot/uploads/gallery/' . $thumb );
					\yii\imagine\Image::thumbnail ( $imagePath, 300, 300 )->save ( $thumbPath );
					
					$galleryImage = new GalleryItem ();
					$galleryImage->GalleryID = $galleryID;
					$galleryImage->Thumb = $thumb;
					$galleryImage->Value = $image;
					$galleryImage->Type = 'Image';
					$galleryImage->Position = $newPosition;
					$galleryImage->save ();
				}
			}
		}
		
		if (! empty ( $_POST ['Video'] )) {
			$newPosition ++;
			
			$url = trim ( Yii::$app->request->post ( 'Video' ) );
			parse_str ( parse_url ( $url, PHP_URL_QUERY ), $vars );
			
			$galleryImage = new GalleryItem ();
			$galleryImage->GalleryID = $galleryID;
			$galleryImage->Thumb = isset ( $vars ['v'] ) ? 'https://img.youtube.com/vi/' . $vars ['v'] . '/mqdefault.jpg' : '';
			$galleryImage->Value = $url;
			$galleryImage->Type = Gallery::TypeVideo;
			$galleryImage->Position = $newPosition;
			$galleryImage->save ();
		}
		
		return $this->renderAjax ( 'upload-images-form', [ 
				'model' => $model 
		] );
	}
	public function actionSortImages() {
		$ids = ( array ) Yii::$app->request->post ( 'ids' );
		
		foreach ( $ids as $position => $id ) {
			GalleryItem::updateAll ( [ 
					'Position' => $position 
			], [ 
					'ID' => $id 
			] );
		}
	}
	public function actionDeleteImage() {
		$id = ( int ) Yii::$app->request->post ( 'id' );
		
		GalleryItem::deleteAll ( [ 
				'ID' => $id 
		] );
	}
	public function actionGetImageLangs() {
		$id = ( int ) Yii::$app->request->post ( 'id' );
		
		$langs = \app\models\GalleryItemLang\GalleryItemLang::find ()->where ( [ 
				'GalleryItemID' => $id 
		] )->indexBy ( 'LangID' )->all ();
		$galleryItemLangs = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $key => $langID ) {
			$galleryItemLangs [$key] = isset ( $langs [$langID] ) ? $langs [$langID] : new \app\models\GalleryItemLang\GalleryItemLang ( [ 
					'GalleryItemID' => $id,
					'LangID' => $langID 
			] );
		}
		
		return $this->renderPartial ( 'gallery-item-langs', [ 
				'galleryItemLangs' => $galleryItemLangs,
				'id' => $id 
		] );
	}
	public function actionSaveImageLangs() {
		$id = ( int ) Yii::$app->request->post ( 'ID' );
		
		$langs = \app\models\GalleryItemLang\GalleryItemLang::find ()->where ( [ 
				'GalleryItemID' => $id 
		] )->indexBy ( 'LangID' )->all ();
		$galleryItemLangs = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $key => $langID ) {
			$galleryItemLangs [$key] = isset ( $langs [$langID] ) ? $langs [$langID] : new \app\models\GalleryItemLang\GalleryItemLang ( [ 
					'GalleryItemID' => $id,
					'LangID' => $langID 
			] );
		}
		
		Model::loadMultiple ( $galleryItemLangs, Yii::$app->request->post () );
		
		foreach ( $galleryItemLangs as $model ) {
			$model->save ();
		}
	}
	public function url($url) {
		$type = Yii::$app->request->get ( 'type', Gallery::TypeDefault );
		
		$url = ( array ) $url;
		$url ['type'] = $type;
		return \yii\helpers\Url::to ( $url );
	}
}
