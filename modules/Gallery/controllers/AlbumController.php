<?php

namespace app\modules\Gallery\controllers;

use Yii;
use app\models\GalleryAlbum\GalleryAlbum;
use app\models\GalleryAlbumLang\GalleryAlbumLang;
use app\models\GalleryAlbum\GalleryAlbumSearch;
use app\controllers\BackendController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Model;

/**
 * GalleryController implements the CRUD actions for Gallery model.
 */
class AlbumController extends BackendController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [ 
				'verbs' => [ 
						'class' => VerbFilter::className (),
						'actions' => [ 
								'delete' => [ 
										'POST' 
								] 
						] 
				] 
		];
	}
	
	/**
	 * Lists all Gallery models.
	 * 
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new GalleryAlbumSearch ();
		$dataProvider = $searchModel->search ();
		
		return $this->render ( 'index', [ 
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider 
		] );
	}
	
	/**
	 * Creates a new Gallery model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * 
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Gallery ();
		$modelLangs = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $key => $lang ) {
			$modelLangs [$key] = new GalleryLang ( [ 
					'LangID' => $lang 
			] );
		}
		
		if (Yii::$app->request->isPost) {
			if ($model->load ( Yii::$app->request->post () ) && $model->validate () && Model::loadMultiple ( $modelLangs, Yii::$app->request->post () ) && Model::validateMultiple ( $modelLangs )) {
				$model->save ();
				
				foreach ( $modelLangs as $ml ) {
					$ml->GalleryID = $model->ID;
					$ml->save ();
				}
				
				Yii::$app->session->setFlash ( 'success', Yii::t("app", 'Galeria a fost salvată') );
				
				if (Yii::$app->request->post ( 'Redirect' ) == 'list') {
					return $this->redirect ( [ 
							'index' 
					] );
				}
				
				return $this->redirect ( [ 
						'update',
						'id' => $model->ID 
				] );
			}
		}
		
		return $this->render ( 'create', [ 
				'model' => $model,
				'modelLangs' => $modelLangs 
		] );
	}
	
	/**
	 * Updates an existing Gallery model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * 
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel ( $id );
		
		$modelLangs = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $key => $lang ) {
			$modelLangs [$key] = isset ( $model->langs [$lang] ) ? $model->langs [$lang] : new GalleryLang ( [ 
					'LangID' => $lang 
			] );
		}
		
		if (Yii::$app->request->isPost) {
			if ($model->load ( Yii::$app->request->post () ) && $model->validate () && Model::loadMultiple ( $modelLangs, Yii::$app->request->post () ) && Model::validateMultiple ( $modelLangs )) {
				$model->save ();
				
				foreach ( $modelLangs as $ml ) {
					$ml->GalleryID = $model->ID;
					$ml->save ();
				}
				
				Yii::$app->session->setFlash ( 'success', Yii::t("app", 'Galeria a fost salvată'));
				
				if (Yii::$app->request->post ( 'Redirect' ) == 'list') {
					return $this->redirect ( [ 
							'index' 
					] );
				}
				
				return $this->redirect ( [ 
						'update',
						'id' => $model->ID 
				] );
			}
		}
		
		return $this->render ( 'create', [ 
				'model' => $model,
				'modelLangs' => $modelLangs 
		] );
	}
	
	/**
	 * Deletes an existing Gallery model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * 
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel ( $id )->delete ();
		
		return $this->redirect ( [ 
				'index' 
		] );
	}
	
	/**
	 * Finds the Gallery model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * 
	 * @param integer $id        	
	 * @return Gallery the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Gallery::find ()->with ( 'langs' )->where ( [ 
				'ID' => $id 
		] )->one ()) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
}
