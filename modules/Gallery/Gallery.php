<?php

namespace app\modules\Gallery;

/**
 * Gallery module definition class
 */
class Gallery extends \yii\base\Module {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\Gallery\controllers';
	
	/**
	 * @inheritdoc
	 */
	public function init() {
		parent::init ();
		
		// custom initialization code goes here
	}
}
