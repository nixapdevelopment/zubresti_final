<?php

namespace app\modules\Page\controllers;

use Yii;
use app\controllers\BackendController;
use app\models\Page\Page;
use app\models\Page\PageSearch;
use app\models\PageLang\PageLang;
use yii\base\Model;

/**
 * Default controller for the `Page` module
 */
class PageController extends BackendController {
	public function actionIndex() {
		$searchModel = new PageSearch ();
		$dataProvider = $searchModel->search ( Yii::$app->request->queryParams );
		
		return $this->render ( 'index', [ 
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider 
		] );
	}
	public function actionCreate() {
		$model = new Page ();
		
		$pageLangModels = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $i => $lang ) {
			$pageLangModels [$i] = new PageLang ( [ 
					'LangID' => $lang 
			] );
		}
		
		if (Yii::$app->request->isPost) {
			if ($model->load ( Yii::$app->request->post () ) && $model->validate () && Model::loadMultiple ( $pageLangModels, Yii::$app->request->post () ) && Model::validateMultiple ( $pageLangModels )) {
				$model->save ();
				
				foreach ( $pageLangModels as $pageLangModel ) {
					$pageLangModel->PageID = $model->ID;
					$pageLangModel->save ();
				}
				
				Yii::$app->session->setFlash ( 'success', Yii::t("app", 'Pagina a fost salvată') );
				
				return $this->redirect ( [ 
						'update',
						'id' => $model->ID 
				] );
			}
		}
		
		return $this->render ( 'create', [ 
				'model' => $model,
				'pageLangModels' => $pageLangModels 
		] );
	}
	public function actionUpdate($id) {
		$model = $this->findModel ( $id );
		$pageLangModels = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $i => $lang ) {
			$pageLangModels [$i] = isset ( $model->pageLangs [$lang] ) ? $model->pageLangs [$lang] : new PageLang ( [ 
					'LangID' => $lang,
					'PageID' => $model->ID 
			] );
		}
		
		if (Yii::$app->request->isPost) {
			if ($model->load ( Yii::$app->request->post () ) && $model->validate () && Model::loadMultiple ( $pageLangModels, Yii::$app->request->post () ) && Model::validateMultiple ( $pageLangModels )) {
				$model->save ();
				
				foreach ( $pageLangModels as $pageLangModel ) {
					$pageLangModel->save ();
				}
				
				Yii::$app->session->setFlash ( 'success', Yii::t("app", 'Pagina a fost salvată'));
				
				return $this->redirect ( [ 
						'update',
						'id' => $model->ID 
				] );
			}
		}
		
		return $this->render ( 'update', [ 
				'model' => $model,
				'pageLangModels' => $pageLangModels 
		] );
	}
	public function actionDelete($id) {
		$model = $this->findModel ( $id );
		$model->delete ();
		
		Yii::$app->session->setFlash ( 'success', Yii::t("app", 'Pagina a fost ştearsă'));
		
		return $this->redirect ( [ 
				'index' 
		] );
	}
	public function findModel($id) {
		if (($model = Page::find ()->with ( 'pageLangs' )->where ( [ 
				'ID' => $id 
		] )->one ()) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
}