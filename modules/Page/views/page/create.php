<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Page\Page */

$this->title = Yii::t("app", 'Creează pagină');
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t("app", 'Pagini'),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="category-create">

	<h1><?= Html::encode($this->title) ?></h1>

    <?=$this->render ( '_form', [ 'model' => $model,'pageLangModels' => $pageLangModels ] )?>

</div>
