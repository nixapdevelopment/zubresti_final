<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\select2\Select2;
use app\models\Page\Page;

?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div>
        <?=$form->field ( $model, 'Status' )->widget ( Select2::className (), [ 'data' => Page::getStatusList () ] )?>
    </div>
    
    <?php foreach ($pageLangModels as $key => $lmodel) { ?>
    
        <?php
					
$items [] = [ 
							'label' => strtoupper ( Yii::$app->params ['siteLanguages'] [$key] ),
							'content' => $this->render ( '_lang_form', [ 
									'lmodel' => $lmodel,
									'form' => $form,
									'key' => $key 
							] ),
							'active' => $key == 0 
					];
					?>
    
    <?php } ?>
    
    <?=Tabs::widget ( [ 'items' => $items ] )?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t("app", 'Creează') : Yii::t("app", 'Editează'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
