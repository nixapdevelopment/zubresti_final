<?php
use app\core\widgets\TinyMce;

?>

<br />

<?= $form->field($lmodel, "[$key]Title")->textInput(['maxlength' => true]) ?>

<?= $form->field($lmodel, "[$key]Text")->widget(TinyMce::className()) ?>

<?= $form->field($lmodel, "[$key]SeoTitle")->textInput(['maxlength' => true]) ?>

<div class="row">
	<div class="col-md-6">
        <?= $form->field($lmodel, "[$key]Keywords")->textarea() ?>
    </div>
	<div class="col-md-6">
        <?= $form->field($lmodel, "[$key]Description")->textarea() ?>
    </div>
</div>