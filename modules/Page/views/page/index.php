<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Page\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t("app", 'Pagini');
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="category-index">

	<h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t("app", 'Creează pagină'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
        <?=GridView::widget ( [ 'dataProvider' => $dataProvider,// 'filterModel' => $searchModel,'columns' => [ [ 'class' => 'yii\grid\SerialColumn','options' => [ 'width' => '50px' ] ],[ 'label' => 'Name','value' => function ($model) {return $model->currentLang->Title;},'filter' => false ],[ 'label' => 'Text','value' => function ($model) {return mb_substr ( strip_tags ( $model->currentLang->Text ), 0, 80 ) . ' ...';},'filter' => false ],[ 'attribute' => 'Status','filter' => false,'options' => [ 'width' => '130px' ] ],[ 'class' => 'yii\grid\ActionColumn','options' => [ 'width' => '100px' ],'template' => '<div class="text-center">{update}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{delete}</div>' ] ] ] );?>
    <?php Pjax::end(); ?>
</div>