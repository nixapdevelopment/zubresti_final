<?php

namespace app\modules\Article;

use Yii;
use app\core\CoreModule;

/**
 * Article module definition class
 */
class Article extends CoreModule {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\Article\controllers';
	public $name = 'Article';
	
	/**
	 * @inheritdoc
	 */
	public function init() {
		parent::init ();
		
		// custom initialization code goes here
	}
	public function articleTypes() {
		return [ 
				'Page' => [ 
						'labels' => [ 
								'icon' => 'fa fa-file-text-o', // fa or glyphicon full icon name
								'singular' => Yii::t ( "app", 'Pagină' ),
								'plural' => Yii::t ( "app", 'Pagini' ),
								'add' => Yii::t ( "app", 'Adaugă pagina' ),
								'wasSaved' => Yii::t ( "app", 'Pagina a fost salvată' ) 
						],
						'hierarchy' => false,
						'hasChronology' => false,
						'gridViewColumns' => [ 
								'lang.Title',
								[ 
										'label' => Yii::t ( "app", 'Text' ),
										'value' => function ($article) {
											return mb_substr ( strip_tags ( html_entity_decode ( $article->lang->Text ) ), 0, 80 ) . ' ...';
										} 
								],
								'Status' 
						],
						'hasImages' => true,
						'hasFiles' => true 
				],
				'News' => [ 
						'labels' => [ 
								'icon' => 'fa fa-newspaper-o', // fa or glyphicon full icon name
								'singular' => Yii::t ( "app", 'Noutate' ),
								'plural' => Yii::t ( "app", 'Noutăți' ),
								'add' => Yii::t ( "app", 'Adaugă noutate' ),
								'wasSaved' => Yii::t ( "app", 'Noutatea a fost salvată' ) 
						],
						'hierarchy' => false,
						'hasChronology' => true,
						'gridViewColumns' => [ 
								'lang.Title',
								[ 
										'label' => Yii::t ( "app", 'Text' ),
										'value' => function ($article) {
											return mb_substr ( strip_tags ( html_entity_decode ( $article->lang->Text ) ), 0, 80 ) . ' ...';
										} 
								],
								[ 
										'label' => Yii::t ( "app", 'Data' ),
										'value' => function ($article) {
											return date ( 'd.m.Y', strtotime ( $article->Date ) );
										} 
								],
								[ 
										'attribute' => 'Status' 
								] 
						],
						'hasImages' => true,
						'hasFiles' => true 
				],
				// 'Event' => [
				// 'labels' => [
				// 'icon' => 'fa fa-calendar-o',
				// 'singular' => Yii::t("app", 'Eveniment'),
				// 'plural' => Yii::t("app", 'Evenimente'),
				// 'add' => Yii::t("app", 'Adaugă eveniment'),
				// 'wasSaved' => Yii::t("app", 'Evenimentul a fost salvat'),
				// ],
				// 'hierarchy' => false,
				// 'hasChronology' => false,
				// 'gridViewColumns' => [
				// 'lang.Title',
				// [
				// 'label' => Yii::t("app", 'Text'),
				// 'value' => function($article) {
				// return mb_substr(strip_tags(html_entity_decode($article->lang->Text)), 0, 80) . ' ...';
				// }
				// ],
				// 'Status'
				// ],
				// 'hasImages' => true,
				// 'hasFiles' => true,
				// ],
				'Anouncment' => [ 
						'labels' => [ 
								'icon' => 'fa fa-bullhorn',
								'singular' => Yii::t ( "app", 'Anunț' ),
								'plural' => Yii::t ( "app", 'Anunțuri' ),
								'add' => Yii::t ( "app", 'Adaugă anunț' ),
								'wasSaved' => Yii::t ( "app", 'Anunțul a fost salvat' ) 
						],
						'hierarchy' => false,
						'hasChronology' => true,
						'gridViewColumns' => [ 
								'lang.Title',
								[ 
										'label' => Yii::t ( "app", 'Text' ),
										'value' => function ($article) {
											return mb_substr ( strip_tags ( html_entity_decode ( $article->lang->Text ) ), 0, 80 ) . ' ...';
										} 
								],
								[ 
										'label' => Yii::t ( "app", 'Data' ),
										'value' => function ($article) {
											return date ( 'd.m.Y', strtotime ( $article->Date ) );
										} 
								],
								'Status' 
						],
						'hasImages' => true,
						'hasFiles' => true 
				] 
			// 'PublicInformation' => [
			// 'labels' => [
			// 'icon' => 'fa fa-bullhorn',
			// 'singular' => Yii::t("app", 'Informație publică'),
			// 'plural' => Yii::t("app", 'Informații publice'),
			// 'add' => Yii::t("app", 'Adaugă informații publice'),
			// 'wasSaved' => Yii::t("app", 'Informație publică a fost salvată'),
			// ],
			// 'hierarchy' => false,
			// 'hasChronology' => true,
			// 'gridViewColumns' => [
			// 'lang.Title',
			// [
			// 'label' => Yii::t("app", 'Text'),
			// 'value' => function($article) {
			// return mb_substr(strip_tags(html_entity_decode($article->lang->Text)), 0, 80) . ' ...';
			// }
			// ],
			// [
			// 'label' => Yii::t("app", 'Data'),
			// 'value' => function($article) {
			// return date('d.m.Y', strtotime($article->Date));
			// }
			// ],
			// 'Status'
			// ],
			// 'hasImages' => true,
			// 'hasFiles' => true,
			// ],
			// 'Decision' => [
			// 'labels' => [
			// 'icon' => 'fa fa-bullhorn',
			// 'singular' => Yii::t("app", 'Decizie'),
			// 'plural' => Yii::t("app", 'Decizii'),
			// 'add' => Yii::t("app", 'Adaugă decizie'),
			// 'wasSaved' => Yii::t("app", 'Decizia a fost salvată'),
			// ],
			// 'hierarchy' => false,
			// 'hasChronology' => true,
			// 'gridViewColumns' => [
			// 'lang.Title',
			// [
			// 'label' => Yii::t("app", 'Text'),
			// 'value' => function($article) {
			// return mb_substr(strip_tags(html_entity_decode($article->lang->Text)), 0, 80) . ' ...';
			// }
			// ],
			// [
			// 'label' => Yii::t("app", 'Data'),
			// 'value' => function($article) {
			// return date('d.m.Y', strtotime($article->Date));
			// }
			// ],
			// 'Status'
			// ],
			// 'hasImages' => true,
			// 'hasFiles' => true,
			// ],
			// 'Media' => [
			// 'labels' => [
			// 'icon' => 'fa fa-bullhorn',
			// 'singular' => Yii::t("app", 'Media'),
			// 'plural' => Yii::t("app", 'Articole Media'),
			// 'add' => Yii::t("app", 'Adaugă articol Media'),
			// 'wasSaved' => Yii::t("app", 'Media articol a fost salvat'),
			// ],
			// 'hierarchy' => false,
			// 'hasChronology' => true,
			// 'gridViewColumns' => [
			// 'lang.Title',
			// [
			// 'label' => Yii::t("app", 'Text'),
			// 'value' => function($article) {
			// return mb_substr(strip_tags(html_entity_decode($article->lang->Text)), 0, 80) . ' ...';
			// }
			// ],
			// [
			// 'label' => Yii::t("app", 'Data'),
			// 'value' => function($article) {
			// return date('d.m.Y', strtotime($article->Date));
			// }
			// ],
			// 'Status'
			// ],
			// 'hasImages' => true,
			// 'hasFiles' => true,
			// ],
		];
	}
	public function getArticleConfig($articleType) {
		$types = $this->articleTypes ();
		
		if (! isset ( $types [$articleType] )) {
			throw new \yii\base\InvalidConfigException ( "Unknown article type. Please verify configuration in " . __CLASS__ . "::articleTypes()." );
		}
		
		return $types [$articleType];
	}
}
