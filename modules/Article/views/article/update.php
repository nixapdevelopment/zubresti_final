<?php
use yii\helpers\Html;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\Article\Article */

$this->title = Yii::t("app", 'Editare').' ' . strtolower ( $articleConfig ['labels'] ['singular'] ) . ': ' . $model->lang->Title;
$this->params ['breadcrumbs'] [] = [ 
		'label' => $articleConfig ['labels'] ['plural'],
		'url' => $this->context->url ( [ 
				'index' 
		], Yii::$app->request->get ( 'type', 'Page' ) ) 
];
$this->params ['breadcrumbs'] [] = $model->lang->Title;
?>

<div class="article-update">

	<h1><?= Html::encode($this->title) ?></h1>

    <?=Tabs::widget ( [ 'items' => [ [ 'label' => 'General','content' => $this->render ( '_form', [ 'model' => $model,'langModels' => $langModels,'all_articles' => $all_articles,'related_articles' => $related_articles ] ),'active' => true ],[ 'label' => 'Images','content' => $this->render ( '_images_form', [ 'model' => $model ] ) ],[ 'label' => 'Files','content' => $this->render ( '_files_form', [ 'model' => $model ] ) ] ] ] )?>

</div>
