<?php
use yii\bootstrap\Html;
use yii\widgets\ActiveForm;
use yii\bootstrap\Tabs;
use kartik\select2\Select2;
use app\models\Article\Article;
use kartik\widgets\DatePicker;
use yii\helpers\ArrayHelper;

?>

<br />
<div class="article-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
		<div class="col-md-6 <?= $model->Type == 'Page' ? 'hidden' : '' ?>">
            <?=$form->field ( $model, 'Date' )->widget ( DatePicker::className (), [ 'type' => DatePicker::TYPE_COMPONENT_APPEND,'options' => [ 'value' => date ( 'd.m.Y', empty ( $model->Date ) ? time () : strtotime ( $model->Date ) ) ],'pluginOptions' => [ 'autoclose' => true,'format' => 'dd.mm.yyyy','todayHighlight' => true ] ] )?>
        </div>
		<div class="col-md-6">
            <?=$form->field ( $model, 'Status' )->widget ( Select2::className (), [ 'data' => Article::getStatusList () ] )?>
        </div>
	</div>

	<div class="form-group">
		<label class="control-label"><?= Yii::t("app", "Părinte") ?></label>
        <?=Select2::widget ( [ 'name' => 'parents[]','data' => ArrayHelper::map ( $all_articles, 'ID', 'lang.Title' ),'value' => ArrayHelper::map ( $related_articles, 'ParentArticleID', 'ParentArticleID' ),'size' => Select2::LARGE,'options' => [ 'placeholder' => Yii::t("app", 'Selectează părinţi ...'),'multiple' => true ],'pluginOptions' => [ 'allowClear' => true ] ] );?>
    </div>
    
    <?php foreach ($langModels as $key => $lmodel) { ?>
        <?php
					
$items [] = [ 
							'label' => strtoupper ( Yii::$app->params ['siteLanguages'] [$key] ),
							'content' => $this->render ( '_lang_form', [ 
									'lmodel' => $lmodel,
									'form' => $form,
									'key' => $key 
							] ),
							'active' => $key == 0 
					];
					?>
    <?php } ?>
    
    <?=Tabs::widget ( [ 'items' => $items ] )?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t("app", 'Creează') : Yii::t("app", 'Editează'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?> 
        and 
        <?=Html::dropDownList ( 'Redirect', NULL, [ 'list' => Yii::t("app", 'Înapoi la listă'),'remain' => Yii::t("app", 'Rămîne') ], [ 'class' => 'form-control','style' => 'width: 150px; display: inline-block;' ] )?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
