<?php
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
use yii\widgets\ListView;
use yii\data\ActiveDataProvider;
use app\models\ArticleFile\ArticleFile;

$dataProvider = new ActiveDataProvider ( [ 
		'query' => ArticleFile::find ()->with ( 'lang' )->where ( [ 
				'ArticleID' => $model->ID 
		] )->orderBy ( 'Date DESC' ),
		'pagination' => [ 
				'pageSize' => 10 
		] 
] );

?>

<br />

<?php

$form = ActiveForm::begin ( [ 
		'id' => 'article-files-form',
		'options' => [ 
				'enctype' => 'multipart/form-data' 
		] 
] )?>

<div class="row">
	<div class="col-md-4">
        <?=Html::fileInput ( 'files[]', null, [ 'required' => true,'class' => 'form-control','multiple' => true ] )?>
    </div>
	<div class="col-md-3">
        <?=Html::button ( 'Upload', [ 'class' => 'btn btn-success','onclick' => 'sendFiles()' ] )?>
    </div>
</div>

<?php ActiveForm::end() ?>

<hr />


<?php

Pjax::begin ( [ 
		'id' => 'files-list-pjax',
		'enablePushState' => false 
] )?>

    <?=ListView::widget ( [ 'dataProvider' => $dataProvider,'layout' => "{summary}\n<div class=\"list-group\">{items}</div>\n{pager}",'itemView' => '_file_item' ] );?>

<?php Pjax::end() ?>

<?php

\yii\bootstrap\Modal::begin ( [ 
		'id' => 'files-lang-modal' 
] )?>

<?php \yii\bootstrap\Modal::end() ?>

<script>

    function sendFiles(){
        $.ajax({
            url: "<?= Url::to(['/admin/article/article/upload-files', 'ArticleID' => $model->ID]) ?>",
            type: "POST",
            data: new FormData($("#article-files-form")[0]),
            cache: false,
            contentType: false,
            processData: false,
            success: function() {
                $.pjax.reload({container: "#files-list-pjax"});
                $("#article-files-form").trigger("reset");
            }
        });
        return false;
    }
    
    function getFileLang(id)
    {
        $.post("<?= Url::to(['/admin/article/article/get-file-lang']) ?>", {id: id}, function(html){
            $('#files-lang-modal').modal();
            $('#files-lang-modal .modal-body').html(html);
        });
        return false;
    }
    
    function saveFileLangs()
    {
        $.post('<?= Url::to(['/admin/article/article/save-file-lang']) ?>', $('#save-file-langs').serialize(), function(){
            if ( $('#files-lang-modal .has-error').length == 0)
            {
                $('#files-lang-modal').modal('hide');
                $.pjax.reload({container: "#files-list-pjax", timeout: 10000});
            }
        });
        
        return false;
    }

</script>