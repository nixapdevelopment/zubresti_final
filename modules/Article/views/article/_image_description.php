<?php
use yii\bootstrap\Html;
use kartik\form\ActiveForm;
use yii\helpers\Url;

?>
    
<?php

$form = ActiveForm::begin ( [ 
		'id' => 'image-description-form',
		'action' => Url::to ( [ 
				'/admin/article/article/save-image-description' 
		] ) 
] );
?>

    <?php foreach ($langModels as $key => $lmodel) { ?>
        <?= $form->field($lmodel, "[$key]Title")->textInput()->label('Title (' . strtoupper($lmodel->LangID) . ')') ?>
    <?php } ?>

    <?= Html::hiddenInput('imageID', $image->ID) ?>

<div class="text-center">
        <?=Html::button ( 'Update', [ 'id' => 'save-image-description','class' => 'btn btn-success' ] )?>
    </div>

<?php ActiveForm::end(); ?>