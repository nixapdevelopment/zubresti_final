<?php
use yii\bootstrap\ActiveForm;
use kartik\widgets\DatePicker;
use yii\bootstrap\Html;

?>

<?php

$form = ActiveForm::begin ( [ 
		'id' => 'save-file-langs',
		'options' => [ 
				'data-pjax' => '',
				'onsubmit' => 'return saveFileLangs()' 
		] 
] )?>

    <?php foreach ($langsModels as $lm) { ?>
    
        <?= $form->field($lm, "[$lm->LangID]Title")->textInput(['maxlength' => true])->label('Titlu ' . strtoupper($lm->LangID)) ?>

    <?php } ?>     

    <?=$form->field ( $model, 'Date' )->widget ( DatePicker::className (), [ 'type' => DatePicker::TYPE_COMPONENT_APPEND,'options' => [ 'value' => date ( 'd.m.Y', empty ( $model->Date ) ? time () : strtotime ( $model->Date ) ),'required' => true ],'pluginOptions' => [ 'autoclose' => true,'format' => 'dd.mm.yyyy','todayHighlight' => true ] ] )?>

    <?= $form->field($model, 'ID')->hiddenInput()->label(false) ?>

<div>
        <?= Html::submitButton('Update', ['class' => 'btn btn-success']) ?> 
    </div>

<?php ActiveForm::end() ?>