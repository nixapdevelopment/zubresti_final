<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Article\ArticleSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$columns = [ 
		[ 
				'class' => 'yii\grid\SerialColumn',
				'options' => [ 
						'width' => '50px' 
				] 
		] 
];

if (! empty ( $articleConfig ['gridViewColumns'] )) {
	foreach ( $articleConfig ['gridViewColumns'] as $column ) {
		$columns [] = $column;
	}
}

$columns [] = [ 
		'class' => 'yii\grid\ActionColumn',
		'options' => [ 
				'width' => '100px' 
		],
		'template' => '<div class="text-center">{update}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{delete}</div>',
		'buttons' => [ 
				'update' => function ($url, $model) {
					$url = $this->context->url ( [ 
							'/admin/article/article/update',
							'id' => $model->ID 
					], Yii::$app->request->get ( 'type', 'Page' ) );
					return Html::a ( '<span class="glyphicon glyphicon-pencil"></span>', $url, [ 
							'title' => 'Edit',
							'data-pjax' => 0 
					] );
				},
				'delete' => function ($url, $model) {
					
					if ($model->NotRemovable)
						return '';
					
					$url = $this->context->url ( [ 
							'/admin/article/article/delete',
							'id' => $model->ID 
					], Yii::$app->request->get ( 'type', 'Page' ) );
					return Html::a ( '<span class="glyphicon glyphicon-trash text-danger"></span>', $url, [ 
							'title' => 'Delete',
							'data-pjax' => 0,
							'onclick' => "return confirm('Delete?')" 
					] );
				} 
		] 
];

$this->title = $articleConfig ['labels'] ['plural'];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="article-index">

	<h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> ' . $articleConfig['labels']['add'], $this->context->url(['create'], Yii::$app->request->get('type', 'Page')), ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>
        <?=GridView::widget ( [ 'dataProvider' => $dataProvider, 'filterModel' => $searchModel,'columns' => $columns  ] );?>
    <?php Pjax::end(); ?>
</div>