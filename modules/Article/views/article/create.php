<?php
use yii\helpers\Html;
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\Article\Article */

$this->title = Yii::t("app", 'Creează') . ' ' . strtolower ( $articleConfig ['labels'] ['singular'] );
$this->params ['breadcrumbs'] [] = [ 
		'label' => $articleConfig ['labels'] ['plural'],
		'url' => $this->context->url ( [ 
				'index' 
		], Yii::$app->request->get ( 'type', 'Page' ) ) 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>

<div class="article-create">

	<h1><?= Html::encode($this->title) ?></h1>
    
    <?=Tabs::widget ( [ 'items' => [ [ 'label' => Yii::t("app", 'General'),'content' => $this->render ( '_form', [ 'model' => $model,'langModels' => $langModels,'all_articles' => $all_articles,'related_articles' => $related_articles ] ),'active' => true ] ] ] )?>

</div>
