<?php
use kartik\widgets\FileInput;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use yii\bootstrap\Modal;
use kartik\form\ActiveForm;

$initialPreview = [ ];
$initialPreviewConfig = [ ];
foreach ( $model->images as $imageModel ) {
	$initialPreview [] = Url::to ( '/uploads/article/' . $imageModel->Image, true );
	$initialPreviewConfig [] = [ 
			'caption' => empty ( $imageModel->lang->Title ) ? '' : $imageModel->lang->Title,
			'size' => '&nbsp;',
			'url' => Url::to ( [ 
					'/admin/article/article/delete-image' 
			] ),
			'key' => $imageModel->ID 
	];
}

?>

<br />

<?php

Pjax::begin ( [ 
		'id' => 'images-pjax',
		'timeout' => 20000 
] )?>
    <?=FileInput::widget ( [ 'name' => 'Images[]','options' => [ 'multiple' => true,'accept' => '.png,.jpg,.jpeg' ],'pluginOptions' => [ 'initialPreview' => $initialPreview,'initialPreviewAsData' => true,'initialPreviewConfig' => $initialPreviewConfig,'overwriteInitial' => false,'showRemove' => true,'showUpload' => false,'uploadAsync' => false,'uploadUrl' => Url::to ( [ '/admin/article/article/upload-image' ] ),'otherActionButtons' => '<a data-pjax="0" class="btn btn-xs btn-default image-description" {dataKey} title="Image description"><i class="glyphicon glyphicon-text-size text-info"></i></a>','uploadExtraData' => new JsExpression ( "
                function() {
                    return {
                        ArticleID: " . $model->ID . "
                    };
                }
            " ) ],'pluginEvents' => [ 'filesorted' => new yii\web\JsExpression ( "
                function(event, params) {
                    var data = [];
                    $.each(params.stack, function(key, val){
                        data.push(val.key);
                    });
                    $.post('" . Url::to ( [ 'image-sort' ] ) . "', {order: data});
                }
            " ),'filebatchselected' => new yii\web\JsExpression ( "
                function() {
                    $('input[name=\"Images[]\"]').fileinput('upload');
                }
            " ),'filebatchuploadcomplete' => new yii\web\JsExpression ( "
                function() {
                    $.pjax.reload({container:'#images-pjax'});
                }
            " ) ] ] );?>
<?php Pjax::end(); ?>

<?php

Modal::begin ( [ 
		'id' => 'image-description-modal' 
] )?>

<?php Modal::end() ?>

<?php

$this->registerJs ( "
    $(document).on('click', '.image-description', function(event){
        event.preventDefault();
        $.post('" . Url::to ( [ 
		'image-description' 
] ) . "', {imageID: $(this).attr('data-key')}, function(html){
            $('#image-description-modal .modal-body').html(html);
            $('#image-description-modal').modal('show');
        });
    });
" );
?>

<?php

$this->registerJs ( "
    $(document).on('click', '#save-image-description', function(e){
        var form = $(this).closest('form');
        $.post(form.attr('action'), form.serialize(), function(){
            $.pjax.reload({container:'#images-pjax'});
            $('#image-description-modal').modal('hide');
        });
    });
" )?>


<br />