<?php
use yii\helpers\Html;

echo newerton\fancybox\FancyBox::widget ( [ 
		'target' => "a[rel=article-images-$article->ID]",
		'helpers' => true,
		'mouse' => true,
		'config' => [ 
				'maxWidth' => '90%',
				'maxHeight' => '90%',
				'playSpeed' => 7000,
				'padding' => 0,
				'fitToView' => false,
				'width' => '90%',
				'height' => '90%',
				'autoSize' => false,
				'closeClick' => false,
				'openEffect' => 'elastic',
				'closeEffect' => 'elastic',
				'prevEffect' => 'elastic',
				'nextEffect' => 'elastic',
				'closeBtn' => false,
				'openOpacity' => true,
				'helpers' => [ 
						'buttons' => [ ],
						'thumbs' => [ 
								'width' => 68,
								'height' => 60 
						],
						'overlay' => [ 
								'css' => [ 
										'background' => 'rgba(0, 0, 0, 0.8)' 
								] 
						] 
				] 
		] 
] );

?>

<div class="uk-grid">
    <?php foreach ($article->images as $image) { ?>
    <div class="uk-width-1-3">
        <?=Html::a ( Html::img ( $image->thumbLink, [ 'class' => 'img-responsive img-thumbnail','style' => 'margin-bottom:30px;' ] ), $image->imageLink, [ 'rel' => 'article-images-' . $article->ID,'class' => 'fancybox','title' => $image->lang->Title ] )?>
    </div>
    <?php } ?>
</div>

<?= $this->registerCss("#fancybox-thumbs.bottom{bottom:20px;}") ?>