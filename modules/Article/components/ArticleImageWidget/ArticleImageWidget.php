<?php

namespace app\modules\Article\components\ArticleImageWidget;

use yii\base\Widget;

class ArticleImageWidget extends Widget {
	public $article = null;
	public $view = 'images';
	public function init() {
		if ($this->article == null) {
			throw new InvalidParamException ( 'ArticleFileWidget::$article is required' );
		}
		
		parent::init ();
	}
	public function run() {
		return $this->render ( $this->view, [ 
				'article' => $this->article 
		] );
	}
}