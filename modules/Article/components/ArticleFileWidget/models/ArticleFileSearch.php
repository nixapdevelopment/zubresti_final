<?php

namespace app\modules\Article\components\ArticleFileWidget\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ArticleFile\ArticleFile;

/**
 * ArticleSearch represents the model behind the search form about `app\models\Article\Article`.
 */
class ArticleFileSearch extends Model {
	public $Day = null;
	public $Month = null;
	public $Year = null;
	public $ArticleID = null;
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Year',
								'Month',
								'Day' 
						],
						'integer' 
				] 
		];
	}
	public function attributeLabels() {
		return [ 
				'Year' => Yii::t ( 'app', 'Anul' ),
				'Month' => Yii::t ( 'app', 'Lună' ),
				'Day' => Yii::t ( 'app', 'Zi' ) 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	public function search() {
		$query = ArticleFile::find ()->where ( [ 
				'ArticleID' => $this->ArticleID 
		] )->with ( [ 
				'lang' 
		] );
		
		// add conditions that should always apply here
		if ($this->Year) {
			$query->andWhere ( [ 
					"YEAR(Date)" => $this->Year 
			] );
		}
		
		if ($this->Month) {
			$query->andWhere ( [ 
					"MONTH(Date)" => $this->Month 
			] );
		}
		
		if ($this->Month && $this->Day) {
			$query->andWhere ( [ 
					"DAY(Date)" => $this->Day 
			] );
		}
		
		$query->orderBy ( 'Date DESC' );
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'pagination' => [ 
						'pageSize' => 10 
				] 
		] );
		
		return $dataProvider;
	}
}