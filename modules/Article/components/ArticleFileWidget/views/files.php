<?php
use yii\widgets\ActiveForm;
use app\modules\Article\components\ArticleFileWidget\components\DateHelper;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use yii\helpers\Html;

$yearOptions = [ 
		'' => '-' 
];
for($year = date ( 'Y' ); $year >= 2005; $year --) {
	$yearOptions [$year] = $year;
}

$dayOptions = [ 
		'' => '-' 
];
for($day = 1; $day <= 31; $day ++) {
	$dayOptions [$day] = $day;
}

?>

<?php

Pjax::begin ( [ 
		'id' => 'files-pjax',
		'timeout' => 15000 
] )?>
<div>
        <?php if ($useDateFilter) { ?>
        <?php
									
$form = ActiveForm::begin ( [ 
											'id' => 'file-filter-form',
											'options' => [ 
													'data-pjax' => '' 
											],
											'method' => 'get',
											'action' => yii\helpers\Url::current ( [ ] ) 
									] )?>
        <div class="uk-grid">
		<div class="uk-width-1-4">
                <?=$form->field ( $searchModel, 'Year' )->dropDownList ( $yearOptions, [ 'class' => 'uk-select' ] )?>
            </div>
		<div class="uk-width-1-4">
                <?=$form->field ( $searchModel, 'Month' )->dropDownList ( DateHelper::monthsList (), [ 'id' => 'month-select','class' => 'uk-select' ] )?>
            </div>
		<div class="uk-width-1-4">
                <?=$form->field ( $searchModel, 'Day' )->dropDownList ( $dayOptions, [ 'id' => 'day-select','class' => 'uk-select' ] )?>
            </div>
		<div class="uk-width-1-4">
			<div class="form-group">
                    <?= Html::submitButton(Yii::t('app', 'Apply filter'), ['class' => 'btn btn-danger btn-block', 'style' => 'top: 14px; position: relative;']) ?>
                </div>
		</div>
	</div>
        <?php ActiveForm::end() ?>
        <?php } ?>
    </div>

<div>
        <?=ListView::widget ( [ 'dataProvider' => $dataProvider,'itemView' => 'file_item','layout' => "{items}\n<div class=\"clearfix\"></div>{pager}",'options' => [ 'class' => 'list-group' ] ] );?>
    </div>
<?php Pjax::end() ?>

<?php

$this->registerJs ( "
    $(document).on('change', 'select#month-select', function(){
        var daySelect = $('#file-filter-form select#day-select');
        if ($(this).val() == '')
        {
            daySelect.attr('disabled', 'disabled');
        }
        else
        {
            daySelect.removeAttr('disabled');
        }
    });
", yii\web\View::POS_READY )?>