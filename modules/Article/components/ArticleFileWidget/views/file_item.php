<?php
use yii\helpers\Html;
use bl\files\icons\FileIconWidget;

?>

<?php

$iconWidget = FileIconWidget::begin ( [ 
		'useDefaultIcons' => true 
] );
?>

    <?=Html::a ( $iconWidget->getIcon ( $model->File ) . ' ' . (empty ( $model->lang->Title ) ? $model->File : $model->lang->Title), $model->fullLink, [ 'title' => $model->lang->Title,'style' => 'display:block; padding:10px;','target' => '_blank','data-pjax' => 0,'class' => 'list-group-item' ] )?>

<?php FileIconWidget::end() ?>