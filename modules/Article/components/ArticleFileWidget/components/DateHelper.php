<?php

namespace app\modules\Article\components\ArticleFileWidget\components;

use Yii;

class DateHelper {
	public static function monthsList() {
		return [ 
				'' => '-',
				1 => Yii::t ( 'app', 'Ianuarie' ),
				2 => Yii::t ( 'app', 'Februarie' ),
				3 => Yii::t ( 'app', 'Martie' ),
				4 => Yii::t ( 'app', 'Aprilie' ),
				5 => Yii::t ( 'app', 'Mai' ),
				6 => Yii::t ( 'app', 'Iunie' ),
				7 => Yii::t ( 'app', 'Iulie' ),
				8 => Yii::t ( 'app', 'August' ),
				9 => Yii::t ( 'app', 'Septembrie' ),
				10 => Yii::t ( 'app', 'Octombrie' ),
				11 => Yii::t ( 'app', 'Noiembrie' ),
				12 => Yii::t ( 'app', 'Decembrie' ) 
		];
	}
}
