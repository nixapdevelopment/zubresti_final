<?php

namespace app\modules\Article\components\ArticleFileWidget;

use yii\base\Widget;
use app\modules\Article\components\ArticleFileWidget\models\ArticleFileSearch;
use app\models\ArticleFile\ArticleFile;

class ArticleFileWidget extends Widget {
	public $article = null;
	public $query = null;
	public $view = 'files';
	public $useDateFilter = false;
	public function init() {
		if ($this->article == null) {
			throw new InvalidParamException ( 'ArticleImageWidget::$article is required' );
		}
		
		parent::init ();
	}
	public function run() {
		$articleID = $this->article->ID;
		
		if (ArticleFile::find ()->where ( [ 
				'ArticleID' => $articleID 
		] )->count () == 0) {
			return '';
		}
		
		$searchModel = new ArticleFileSearch ( [ 
				'ArticleID' => $articleID 
		] );
		
		$searchModel->load ( \Yii::$app->request->get () );
		
		return $this->render ( $this->view, [ 
				'searchModel' => $searchModel,
				'dataProvider' => $searchModel->search (),
				'useDateFilter' => $this->useDateFilter 
		] );
	}
}