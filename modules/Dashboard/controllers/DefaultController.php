<?php

namespace app\modules\Dashboard\controllers;

use Yii;
use app\controllers\BackendController;

/**
 * Default controller for the `Dashboard` module
 */
class DefaultController extends BackendController {
	public function init() {
		parent::init ();
	}
	public function permissions() {
		return [ 
				'index' => [ 
						'permission' => 'view_dashboard',
						'label' => Yii::t("app", 'Acces in panel') 
				] 
		];
	}
	public function actionIndex() {
		return $this->render ( 'index' );
	}
}
