<?php
use yii\bootstrap\Tabs;

$this->title = Yii::t("app", "Panel");
$this->params ['breadcrumbs'] [] = $this->title;
?>

<div class="Dashboard-default-index">
	<h1><?= $this->context->action->uniqueId ?></h1>

	<!-- Step 1: Create the containing elements. -->
	<section id="embed-api-auth-container"></section>
	<br />
    <?=Tabs::widget ( [ 'items' => [ [ 'label' => 'Google analitics','content' => $this->render ( 'google-analitics' ),'active' => true ] ] ] )?>

    
    <div id="view-selector-container" style="display: none;"></div>


	<!-- Step 2: Load the library. -->
	<script>
        
        (function (w, d, s, g, js, fjs) {
            g = w.gapi || (w.gapi = {});
            g.analytics = {q: [], ready: function (cb) {
                    this.q.push(cb)
                }};
            js = d.createElement(s);
            fjs = d.getElementsByTagName(s)[0];
            js.src = 'https://apis.google.com/js/platform.js';
            fjs.parentNode.insertBefore(js, fjs);
            js.onload = function () {
                g.load('analytics')
            };
        }(window, document, 'script'));
    </script>

	<script>

        gapi.analytics.ready(function () {
            // auth logig
            gapi.analytics.auth.authorize({
                container: 'embed-api-auth-container',
                clientid: '<?= Yii::$app->params['googleAnalitics']['clientId'] ?>'
            });
            
            var viewSelector = new gapi.analytics.ViewSelector({
                container: 'view-selector-container'
            });

            viewSelector.execute();

            var dataChart = new gapi.analytics.googleCharts.DataChart({
                query: {
                    metrics: 'ga:sessions',
                    dimensions: 'ga:date',
                    'start-date': '30daysAgo',
                    'end-date': 'today'
                },
                chart: {
                    container: 'chart-container',
                    type: 'LINE',
                    options: {
                        width: '100%'
                    }
                }
            });

            var dataChart2 = new gapi.analytics.googleCharts.DataChart({
                query: {
                    metrics: 'ga:pageviews,ga:sessionDuration,ga:exits',
                    dimensions: 'ga:source',
                    sort: '-ga:pageviews'
                },
                chart: {
                    container: 'chart-container-2',
                    type: 'LINE',
                    options: {
                        width: '100%'
                    }
                }
            });
            
            viewSelector.on('change', function(ids) {
                dataChart.set({query: {ids: ids}}).execute();
                dataChart2.set({query: {ids: ids}}).execute();
            });
        });
    </script>

</div>
