<?php

namespace app\modules\Dashboard;

use app\core\CoreModule;

class Dashboard extends CoreModule {
	public $controllerNamespace = 'app\modules\Dashboard\controllers';
	public $name = 'Dashboard';
	public function init() {
		parent::init ();
	}
}