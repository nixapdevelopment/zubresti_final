<?php

namespace app\modules\Admin\controllers;

use Yii;
use yii\web\Controller;
use app\models\ArticleFile\ArticleFile;
use app\models\ArticleFileLang\ArticleFileLang;

/**
 * Default controller for the `Admin` module
 */
class DefaultController extends Controller {
	/**
	 * Renders the index view for the module
	 * 
	 * @return string
	 */
	public function actionIndex() {
		return $this->render ( 'index' );
	}
	public function actionParse() {
		exit ();
		set_time_limit ( 0 );
		
		$arr = array (
				0 => array (
						'documente_id' => '2821',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 37 din 12 iunie 2015',
						'documente_descriere' => 'cu privire la demersul CEP al PP „Partidul Socialiştilor din Republica Moldova privind excluderea candidatului la funcţia de consilier în consiliul orăşenesc Soroca pentru alegerile locale generale din 14 iunie 2015.',
						'documente_sursa' => 'Hotărîrea nr. 370001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-12 09:26:33',
						'documente_limba' => '1' 
				),
				1 => array (
						'documente_id' => '2819',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 36 din 06 iunie 2015',
						'documente_descriere' => 'cu privire la acreditarea observatorilor naţionali din partea concurentului electoral Organizaţiei Teritoriale a Partidului Liberal Soroca, în secţiile de votare constituite în circumscripţia electorală Soroca nr. 29/1, în vederea monitorizării alegerilor locale generale din 14 iunie 2015.',
						'documente_sursa' => 'Hotărîrea nr. 360001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-09 15:50:42',
						'documente_limba' => '1' 
				),
				2 => array (
						'documente_id' => '2818',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 35 din 06 iunie 2015',
						'documente_descriere' => 'cu privire la acreditarea observatorilor naţionali din partea concurentului electoral Blocul Electoral „Platforma Populară Europeană din Moldova – Iurie Leancă”, în secţiile de votare constituite în circumscripţia electorală Soroca nr. 29/1, în vederea monitorizării alegerilor locale generale din 14 iunie 2015.',
						'documente_sursa' => 'Hotărîrea nr. 350001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-09 15:50:02',
						'documente_limba' => '1' 
				),
				3 => array (
						'documente_id' => '2817',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 34 din 06 iunie 2015',
						'documente_descriere' => 'Cu privire la modificarea listei de candidaţi la funcţia de consilier în consiliul orăşenesc din partea „PPEM – Iurie Leancă”.',
						'documente_sursa' => 'Hotărîrea nr. 340001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-09 15:49:24',
						'documente_limba' => '1' 
				),
				4 => array (
						'documente_id' => '2816',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 33 din 06 iunie 2015',
						'documente_descriere' => 'Cu privire la modificarea listei de candidaţi la funcţia de consilier în consiliul orăşenesc din partea PP „Mişcarea Populată Antimafie”.',
						'documente_sursa' => 'Hotărîrea nr. 330001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-09 15:48:51',
						'documente_limba' => '1' 
				),
				5 => array (
						'documente_id' => '2815',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 32 din 06 iunie 2015',
						'documente_descriere' => 'Cu privire la modificarea listei de candidaţi la funcţia de consilier în consiliul orăşenesc din partea PLDM.',
						'documente_sursa' => 'Hotărîrea nr. 320001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-09 15:48:14',
						'documente_limba' => '1' 
				),
				6 => array (
						'documente_id' => '2814',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 31 din 06 iunie 2015',
						'documente_descriere' => 'Cu privire la modificarea listei de candidaţi la funcţia de consilier în consiliul orăşenesc din partea PCRM.',
						'documente_sursa' => 'Hotărîrea nr. 310001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-09 15:47:44',
						'documente_limba' => '1' 
				),
				7 => array (
						'documente_id' => '2813',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 30 din 05 iunie 2015',
						'documente_descriere' => 'Cu privire la modificarea listei de candidaţi la funcţia de consilier în consiliul orăşenesc din partea PP „Partidul Nostru”.',
						'documente_sursa' => 'Hotărîrea nr. 300001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-09 15:47:12',
						'documente_limba' => '1' 
				),
				8 => array (
						'documente_id' => '2812',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 29 din 05 iunie 2015',
						'documente_descriere' => 'Cu privire la modificarea listei de candidaţi la funcţia de consilier în consiliul orăşenesc din partea Partidului Socialiştilor din RM.',
						'documente_sursa' => 'Hotărîrea nr. 290001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-09 15:46:24',
						'documente_limba' => '1' 
				),
				9 => array (
						'documente_id' => '2811',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 28 din 05 iunie 2015',
						'documente_descriere' => 'Cu privire la modificarea listei de candidaţi la funcţia de consilier în consiliul orăşenesc din partea PDM.',
						'documente_sursa' => 'Hotărîrea nr. 280001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-09 15:44:31',
						'documente_limba' => '1' 
				),
				10 => array (
						'documente_id' => '2820',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 27 din 05 iunie 2015',
						'documente_descriere' => 'cu privire la demersul Partidului Liberal privind modificarea listei de candidaţi la funcţia de consilier în consiliul orăşenesc Soroca.',
						'documente_sursa' => 'Hotărîrea nr. 270001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-09 10:40:20',
						'documente_limba' => '1' 
				),
				11 => array (
						'documente_id' => '2808',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 25 din 04 iunie 2015',
						'documente_descriere' => 'cu privire la demersul BE al OTS a PDM privind modificarea listei de candidaţi la funcţia de consilier în consiliul orăşenesc Soroca.',
						'documente_sursa' => 'Hotărîrea nr. 250001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-04 10:26:29',
						'documente_limba' => '1' 
				),
				12 => array (
						'documente_id' => '2807',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 24 din 04 iunie 2015',
						'documente_descriere' => 'cu privire la acreditarea observatorilor naţionali din partea concurentului electoral PDM, în secţiile de votare constituite în circumscripţia electorală Soroca nr. 29/1, în vederea monitorizării alegerilor locale generale din 14 iunie 2015.',
						'documente_sursa' => 'Hotărîrea nr. 240001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-04 10:22:48',
						'documente_limba' => '1' 
				),
				13 => array (
						'documente_id' => '2806',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 23 din 04 iunie 2015',
						'documente_descriere' => 'Cu privire la schimbarea numărului de ordine în lista PNL pentru funcţia de consilier în C/o Soroca.',
						'documente_sursa' => 'Hotărîrea nr. 230001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-04 10:21:59',
						'documente_limba' => '0' 
				),
				14 => array (
						'documente_id' => '2805',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 22 din 04 iunie 2015',
						'documente_descriere' => 'cu privire la acreditarea observatorilor naţionali din partea concurentului electoral PCRM, în secţiile de votare constituite în circumscripţia electorală Soroca nr. 29/1, în vederea monitorizării alegerilor locale generale din 14 iunie 2015.',
						'documente_sursa' => 'Hotărîrea nr. 220001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-04 10:16:12',
						'documente_limba' => '1' 
				),
				15 => array (
						'documente_id' => '2804',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 21 din 02 iunie 2015',
						'documente_descriere' => 'Cu privire la substituirea candidaţilor la funcţia de consilier în consiliul orăşenesc Soroca din partea PNL.',
						'documente_sursa' => 'Hotărîrea nr. 210001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-02 14:59:22',
						'documente_limba' => '1' 
				),
				16 => array (
						'documente_id' => '2803',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 20 din 02 iunie 2015',
						'documente_descriere' => 'Cu privire la acreditarea observatorilor în b/e ale secţiilor de votare din or. Soroca.',
						'documente_sursa' => 'Hotărîrea nr. 200001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-02 14:58:40',
						'documente_limba' => '1' 
				),
				17 => array (
						'documente_id' => '2802',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 19 din 02 iunie 2015',
						'documente_descriere' => 'Cu privire la modificarea listei de candidaţi la funcţia de consilier în consiliul orăşenesc din partea PP „Partidul Nostru”.',
						'documente_sursa' => 'Hotărîrea nr. 190001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-02 14:57:06',
						'documente_limba' => '1' 
				),
				18 => array (
						'documente_id' => '2801',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 18 din 02 iunie 2015',
						'documente_descriere' => 'Cu privire la înregistrarea persoanelor de încredere din partea candidatului independent Onică Boris.',
						'documente_sursa' => 'Hotărîrea nr. 180001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-02 14:56:30',
						'documente_limba' => '1' 
				),
				19 => array (
						'documente_id' => '2800',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 17 din 02 iunie 2015',
						'documente_descriere' => 'Cu privire la retragerea din lista candidaţilor PP „Mişcarea Populară Antimafie” pentru funcţia de consilier a d-nei Raţa Ina şi substituirea candidaturii dlui Chiriliuc Mihail cu candidatura dlui Sulima Vasile.',
						'documente_sursa' => 'Hotărîrea nr.17din02 iunie20150001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-02 14:55:53',
						'documente_limba' => '1' 
				),
				20 => array (
						'documente_id' => '2799',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 16 din 02 iunie 2015',
						'documente_descriere' => 'Cu privire la modificarea listei candidaţilor la funcţia de consilier în consiliul orăşenesc din partea PP „Mişcarea Populară Antimafie”.',
						'documente_sursa' => 'Hotărîrea nr. 160001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-06-02 14:55:16',
						'documente_limba' => '1' 
				),
				21 => array (
						'documente_id' => '2798',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 15 din 22 mai 2015',
						'documente_descriere' => 'Cu privire la aprobarea textului buletinului de vot la alegerile locale generale din 14 iunie 2015.',
						'documente_sursa' => 'Hotărîrea nr. 150001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-22 13:47:19',
						'documente_limba' => '1' 
				),
				22 => array (
						'documente_id' => '2797',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 14 din 22 mai 2015',
						'documente_descriere' => 'Cu privire la aprobarea textului buletinului de vot la alegerile locale generale din 14 iunie 2015.',
						'documente_sursa' => 'Hotărîrea nr. 140001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-22 13:46:27',
						'documente_limba' => '1' 
				),
				23 => array (
						'documente_id' => '2796',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 13 din 22 mai 2015',
						'documente_descriere' => 'Cu privire la stabilirea ordinii de înscriere a concurenţilor electorali în buletinul de vot pentru alegerile locale generale din 14 iunie 2015.',
						'documente_sursa' => 'hotărîrea nr. 130001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-22 13:45:36',
						'documente_limba' => '1' 
				),
				24 => array (
						'documente_id' => '2795',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 12 din 22 mai 2015',
						'documente_descriere' => 'Cu privire la stabilirea ordinii de înscriere a concurenţilor electorali în buletinul de vot pentru alegerile locale generale din 14 iunie 2015.',
						'documente_sursa' => 'Hotărîrea nr. 120001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-22 13:44:25',
						'documente_limba' => '1' 
				),
				25 => array (
						'documente_id' => '2793',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 11 din 21 mai 2015',
						'documente_descriere' => 'Cu privire la completarea listei candidaţilor la funcţia de consilier în consiliul orăşenesc Soroca din partea Partidului Politic „Partidul Nostru”.',
						'documente_sursa' => 'Hotărîrea nr. 110001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-21 10:02:38',
						'documente_limba' => '1' 
				),
				26 => array (
						'documente_id' => '2792',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 10 din 21 mai 2015',
						'documente_descriere' => 'Cu privire la înregistrarea candidaţilor la funcţia de consilier în consiliul orăşenesc Soroca.',
						'documente_sursa' => 'Hotărîrea nr. 100001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-21 10:01:44',
						'documente_limba' => '1' 
				),
				27 => array (
						'documente_id' => '2791',
						'documente_categorie' => '41',
						'documente_titlu' => 'HOTĂRÎREA nr. 9 din 21 mai 2015',
						'documente_descriere' => 'Cu privire la înregistrarea candidaţilor la funcţia de primar al or. Soroca.',
						'documente_sursa' => 'Hotărîrea nr. 90001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-21 10:00:44',
						'documente_limba' => '1' 
				),
				28 => array (
						'documente_id' => '2790',
						'documente_categorie' => '41',
						'documente_titlu' => 'Hotărîrea nr. 8 din 19 mai 2015',
						'documente_descriere' => 'Cu privire la înregistrarea candidaților la funcția de consilier în consiliul orășenesc Soroca',
						'documente_sursa' => 'hotărîre nr. 80001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-19 08:57:20',
						'documente_limba' => '1' 
				),
				29 => array (
						'documente_id' => '2789',
						'documente_categorie' => '41',
						'documente_titlu' => 'Hotărîrea nr. 7 din 19 mai 2015',
						'documente_descriere' => 'Cu privire la înregistrarea candidaților la funcția de primar al or. Soroca',
						'documente_sursa' => 'Hotărîrea nr. 70001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-19 08:55:02',
						'documente_limba' => '1' 
				),
				30 => array (
						'documente_id' => '2783',
						'documente_categorie' => '41',
						'documente_titlu' => 'Hotărîrea nr. 6',
						'documente_descriere' => 'Cu privire la înregistrarea candidaților la funcția de consilier în consiliul orășenesc Soroca',
						'documente_sursa' => 'hotarire60001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-12 15:23:16',
						'documente_limba' => '1' 
				),
				31 => array (
						'documente_id' => '2782',
						'documente_categorie' => '41',
						'documente_titlu' => 'Hotărîre nr. 5',
						'documente_descriere' => 'Cu privire la înregistrarea candidaților la funcția de primar al or. Soroca',
						'documente_sursa' => 'hotirire50001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-12 15:21:13',
						'documente_limba' => '0' 
				),
				32 => array (
						'documente_id' => '2788',
						'documente_categorie' => '41',
						'documente_titlu' => 'Dispoziție nr. 84 din 06 mai 2015',
						'documente_descriere' => 'Cu privire la secțiile de votare din orașul Soroca',
						'documente_sursa' => 'dispoziție20001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-06 08:23:13',
						'documente_limba' => '1' 
				),
				33 => array (
						'documente_id' => '2787',
						'documente_categorie' => '41',
						'documente_titlu' => 'Dispoziție nr. 83 din 06 mai 2015',
						'documente_descriere' => 'Cu privire la stabilirea locurilor de afișaj electoral',
						'documente_sursa' => 'Dispoziție0001.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-06 08:19:54',
						'documente_limba' => '1' 
				),
				34 => array (
						'documente_id' => '2780',
						'documente_categorie' => '41',
						'documente_titlu' => 'Hotărîrea nr.3',
						'documente_descriere' => 'Cu privire la constituirea comisiei de tragere la sorţi',
						'documente_sursa' => 'hotărîre3.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-05 14:11:31',
						'documente_limba' => '1' 
				),
				35 => array (
						'documente_id' => '2779',
						'documente_categorie' => '41',
						'documente_titlu' => 'Hotărtîrea nr.1a/2',
						'documente_descriere' => 'Cu privire la stabilirea locului şi timpului primirii documentelor necesare pentru înregistrarea în calitate de concurent electoral de către Consiliul electoral al circumscripţiei electorale orăşeneşti Soroca nr. 29/1 la alegerile locale generale din 14 iunie 2015.',
						'documente_sursa' => 'hotărîre1a.2.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-05 13:53:24',
						'documente_limba' => '1' 
				),
				36 => array (
						'documente_id' => '2781',
						'documente_categorie' => '41',
						'documente_titlu' => 'Hotărîrea nr. 1a/1',
						'documente_descriere' => 'Cu privire la propunerea pentru degrevarea de atribuţiile de la locul de muncă permanent',
						'documente_sursa' => 'hotărîre1a.1.pdf',
						'documente_afisari' => '0',
						'documente_data' => '2015-05-05 13:00:37',
						'documente_limba' => '1' 
				) 
		);
		
		foreach ( $arr as $item ) {
			$file = new ArticleFile ( [ 
					'ArticleID' => 50,
					'File' => $item ['documente_sursa'],
					'Type' => 'Default',
					'Date' => $item ['documente_data'],
					'Position' => 0 
			] );
			$file->save ();
			
			foreach ( Yii::$app->params ['siteLanguages'] as $lang ) {
				$fileLang = new ArticleFileLang ( [ 
						'ArticleFileID' => $file->ID,
						'LangID' => $lang,
						'Title' => $item ['documente_titlu'],
						'Text' => $item ['documente_descriere'] 
				] );
				$fileLang->save ();
			}
		}
	}
}
