<?php

namespace app\modules\Admin\components\AdminMenuWidget;

use yii\base\Widget;
use app\modules\Article\Article;

class AdminMenuWidget extends Widget {
	public function run() {
		$articleModule = new Article ( 'article' );
		
		$articleTypes = $articleModule->articleTypes ();
		
		return $this->render ( 'index', [ 
				'articleTypes' => $articleTypes 
		] );
	}
}