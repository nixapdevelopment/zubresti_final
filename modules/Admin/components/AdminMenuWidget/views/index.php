<?php
use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use app\models\Gallery\Gallery;

$mainItems = [ ];
$quickAddItems = [ ];
foreach ( $articleTypes as $type => $articleType ) {
	$mainItems [] = [ 
			'label' => $articleType ['labels'] ['plural'],
			'url' => [ 
					'/admin/article',
					'type' => $type 
			] 
	];
	$quickAddItems [] = [ 
			'label' => '<i style="width:20px;text-align: center" class="' . $articleType ['labels'] ['icon'] . '" aria-hidden="true"></i> ' . $articleType ['labels'] ['add'],
			'url' => [ 
					'/admin/article/article/create',
					'type' => $type,
					'hash' => sha1 ( microtime () ) 
			],
			'encode' => false 
	];
}

$mainItemsGroup [] = [ 
		'label' => Yii::t("app", 'Conținut'),
		'items' => $mainItems 
];

NavBar::begin ( [ 
		'brandLabel' => 'Nixap CMS',
		'brandUrl' => yii\helpers\Url::to ( '/admin' ),
		'options' => [ 
				'class' => 'navbar-inverse navbar-fixed-top' 
		] 
] );

if (Yii::$app->user->isGuest) {
	$menuItems = [ 
			[ 
					'label' => 'Login',
					'url' => [ 
							'/admin/user/login' 
					] 
			] 
	];
} else {
	$leftItems = [ 
			[ 
					'label' => Yii::t ( 'app', 'Panel' ),
					'url' => [ 
							'/admin/dashboard' 
					] 
			],
			[ 
					'label' => Yii::t ( 'app', 'Adaugă rapid' ),
					'items' => $quickAddItems 
			] 
	];
	$rightItems = [ 
			[ 
					'label' => Yii::t ( 'app', 'Meniu' ),
					'url' => [ 
							'/admin/menu' 
					] 
			],
			[ 
					'label' => Yii::t ( 'app', 'Slider' ),
					'url' => [ 
							'/admin/slider' 
					] 
			],
			[ 
					'label' => Yii::t ( 'app', 'Galerii' ),
					'items' => [ 
							[ 
									'label' => Yii::t ( 'app', 'Foto galerie' ),
									'url' => [ 
											'/admin/gallery',
											'type' => Gallery::TypeDefault 
									] 
							],
							[ 
									'label' => Yii::t ( 'app', 'Video galerie' ),
									'url' => [ 
											'/admin/gallery',
											'type' => Gallery::TypeVideo 
									] 
							] 
					] 
			],
			[ 
					'label' => Yii::t ( 'app', 'Feedback' ),
					'url' => [ 
							'/admin/feedback' 
					] 
			],
			[ 
					'label' => Yii::t ( 'app', 'Setările' ),
					'url' => [ 
							'/admin/settings' 
					] 
			],
			[ 
					'label' => Yii::t ( 'app', 'Logout' ),
					'url' => [ 
							'/admin/user/login/logout' 
					] 
			] 
	];
	
	$menuItems = yii\helpers\ArrayHelper::merge ( $leftItems, $mainItemsGroup, $rightItems );
}

echo Nav::widget ( [ 
		'options' => [ 
				'class' => 'navbar-nav navbar-right' 
		],
		'items' => $menuItems 
] );
NavBar::end ();
?>