<?php

namespace app\modules\Country;

/**
 * Country module definition class
 */
class Country extends \yii\base\Module {
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\Country\controllers';
	
	/**
	 * @inheritdoc
	 */
	public function init() {
		parent::init ();
		
		// custom initialization code goes here
	}
}
