<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Country\Country */

$this->title = Yii::t ( "app", "Creează țară" );
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', "Țări" ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="country-create">

	<h1><?= Html::encode($this->title) ?></h1>

    <?=$this->render ( '_form', [ 'model' => $model,'langsModels' => $langsModels ] )?>

</div>
