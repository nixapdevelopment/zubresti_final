<?php
use yii\bootstrap\Tabs;

/* @var $this yii\web\View */
/* @var $model app\models\Country\Country */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="country-form">
    
    <?=Tabs::widget ( [ 'items' => [ [ 'label' => Yii::t ( "app", "General" ),'content' => $this->render ( '_country_form', [ 'model' => $model,'langsModels' => $langsModels ] ) ],[ 'label' => Yii::t ( "app", "Regiuni" ),'content' => $this->render ( '_regions', [ 'model' => $model,'countryRegionsDataProvider' => $countryRegionsDataProvider ] ) ] ] ] );?>

</div>