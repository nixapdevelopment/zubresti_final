<?php

use yii\widgets\Pjax;
use app\core\CoreGridView;
use yii\helpers\Url;

?>

<br />

<?php if (!empty($countryRegionsDataProvider)) { ?>

<?php Pjax::begin(['id' => 'countryRegions']) ?>

<?= \yii\helpers\Html::a('<i class="glyphicon glyphicon-plus"></i> '.Yii::t("app", "Adaugă regiune"), Url::to(['/admin/country/region/edit', 'id' => 0, 'countryID' => $model->ID], true), [
    'title' => Yii::t("app", "Adaugă regiune"),
    'class' => 'edit-region btn btn-success',
    'data-pjax' => true
]); ?>

<hr />

<?= CoreGridView::widget([
    'dataProvider' => $countryRegionsDataProvider,
    'columns' => [
        [
            'class' => 'yii\grid\SerialColumn',
            'options' => [
                'width' => '50px'
            ]
        ],
        [
            'label' => Yii::t("app", "Nume"),
            'value' => function($model)
            {
                return implode(' | ', \yii\helpers\ArrayHelper::getColumn($model->countryRegionLangs, 'Name'));
            }
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'buttons' => [
                'edit' => function($url, $model)
                {
                    return \yii\helpers\Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['/admin/country/region/edit', 'id' => $model->ID], true), [
                        'title' => Yii::t("app", "Editează"),
                        'class' => 'edit-region',
                        'data-pjax' => true
                    ]);
                },
                'delete' => function($url, $model)
                {
                    return \yii\helpers\Html::a('<span class="glyphicon glyphicon-trash text-danger"></span>', Url::to(['/admin/country/region/delete', 'id' => $model->ID], true), [
                        'title' => Yii::t("app", "Șterge"),
                        'class' => 'delete-region',
                        'data-pjax' => true
                    ]);
                },
            ],
            'template'=>'{edit}&nbsp;&nbsp;&nbsp;{delete}',
            'options' => [
                'width' => '100px'
            ]
        ],
    ],
]); ?>
<?php Pjax::end() ?>

<?php \yii\bootstrap\Modal::begin([
    'header' => '<b>'.Yii::t("app", "Editare regiune").'</b>',
    'id' => 'edit-region-modal',
    'size' => \yii\bootstrap\Modal::SIZE_LARGE
])?>
<?php \yii\bootstrap\Modal::end()?>

<?php echo $this->registerJs("
    $(document).on('click', '.edit-region', function(e){
        e.preventDefault();
        var modalContent = $('#edit-region-modal').modal('show').find('.modal-body');
        $.get($(this).attr('href'), {}, function(html){
            modalContent.html(html);
        });
    });
"); ?>

<?php echo $this->registerJs("
    $(document).on('click', '.delete-region', function(e){
        e.preventDefault();
        $.get($(this).attr('href'), {}, function(){
            $.pjax.reload({container:'#countryRegions'});
        });
    });
"); ?>

<?php } ?>