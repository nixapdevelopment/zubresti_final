<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>

<br />

<?php $form = ActiveForm::begin(); ?>

<div class="row">
	<div class="col-md-6">
        <?= $form->field($model, 'Code')->textInput(['maxlength' => true]) ?>
    </div>
	<div class="col-md-6">
        <?= $form->field($model, 'Link')->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="row">
    <?php foreach ($langsModels as $i => $lmodel) { ?>
    <div class="col-md-4">
        <?= $form->field($lmodel, "[$i]Name")->textInput(['maxlength' => true])->label('Name (' . strtoupper($lmodel->LangID) . ')') ?>
    </div>
    <?php } ?>
</div>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t("app", "Creează") : Yii::t("app", "Editează"), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>