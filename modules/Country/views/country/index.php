<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Country\CountrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t ( 'app', "Țări" );
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="country-index">

	<h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t("app", "Creează țară"), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    
<?=GridView::widget ( [ 'dataProvider' => $dataProvider,// 'filterModel' => $searchModel,'columns' => [ [ 'class' => 'yii\grid\SerialColumn','options' => [ 'width' => '50px;' ] ],[ 'attribute' => 'Code','filter' => false,'options' => [ 'width' => '50px;' ] ],[ 'label' => Yii::t ( "app", "Flag" ),'value' => function ($model) {return \modernkernel\flagiconcss\Flag::widget ( [ 'tag' => 'span','country' => strtolower ( $model->Code ),'squared' => false,'options' => [ 'style' => 'border: 1px solid #ccc;' ] ] );},'format' => 'html','options' => [ 'width' => '30px;' ] ],[ 'label' => Yii::t ( "app", 'Nume' ),'attribute' => 'countryLangs.Name','value' => function ($model) {return implode ( ' | ', \yii\helpers\ArrayHelper::map ( $model->countryLangs, 'LangID', 'Name' ) );} ],[ 'class' => 'yii\grid\ActionColumn','options' => [ 'width' => '100px;','style' => 'text-align:center;' ] ] ] ] );?>
<?php Pjax::end(); ?></div>
