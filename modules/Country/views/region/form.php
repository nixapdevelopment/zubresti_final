<?php
use yii\widgets\ActiveForm;
use yii\bootstrap\Html;
use yii\widgets\Pjax;

?>

<?php

Pjax::begin ( [ 
		'id' => 'regionForm-' . $uid,
		'enablePushState' => false 
] )?>
<?php $form = ActiveForm::begin(['options' => ['data-pjax' => 1, 'id' => $uid]]); ?>

<div class="row">
    <?php foreach ($langModels as $i => $lmodel) { ?>
    <div class="col-md-4">
        <?= $form->field($lmodel, "[$i]Name")->textInput(['maxlength' => true])->label('Name (' . strtoupper($lmodel->LangID) . ')') ?>
    </div>
    <?php } ?>
</div>

<div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t("app", "Creează") : Yii::t("app", "Editează"), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
</div>

<?php ActiveForm::end(); ?>
<?php Pjax::end() ?>

<?php

$this->registerJs ( '
    $("document").ready(function(){ 
        $("#' . 'regionForm-' . $uid . '").on("pjax:end", function() {
            if ($("#edit-region-modal .has-error").length == 0)
            {
                $("#edit-region-modal").modal("hide");
                $.pjax.reload({container:"#countryRegions"});
            }
        });
    });
' );
?>