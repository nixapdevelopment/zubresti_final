<?php

namespace app\modules\Country\controllers;

use Yii;
use yii\base\Model;
use app\models\CountryRegion\CountryRegion;
use app\models\CountryRegionLang\CountryRegionLang;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class RegionController extends \app\controllers\BackendController {
	public function actionEdit($id = 0, $countryID = null) {
		$model = CountryRegion::find ()->where ( [ 
				'ID' => $id 
		] )->with ( 'countryRegionLangs' )->one ();
		
		if ($model != null) {
			$langModels = $model->countryRegionLangs;
		} else {
			$langModels = [ ];
			foreach ( \Yii::$app->params ['siteLanguages'] as $i => $lang ) {
				$langModels [$i] = new CountryRegionLang ( [ 
						'LangID' => $lang 
				] );
			}
		}
		
		if (Yii::$app->request->isPost) {
			if (Model::loadMultiple ( $langModels, \Yii::$app->request->post () ) && Model::validateMultiple ( $langModels )) {
				if (empty ( $model->ID )) {
					$model = new CountryRegion ( [ 
							'CountryID' => $countryID 
					] );
					$model->save ();
				}
				
				foreach ( $langModels as $i => $lmodel ) {
					$lmodel->CountryRegionID = $model->ID;
					$lmodel->LangID = Yii::$app->params ['siteLanguages'] [$i];
					$lmodel->save ();
				}
			}
		}
		
		return $this->renderAjax ( 'form', [ 
				'model' => $model,
				'langModels' => $langModels,
				'uid' => uniqid () 
		] );
	}
	public function actionDelete($id) {
		return CountryRegion::findOne ( $id )->delete ();
	}
}