<?php

namespace app\modules\Country\controllers;

use Yii;
use app\models\Country\Country;
use app\models\Country\CountrySearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\CountryLang\CountryLang;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * CountryController implements the CRUD actions for Country model.
 */
class CountryController extends \app\controllers\BackendController {
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [ 
				'verbs' => [ 
						'class' => VerbFilter::className (),
						'actions' => [ 
								'delete' => [ 
										'POST' 
								] 
						] 
				] 
		];
	}
	
	/**
	 * Lists all Country models.
	 * 
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new CountrySearch ();
		$dataProvider = $searchModel->search ( Yii::$app->request->queryParams );
		
		return $this->render ( 'index', [ 
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider 
		] );
	}
	
	/**
	 * Displays a single Country model.
	 * 
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionView($id) {
		return $this->render ( 'view', [ 
				'model' => $this->findModel ( $id ) 
		] );
	}
	
	/**
	 * Creates a new Country model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * 
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Country ();
		
		$langsModels = [ ];
		foreach ( \Yii::$app->params ['siteLanguages'] as $i => $langKey ) {
			$langsModels [$i] = new CountryLang ();
		}
		
		if ($model->load ( Yii::$app->request->post () ) && $model->validate ()) {
			if (Model::loadMultiple ( $langsModels, \Yii::$app->request->post () ) && Model::validateMultiple ( $langsModels )) {
				$model->save ();
				
				foreach ( $langsModels as $i => $lmodel ) {
					$lmodel->CountryID = $model->ID;
					$lmodel->LangID = \Yii::$app->params ['siteLanguages'] [$i];
					$lmodel->save ();
				}
				
				\Yii::$app->session->setFlash ( 'success', Yii::t ( "app", "Țara a fost salvată" ) );
				
				return $this->redirect ( [ 
						'update',
						'id' => $model->ID 
				] );
			}
		}
		
		return $this->render ( 'create', [ 
				'model' => $model,
				'langsModels' => $langsModels 
		] );
	}
	
	/**
	 * Updates an existing Country model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * 
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionUpdate($id) {
		$model = $this->findModel ( $id );
		
		$langsModels = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $i => $lang ) {
			$langsModels [$i] = isset ( $model->countryLangs [$lang] ) ? $model->countryLangs [$lang] : new CountryLang ( [ 
					'LangID' => $lang,
					'CountryID' => $model->ID 
			] );
		}
		
		$countryRegionsDataProvider = new ActiveDataProvider ( [ 
				'query' => \app\models\CountryRegion\CountryRegion::find ()->where ( [ 
						'CountryID' => $id 
				] )->with ( 'countryRegionLangs' ),
				'pagination' => [ 
						'pageSize' => 10 
				] 
		] );
		
		if ($model->load ( Yii::$app->request->post () ) && $model->validate ()) {
			if (Model::loadMultiple ( $langsModels, \Yii::$app->request->post () ) && Model::validateMultiple ( $langsModels )) {
				$model->save ();
				
				foreach ( $langsModels as $i => $lmodel ) {
					$lmodel->CountryID = $model->ID;
					$lmodel->LangID = \Yii::$app->params ['siteLanguages'] [$i];
					$lmodel->save ();
				}
				
				\Yii::$app->session->setFlash ( 'success', Yii::t ( "app", "Țara a fost salvată" ) );
				
				return $this->redirect ( [ 
						'update',
						'id' => $model->ID 
				] );
			}
		}
		
		return $this->render ( 'update', [ 
				'model' => $model,
				'langsModels' => $langsModels,
				'countryRegionsDataProvider' => $countryRegionsDataProvider 
		] );
	}
	
	/**
	 * Deletes an existing Country model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * 
	 * @param integer $id        	
	 * @return mixed
	 */
	public function actionDelete($id) {
		$this->findModel ( $id )->delete ();
		
		return $this->redirect ( [ 
				'index' 
		] );
	}
	
	/**
	 * Finds the Country model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * 
	 * @param integer $id        	
	 * @return Country the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if (($model = Country::findOne ( $id )) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
}