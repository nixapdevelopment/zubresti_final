<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\Feedback\FeedbackSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t ( 'app', 'Feedback-uri' );
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="feedback-index">

	<h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>    
        <?=GridView::widget ( [ 'dataProvider' => $dataProvider,'filterModel' => $searchModel,'columns' => [ [ 'class' => 'yii\grid\SerialColumn' ],'Name','Phone','Email:email',[ 'attribute' => 'Date','value' => function ($model) {return date ( 'd.m.Y H:i', strtotime ( $model->Date ) );} ],[ 'class' => 'yii\grid\ActionColumn','template' => '{view}&nbsp;&nbsp;&nbsp;&nbsp;{delete}' ] ] ] );?>
    <?php Pjax::end(); ?>

</div>