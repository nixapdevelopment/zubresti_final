<?php
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Feedback\Feedback */

$this->title = $model->Name;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t ( 'app', 'Feedback-uri' ),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="feedback-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
        <?=Html::a ( '<i class="fa fa-arrow-left" aria-hidden="true"></i> &nbsp;' . Yii::t ( 'app', 'Înapoi la listă' ), [ 'index' ], [ 'class' => 'btn btn-info' ] )?>
        &nbsp;&nbsp;
        <?=Html::a ( '<i class="fa fa-trash"></i> &nbsp;' . Yii::t ( 'app', 'Șterge' ), [ 'delete','id' => $model->ID ], [ 'class' => 'btn btn-danger','data' => [ 'confirm' => Yii::t ( 'app', 'Are you sure you want to delete this item?' ),'method' => 'post' ] ] )?>
    </p>

    <?=DetailView::widget ( [ 'model' => $model,'attributes' => [ 'Name','Phone','Email:email','Message:ntext','Date' ] ] )?>

</div>
