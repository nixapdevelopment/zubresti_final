<?php
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

?>


<?php Pjax::begin() ?>
    
    <?php if (Yii::$app->session->getFlash('feedbackSend', false)) { ?>

        <?=Html::tag ( 'div', Yii::t ( 'app', 'Feedback send' ), [ 'class' => 'alert alert-success' ] )?>

    <?php } else { ?>

        <?php
					
$form = ActiveForm::begin ( [ 
							'id' => 'feedback-form',
							'options' => [ 
									'data-pjax' => '' 
							],
							'fieldConfig' => [ 
									'template' => '<label>{input}<span style="color:tomato;">{error}</span></label>' 
							] 
					] );
					?>
<div class="uk-grid">
	<div class="uk-width-1-2">
                    
                    <?= $form->field($model, 'Name')->textInput(['placeholder' => $model->getAttributeLabel('Name')]) ?>
                    
                    <?= $form->field($model, 'Phone')->textInput(['placeholder' => $model->getAttributeLabel('Phone')]) ?>
                    
                    <?= $form->field($model, 'Email')->textInput(['placeholder' => $model->getAttributeLabel('Email')]) ?>
                    
                </div>
	<div class="uk-width-1-2">
                    
                    <?= $form->field($model, 'Subject')->textInput(['placeholder' => $model->getAttributeLabel('Subject')]) ?>
                    
                    <?= $form->field($model, 'Message')->textarea(['placeholder' => $model->getAttributeLabel('Message')]) ?>
                    
                </div>
</div>
<div class="uk-width-1-1">
	<div class="button">
                    <?= Html::submitButton(Yii::t('app', 'Trimite'), ['class' => 'btn']) ?> 
                </div>
</div>
<?php ActiveForm::end(); ?>
    
    <?php } ?>
    
<?php Pjax::end() ?>