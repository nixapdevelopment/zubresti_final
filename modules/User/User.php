<?php

namespace app\modules\User;

use app\core\CoreModule;

class User extends CoreModule {
	public $controllerNamespace = 'app\modules\User\controllers';
	public $name = 'User module';
	public function init() {
		parent::init ();
	}
}
