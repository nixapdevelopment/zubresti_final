<?php

namespace app\modules\User\controllers;

use Yii;
use app\models\User\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use app\models\User\LoginForm;
use app\models\User\PinForm;
use yii\helpers\Url;

/**
 * UserController implements the CRUD actions for User model.
 */
class LoginController extends Controller {
	public function actionIndex() {
		if (! Yii::$app->user->isGuest) {
			$this->redirect ( Url::toRoute ( [ 
					'/admin/dashboard' 
			] ) );
		}
		
		$model = new LoginForm ();
		if ($model->load ( Yii::$app->request->post () ) && $model->login ()) {
			$this->redirect ( '/admin/dashboard' );
		}
		
		return $this->render ( 'login', [ 
				'model' => $model 
		] );
	}
	public function actionLogout() {
		Yii::$app->user->logout ();
		
		return $this->goHome ();
	}
	protected function findModel($id) {
		if (($model = User::findOne ( $id )) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
}
