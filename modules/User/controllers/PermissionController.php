<?php

namespace app\modules\User\controllers;

use Yii;
use app\controllers\BackendController;
use app\models\UserPermission\UserPermission;

class PermissionController extends BackendController {
	public function permissions() {
		return [ 
				'index' => [ 
						'permission' => 'change_permissions',
						'label' => Yii::t('app',"Setare permisiuni") 
				] 
		];
	}
	public function actionIndex() {
		$userID = Yii::$app->request->get ( 'userID' );
		
		if (empty ( $userID )) {
			throw new \yii\web\NotFoundHttpException ( 'User not found' );
		}
		
		if (Yii::$app->request->isPost) {
			$permissionsToSave = Yii::$app->request->post ( 'permissions', [ ] );
			
			UserPermission::deleteAll ( [ 
					'UserID' => $userID,
					'Module' => Yii::$app->request->post ( 'module', 'user' ) 
			] );
			
			foreach ( $permissionsToSave as $pts => $val ) {
				$permissionModel = new UserPermission ();
				$permissionModel->UserID = $userID;
				$permissionModel->Module = Yii::$app->request->post ( 'module', 'user' );
				$permissionModel->Permission = $pts;
				$permissionModel->save ();
			}
			
			Yii::$app->session->setFlash ( 'success', Yii::t("app", 'Permisiuni au fost salvate') );
			
			return $this->refresh ();
		}
		
		$allModules = Yii::$app->modules;
		
		$tabItems = [ ];
		foreach ( $allModules as $moduleID => $module ) {
			$module = Yii::$app->getModule ( $moduleID );
			
			if (isset ( $module->isCustomModule )) {
				$tabItems [] = [ 
						'label' => $module->name,
						'url' => \yii\helpers\Url::to ( [ 
								'index',
								'module' => $module->id,
								'userID' => $userID 
						] ),
						'content' => '',
						'active' => Yii::$app->request->get ( 'module' ) == $module->id 
				];
			}
		}
		
		$currentModule = Yii::$app->getModule ( Yii::$app->request->get ( 'module', 'user' ) );
		
		if (empty ( $currentModule->id ))
			$this->redirect ( 'index' );
		
		$controllersPath = $currentModule->controllerPath;
		
		$files = scandir ( $controllersPath );
		
		$controllerNamespace = $currentModule->controllerNamespace;
		
		$classes = array_map ( function ($file) use ($controllerNamespace) {
			return $controllerNamespace . '\\' . str_replace ( '.php', '', $file );
		}, $files );
		
		$controllers = array_filter ( $classes, function ($possibleClass) {
			return class_exists ( $possibleClass );
		} );
		
		$persmissions = [ ];
		foreach ( $controllers as $c ) {
			$co = new $c ( null, null );
			if (method_exists ( $co, 'permissions' )) {
				$controllerPermissions = $co->permissions ();
				
				foreach ( $controllerPermissions as $cp ) {
					$persmissions [] = $cp;
				}
			}
		}
		
		$userPermissionsList = \app\models\User\User::findOne ( $userID )->permissionsList;
		
		return $this->render ( 'index', [ 
				'tabItems' => $tabItems,
				'permissions' => $persmissions,
				'userID' => $userID,
				'userPermissionsList' => $userPermissionsList,
				'currentModule' => $currentModule 
		] );
	}
}
