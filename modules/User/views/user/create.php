<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\User\User */

$this->title = Yii::t("app", 'Creeare utilizator');
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t("app", 'Utilizatori'),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="user-create">

	<h1><?= Html::encode($this->title) ?></h1>

    <?=$this->render ( '_form', [ 'model' => $model ] )?>

</div>
