<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

?>

<div class="user-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
		<div class="col-md-6">
            <?= $form->field($model, 'Email')->textInput(['maxlength' => true]) ?>
        </div>
		<div class="col-md-6">
            <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>
        </div>
        <?php if ($model->scenario == 'add') { ?>
        <div class="col-md-6">
            <?= $form->field($model, 'Password')->passwordInput(['maxlength' => true]) ?>
        </div>
        <?php } ?>
        <div class="col-md-6">
            <?=$form->field ( $model, 'Status' )->widget ( Select2::className (), [ 'data' => \app\models\User\User::getStatusList (),'hideSearch' => true ] )?>
        </div>
	</div>
	<div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t("app", 'Creează') : Yii::t("app", 'Editează'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>