<?php
use kartik\grid\GridView;
use yii\widgets\Pjax;
use yii\bootstrap\Html;
use app\models\User\User;
/* @var $this yii\web\View */
/* @var $searchModel app\models\User\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="user-index">

	<h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
<?php Pjax::begin(); ?>    
    <?=GridView::widget ( [ 'dataProvider' => $dataProvider,'filterModel' => $searchModel,'columns' => [ [ 'class' => 'yii\grid\SerialColumn' ],'Email:email','Name',[ 'attribute' => 'Status','filter' => Html::activeDropDownList ( $searchModel, 'Status', User::getStatusList (), [ 'class' => 'form-control' ] ),'format' => 'raw','value' => function ($model) {return User::getStatusLabel ( $model->Status );} ],[ 'class' => 'yii\grid\ActionColumn','buttons' => [ 'permissions' => function ($url, $model, $key) {return Html::a ( '<span class="glyphicon glyphicon glyphicon-lock" aria-hidden="true"></span> ', yii\helpers\Url::to ( [ '/admin/user/permission/index','userID' => $model->id ] ) );} ],'template' => '<div class="text-center">{update}&nbsp;&nbsp;&nbsp;{view}&nbsp;&nbsp;&nbsp;{permissions}&nbsp;&nbsp;&nbsp;{delete}</div>' ] ],'responsive' => true,'hover' => true ] );?>
<?php Pjax::end(); ?></div>
