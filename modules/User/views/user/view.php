<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\User\User;

/* @var $this yii\web\View */
/* @var $model app\models\User\User */

$this->title = $model->Name;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t("app", "Utilizatori"),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="user-view">

	<h1><?= Html::encode($this->title) ?></h1>

	<p>
        <?= Html::a('Update', ['update', 'id' => $model->ID], ['class' => 'btn btn-primary']) ?>
        <?=Html::a ( 'Delete', [ 'delete','id' => $model->ID ], [ 'class' => 'btn btn-danger','data' => [ 'confirm' => Yii::t("app", "Sunteți sigur că doriți să ștergeți acest utilizator?"),'method' => 'post' ] ] )?>
    </p>

    <?=DetailView::widget ( [ 'model' => $model,'attributes' => [ 'ID','Email:email','Name',[ 'attribute' => 'Status','format' => 'raw','value' => function ($model) {return User::getStatusLabel ( $model->Status );} ] ] ] )?>

</div>
