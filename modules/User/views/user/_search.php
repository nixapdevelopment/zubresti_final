<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\User\UserSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-search">

    <?php
				
$form = ActiveForm::begin ( [ 
						'action' => [ 
								'index' 
						],
						'method' => 'get' 
				] );
				?>

    <?= $form->field($model, 'ID') ?>

    <?= $form->field($model, 'Email') ?>

    <?= $form->field($model, 'Password') ?>

    <?= $form->field($model, 'AuthKey') ?>

    <?= $form->field($model, 'Name') ?>

    <?php echo $form->field($model, 'Status') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t("app",'Caută'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t("app", 'Resetează'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
