<?php

namespace app\modules\Settings\controllers;

use Yii;
use app\controllers\BackendController;
use yii\bootstrap\Html;
use yii\bootstrap\Tabs;
use kartik\checkbox\CheckboxX;
use app\models\Settings\Settings;
use app\components\UploadedFile;
use kartik\widgets\FileInput;
use yii\caching\TagDependency;

/**
 * Default controller for the `Settings` module
 */
class DefaultController extends BackendController {
	const TYPE_SELECT = 'select';
	const TYPE_INTIGER = 'intiger';
	const TYPE_IMAGE = 'image';
	const TYPE_CHECKBOX = 'checkbox';
	const TYPE_VARCHAR = 'varchar';
	const TYPE_TEXT = 'text';
	const TYPE_LANG_TEXT = 'langText';
	public $settingsData = [ ];
	public function settingsList() {
		return [ 
				'General' => [ 
						'label' => Yii::t ( 'app', 'Setări generale' ),
						'items' => [ 
								'<div class="row"><div class="col-md-6">',
								'defaultLanguage' => [ 
										'type' => self::TYPE_SELECT,
										'label' => Yii::t ( 'app', 'Limba de bază' ),
										'dropdownOptions' => function () {
											$options = [ ];
											foreach ( Yii::$app->params ['siteLanguages'] as $lang ) {
												$options [$lang] = $lang;
											}
											return $options;
										} 
								],
								'</div><div class="col-md-6">',
								'articlesPerPage' => [ 
										'type' => self::TYPE_INTIGER,
										'label' => Yii::t("app",'Articole pe pagină') 
								],
								'</div></div><hr />',
								'topBarText' => [ 
										'type' => self::TYPE_LANG_TEXT,
										'label' => Yii::t ( 'app', 'Slogan' ) 
								],
								'<div class="row"><div class="col-md-4">',
								'favicon' => [ 
										'type' => self::TYPE_IMAGE,
										'label' => 'Favicon' 
								],
								'</div><div class="col-md-4">',
								'logo' => [ 
										'type' => self::TYPE_IMAGE,
										'label' => 'Header logo' 
								],
								'</div><div class="col-md-4">',
								'footerLogo' => [ 
										'type' => self::TYPE_IMAGE,
										'label' => 'Footer logo' 
								],
								'</div><div class="col-md-4">',
								'contactLogo' => [ 
										'type' => self::TYPE_IMAGE,
										'label' => 'Contact logo' 
								],
								'</div></div>',
								'footerText' => [ 
										'type' => self::TYPE_LANG_TEXT,
										'label' => Yii::t ( 'app', 'Footer text' ) 
								],
								'<hr />',
								'<div class="row"><div class="col-md-6">',
								'facebookLink' => [ 
										'type' => self::TYPE_VARCHAR,
										'label' => Yii::t ( 'app', 'Facebook link' ) 
								],
								'</div><div class="col-md-6">',
								'ytLink' => [ 
										'type' => self::TYPE_VARCHAR,
										'label' => Yii::t ( 'app', 'Youtube link' ) 
								],
								'</div><div class="col-md-6">',
								'okLink' => [ 
										'type' => self::TYPE_VARCHAR,
										'label' => Yii::t ( 'app', 'Odnoklassniki link' ) 
								],
								'</div><div class="col-md-6">',
								'instagramLink' => [ 
										'type' => self::TYPE_VARCHAR,
										'label' => Yii::t ( 'app', 'Instagram link' ) 
								],
								'</div></div>' 
						],
						'active' => true 
				],
				'Contacts' => [ 
						'label' => Yii::t ( 'app', 'Contacte' ),
						'items' => [ 
								'<div class="row"><div class="col-md-4">',
								'phone' => [ 
										'type' => self::TYPE_VARCHAR,
										'label' => Yii::t("app", 'Telefon') 
								],
								'</div><div class="col-md-4">',
								'fax' => [ 
										'type' => self::TYPE_VARCHAR,
										'label' => Yii::t("app", 'Fax')
								],
								'</div><div class="col-md-4">',
								'email' => [ 
										'type' => self::TYPE_VARCHAR,
										'label' => Yii::t("app", 'Email') 
								],
								'</div></div>',
								'<hr />',
								'<div class="row"><div class="col-md-6">',
								'address' => [ 
										'type' => self::TYPE_LANG_TEXT,
										'label' => Yii::t("app",'Adresă') 
								],
								'</div><div class="col-md-6">',
								'grafic' => [ 
										'type' => self::TYPE_LANG_TEXT,
										'label' => Yii::t("app",'Grafic') 
								],
								'</div></div>',
								'<hr />',
								'map' => [ 
										'type' => self::TYPE_TEXT,
										'label' => Yii::t("app",'Hartă')
								] 
						] 
				],
				'System' => [ 
						'label' => Yii::t ( 'app', 'Sistem' ),
						'items' => [ 
								'mentenance' => [ 
										'type' => self::TYPE_CHECKBOX,
										'label' => Yii::t ( 'app', 'Modul de mentenanţă' ) 
								] 
						] 
				] 
		];
	}
	public function init() {
		parent::init ();
		
		$settingsModels = Settings::find ()->indexBy ( 'Name' )->all ();
		foreach ( $settingsModels as $settingName => $settingModel ) {
			$value = @unserialize ( $settingModel->Value );
			if ($value === FALSE) {
				$value = $settingModel->Value;
			}
			$this->settingsData [$settingName] = $value;
		}
	}
	public function actionIndex() {
		return $this->render ( 'index' );
	}
	public function actionSettings() {
		if (Yii::$app->request->isPost) {
			$settingsData = ( array ) Yii::$app->request->post ( 'Settings' );
			
			// save text settings
			foreach ( $settingsData as $settingName => $settingValue ) {
				$model = $this->getSettingModel ( $settingName );
				$model->Value = is_array ( $settingValue ) ? serialize ( $settingValue ) : $settingValue;
				$model->save ();
			}
			
			// save settings files
			$settingsFiles = UploadedFile::getInstancesByName ( 'Settings' );
			foreach ( $settingsFiles as $settingName => $settingFile ) {
				$fileName = md5 ( $settingFile->name . microtime ( true ) ) . '.' . $settingFile->extension;
				if ($settingFile->saveAs ( Yii::getAlias ( '@webroot/uploads/settings/' . $fileName ) )) {
					$model = $this->getSettingModel ( $settingName );
					$model->Value = $fileName;
					$model->save ();
				}
			}
			
			TagDependency::invalidate ( Yii::$app->cache, [ 
					'Settings' 
			] );
			
			Yii::$app->session->setFlash ( 'success', 'Setări au fost modificate' );
			
			return $this->redirect ( [ 
					'/admin/settings' 
			] );
		}
		
		$settings = $this->settingsList ();
		
		$settingsTabs = [ ];
		foreach ( $settings as $groupName => $group ) {
			$content = '<br />';
			foreach ( $group ['items'] as $settingName => $settingsOption ) {
				if (is_array ( $settingsOption )) {
					$content .= '<div class="form-group">';
					$content .= Html::label ( $settingsOption ['label'] );
					$content .= $this->getSettingOptionControl ( $settingName, $settingsOption );
					$content .= '</div>';
				} else {
					$content .= $settingsOption;
				}
			}
			
			$settingsTabs [] = [ 
					'label' => $group ['label'],
					'content' => $content,
					'active' => ! empty ( $group ['active'] ) 
			];
		}
		
		return $this->render ( 'settings-form', [ 
				'settingsTabs' => $settingsTabs 
		] );
	}
	private function getSettingOptionControl($settingName, $settingsOption) {
		$value = $this->settingsData [$settingName];
		
		switch ($settingsOption ['type']) {
			case self::TYPE_SELECT :
				return Html::dropDownList ( "Settings[$settingName]", $value, $settingsOption ['dropdownOptions'] (), [ 
						'class' => 'form-control' 
				] );
			case self::TYPE_INTIGER :
				return Html::input ( 'number', "Settings[$settingName]", $value, [ 
						'class' => 'form-control' 
				] );
			case self::TYPE_IMAGE :
				return FileInput::widget ( [ 
						'name' => "Settings[$settingName]",
						'pluginOptions' => [ 
								'initialPreview' => [ 
										Yii::getAlias ( "@web/uploads/settings/$value" ) 
								],
								'initialPreviewAsData' => true,
								'initialCaption' => "The Moon and the Earth",
								'initialPreviewConfig' => [ 
								],
								'overwriteInitial' => true,
								'maxFileSize' => 2000,
								'showCaption' => false,
								'showRemove' => false,
								'showUpload' => false 
						] 
				] );
			case self::TYPE_CHECKBOX :
				return CheckboxX::widget ( [ 
						'name' => "Settings[$settingName]",
						'pluginOptions' => [ 
								'threeState' => false 
						] 
				] );
			case self::TYPE_VARCHAR :
				return Html::input ( 'text', "Settings[$settingName]", $value, [ 
						'class' => 'form-control' 
				] );
			case self::TYPE_TEXT :
				return Html::textarea ( "Settings[$settingName]", $value, [ 
						'class' => 'form-control' 
				] );
			case self::TYPE_LANG_TEXT :
				$settingLangTabs = [ ];
				foreach ( \Yii::$app->params ['siteLanguages'] as $key => $lang ) {
					$settingLangTabs [] = [ 
							'label' => strtoupper ( $lang ),
							'content' => Html::textarea ( "Settings[$settingName][$lang]", $value [$lang], [ 
									'class' => 'form-control' 
							] ),
							'active' => $key == 0 
					];
				}
				
				return Tabs::widget ( [ 
						'items' => $settingLangTabs 
				] );
		}
	}
	private function getSettingModel($settingName) {
		$model = Settings::findOne ( [ 
				'Name' => $settingName 
		] );
		
		if (empty ( $model->ID )) {
			return new \app\models\Settings\Settings ( [ 
					'Name' => $settingName 
			] );
		}
		
		return $model;
	}
}