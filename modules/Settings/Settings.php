<?php

namespace app\modules\Settings;

use Yii;

/**
 * Settings module definition class
 */
class Settings extends \yii\base\Module {
	
	/**
	 * @inheritdoc
	 */
	public $controllerNamespace = 'app\modules\Settings\controllers';
	
	/**
	 * @inheritdoc
	 */
	public function init() {
		parent::init ();
		
		// custom initialization code goes here
	}
	public static function getByName($name, $applyLanguage = false) {
		if (! isset ( Yii::$app->params ['settings'] [$name] )) {
			return null;
		}
		
		$settingsItemValue = Yii::$app->params ['settings'] [$name];
		
		$data = @unserialize ( $settingsItemValue );
		
		if ($data !== false) {
			$settingsItemValue = unserialize ( $settingsItemValue );
			
			if ($applyLanguage && isset ( $settingsItemValue [Yii::$app->language] )) {
				return $settingsItemValue [Yii::$app->language];
			}
		}
		
		return $settingsItemValue;
	}
}