<?php
use yii\widgets\ActiveForm;
use yii\helpers\Url;

if (! function_exists ( 'has_children' )) {
	function has_children($rows, $id) {
		foreach ( $rows as $row ) {
			if ($row->ParentID == $id) {
				return true;
			}
		}
		return false;
	}
}

?>

<ul class="<?= $root ? "menu-list sortable nested-sortable" : "" ?>">
    <?php foreach ($rows as $row) { ?>
    <?php if ($row->ParentID != $parent) continue; ?>
    <li id="list_<?= $row->ID ?>">
		<div class="li-content">
			<i class="glyphicon glyphicon-move sortable-handle"></i> <?= $row->lang->Title ?>
            <i style="margin-left: 15px; margin-right: 5px;"
				remove-menu-item="<?= $row->ID ?>"
				class="glyphicon glyphicon-trash pull-right text-danger"></i> <i
				data-toggle="collapse" href="#collapseItem-<?= $row->ID ?>"
				class="glyphicon glyphicon-edit pull-right text-info"></i>
			<div class="clearfix"></div>
			<div id="collapseItem-<?= $row->ID ?>" class="collapse">
                <?php
					
$form = ActiveForm::begin ( [ 
							'id' => 'menu-item-lang-' . $row->ID,
							'action' => Url::to ( [ 
									'save-item-langs',
									'itemID' => $row->ID 
							] ),
							'options' => [ 
									'style' => 'margin-top: 5px;' 
							] 
					] );
					?>
                    <?php foreach (Yii::$app->params['siteLanguages'] as $langID) { ?>
                    <div class="form-group">
					<div class="input-group">
						<span class="input-group-addon"><?= strtoupper($langID) ?></span>
						<input type="text" name="langs[<?= $langID ?>]"
							value="<?= isset($row->langs[$langID]) ? $row->langs[$langID]->Title : '' ?>"
							class="form-control" />
					</div>
				</div>
                    <?php } ?>
                    <div class="form-group">
					<button data-toggle="collapse" type="button"
						class="btn btn-success save-item-langs-form">Save</button>
				</div>
                <?php ActiveForm::end() ?>
            </div>
		</div>
        <?php if (has_children($rows, $row->ID)) { ?>
        <?=$this->render ( '_menu_items', [ 'rows' => $rows,'parent' => $row->ID,'root' => false ] )?>
        <?php } ?>
    </li>
    <?php } ?>
</ul>