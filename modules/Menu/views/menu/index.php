<?php
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\Category\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model app\models\Menu\Menu */

$this->title = Yii::t("app", 'Meniuri');
$this->params ['breadcrumbs'] [] = $this->title;
?>
<div class="Menu-default-index">

	<h1><?= Html::encode($this->title) ?></h1>
	<p>
        <?= Html::a('<i class="glyphicon glyphicon-plus"></i> '. Yii::t ( 'app', 'Creează meniu' ), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?php Pjax::begin(); ?>    
        <?=GridView::widget ( [ 'dataProvider' => $dataProvider,'columns' => [ [ 'class' => 'yii\grid\SerialColumn','options' => [ 'width' => '60px' ] ],[ 'label' => Yii::t ( 'app', 'Nume' ),'value' => function ($model) {return $model->lang->Name;},'filter' => false ],[ 'class' => 'yii\grid\ActionColumn','options' => [ 'width' => '100px' ],'template' => '<div class="text-center">{update}</div>' ] ] ] );?>
    <?php Pjax::end(); ?>
</div>