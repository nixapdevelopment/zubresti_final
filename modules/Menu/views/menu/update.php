<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Menu\Menu */

$this->registerJs ( "
    
" );

$this->title = Yii::t("app", 'Editează meniu') . ': ' . $model->lang->Name;
$this->params ['breadcrumbs'] [] = [ 
		'label' => Yii::t("app", 'Categorii'),
		'url' => [ 
				'index' 
		] 
];
$this->params ['breadcrumbs'] [] = $model->lang->Name;
?>
<div class="category-update">

	<h1><?= Html::encode($this->title) ?></h1>

    <?=$this->render ( '_form', [ 'model' => $model,'langsModels' => $langsModels,'articleModels' => $articleModels ] )?>

</div>
