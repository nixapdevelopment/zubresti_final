<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\modules\Menu\assets\MenuAssets;
use kartik\checkbox\CheckboxX;
use yii\widgets\Pjax;

MenuAssets::register ( $this );

?>

<div class="category-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <div class="row">
        <?php foreach ($langsModels as $i => $lmodel) { ?>
        <div class="col-md-3">
            <?= $form->field($lmodel, "[$i]Name")->textInput(['maxlength' => true])->label('Name (' . strtoupper($lmodel->LangID) . ')') ?>
        </div>
        <?php } ?>
        <div>
			<div class="form-group">
				<label style="display: block;" class="control-label">&nbsp;</label>
                <?= Html::submitButton($model->isNewRecord ? Yii::t("app", "Creează") : Yii::t("app", "Editează"), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
		</div>
	</div>
    
    <?php ActiveForm::end(); ?>
    
    <br />
	<div class="row">
        <?php if (!empty($articleModels)) { ?>
        <div class="col-md-5">
			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
                <?php foreach ($articleModels as $type => $amodels) { ?>
                <li role="presentation"
					class="<?= reset($articleModels) == $amodels ? 'active' : '' ?>"><a
					href="#tab-<?= $type ?>" aria-controls="home" role="tab"
					data-toggle="tab"><?= $type ?></a></li>
                <?php } ?>
            </ul>
			<!-- Tab panes -->
			<div class="tab-content">
				<br />
                <?php foreach ($articleModels as $type => $amodels) { ?>
                <div role="tabpanel"
					class="tab-pane <?= reset($articleModels) == $amodels ? 'active' : '' ?>"
					id="tab-<?= $type ?>">
                    <?php
										
ActiveForm::begin ( [ 
												'id' => $type . '-items-form',
												'options' => [ 
														'style' => 'max-height: 300px; padding-bottom: 20px; overflow-y: auto;' 
												] 
										] )?>
                        <?php foreach ($amodels as $am) { ?>
                        <div style="margin-bottom: 5px;">
                            <?=CheckboxX::widget ( [ 'name' => "articles[$am->ID]",'pluginOptions' => [ 'threeState' => false,'size' => 'sm' ] ] );?> <?= $am->lang->Title ?>
                        </div>
                        <?php } ?>
                        <?= Html::hiddenInput('MenuID', $model->ID) ?>
                    <?php ActiveForm::end() ?>
                    <?= Html::button(Yii::t("app", "Adaugă în meniu"), ['class' => 'add-to-menu btn btn-info btn-xs']) ?>
                </div>
                <?php } ?>
            </div>
			<br />
		</div>
		<div class="col-md-6">
            <?php
									
Pjax::begin ( [ 
											'id' => 'menu-pjax' 
									] )?>
            <div class="menu-box">
                <?=$this->render ( '_menu_items', [ 'rows' => $model->items,'parent' => 0,'root' => true ] )?>
            </div>
            <?php Pjax::end() ?>
        </div>
        <?php } ?>
    </div>

</div>
