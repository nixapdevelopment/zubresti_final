<?php

namespace app\modules\Menu\assets;

use yii\web\AssetBundle;

class MenuAssets extends AssetBundle {
	public $sourcePath = '@app/modules/Menu/assets/files';
	public $baseUrl = '@web';
	public $css = [ 
			'css/style.css' 
	];
	public $js = [ 
			'js/nestable.js' 
	];
	public $depends = [ 
			'yii\web\YiiAsset',
			'yii\bootstrap\BootstrapAsset',
			'yii\jui\JuiAsset' 
	];
	public $forceCopy = true;
}