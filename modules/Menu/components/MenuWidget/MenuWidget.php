<?php

namespace app\modules\Menu\components\MenuWidget;

use Yii;
use yii\widgets\Menu as YiiMenu;
use yii\base\InvalidParamException;
use app\models\Menu\Menu;
use yii\caching\TagDependency;

class MenuWidget extends YiiMenu {
	public $menuId = null;
	private $menuStack;
	private $_items = [ 
			'items' => [ ] 
	];
	public function run() {
		if (! $this->menuId) {
			throw new InvalidParamException ( "Parametr `menuId` for MenuWidget is reqired" );
		}
		
		$result = Menu::getDb ()->cache ( function ($db) {
			return Menu::find ()->with ( [ 
					'lang',
					'items.lang',
					'items.article.link' 
			] )->where ( [ 
					'ID' => $this->menuId 
			] )->one ();
		}, 180, new TagDependency ( [ 
				'tags' => 'MenuWidget' 
		] ) );
		
		$this->menuStack = $result->items;
		$this->buildMenuItems ( $this->menuStack, $this->_items );
		$this->items = $this->_items ['items'];
		
		echo parent::run ();
	}
	private function buildMenuItems(&$menuStack, &$parentItem, $parentID = 0) {
		foreach ( $menuStack as $menuItem ) {
			if ($parentID == $menuItem->ParentID) {
				$parentItem ['items'] [$menuItem->ID] = [ 
						'label' => $menuItem->lang->Title,
						'url' => $menuItem->article->seoLink,
						'items' => [ ] 
				];
				
				unset ( $this->menuStack [$menuItem->ID] );
				
				$this->buildMenuItems ( $menuStack, $parentItem ['items'] [$menuItem->ID], $menuItem->ID );
			}
		}
	}
}