<?php

namespace app\modules\Menu\controllers;

use Yii;
use app\controllers\BackendController;
use app\models\Menu\Menu;
use yii\data\ActiveDataProvider;
use app\models\MenuLang\MenuLang;
use yii\base\Model;
use app\models\MenuItem\MenuItem;
use app\models\MenuItemLang\MenuItemLang;
use app\models\Article\Article;
use yii\caching\TagDependency;

/**
 * Default controller for the `Menu` module
 */
class MenuController extends BackendController {
	public function actionIndex() {
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => Menu::find ()->with ( 'lang' ) 
		] );
		
		return $this->render ( 'index', [ 
				'dataProvider' => $dataProvider 
		] );
	}
	public function actionCreate() {
		$model = new Menu ();
		
		$langsModels = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $i => $lang ) {
			$langsModels [$i] = new MenuLang ( [ 
					'LangID' => $lang 
			] );
		}
		
		if (Yii::$app->request->isPost) {
			if (Model::loadMultiple ( $langsModels, Yii::$app->request->post () ) && Model::validateMultiple ( $langsModels )) {
				$model->save ();
				
				foreach ( $langsModels as $lmodel ) {
					$lmodel->MenuID = $model->ID;
					$lmodel->save ();
				}
				
				TagDependency::invalidate ( Yii::$app->cache, 'MenuWidget' );
				
				Yii::$app->session->setFlash ( 'success', Yii::t("app", 'Meniul a fost salvat') );
				
				return $this->redirect ( [ 
						'update',
						'id' => $model->ID 
				] );
			}
		}
		
		return $this->render ( 'create', [ 
				'model' => $model,
				'langsModels' => $langsModels 
		] );
	}
	public function actionUpdate($id) {
		$model = $this->findModel ( $id );
		
		$langsModels = [ ];
		foreach ( Yii::$app->params ['siteLanguages'] as $i => $lang ) {
			$langsModels [$i] = isset ( $model->langs [$lang] ) ? $model->langs [$lang] : new MenuLang ( [ 
					'LangID' => $lang,
					'MenuID' => $model->ID 
			] );
		}
		
		$articleModels = \app\models\Article\Article::find ()->with ( 'lang' )->where ( [ 
				'Status' => 'Active' 
		] )->all ();
		$articlesByType = [ ];
		foreach ( $articleModels as $am ) {
			$articlesByType [$am->Type] [] = $am;
		}
		
		if (Yii::$app->request->isPost) {
			if (Model::loadMultiple ( $langsModels, Yii::$app->request->post () ) && Model::validateMultiple ( $langsModels )) {
				$model->save ();
				
				foreach ( $langsModels as $lmodel ) {
					$lmodel->save ();
				}
				
				TagDependency::invalidate ( Yii::$app->cache, 'MenuWidget' );
				
				Yii::$app->session->setFlash ( 'success', Yii::t("app", 'Meniul a fost salvat'));
				
				return $this->redirect ( [ 
						'update',
						'id' => $model->ID 
				] );
			}
		}
		
		return $this->render ( 'update', [ 
				'model' => $model,
				'langsModels' => $langsModels,
				'articleModels' => $articlesByType 
		] );
	}
	public function actionDelete($id) {
		Menu::deleteAll ( [ 
				'ID' => $id 
		] );
		
		TagDependency::invalidate ( Yii::$app->cache, 'MenuWidget' );
		
		return $this->redirect ( [ 
				'index' 
		] );
	}
	protected function findModel($id) {
		if (($model = Menu::find ()->where ( [ 
				'ID' => $id 
		] )->with ( [ 
				'langs',
				'lang',
				'items',
				'items.lang',
				'items.langs',
				'items.article.lang',
				'items.article.langs' 
		] )->one ()) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException ( 'The requested page does not exist.' );
		}
	}
	public function actionMenuItemAdd() {
		$articles = ( array ) Yii::$app->request->post ( 'articles' );
		$MenuID = ( int ) Yii::$app->request->post ( 'MenuID' );
		
		foreach ( $articles as $articleID => $checked ) {
			if ($checked) {
				$article = Article::find ()->with ( 'langs' )->where ( [ 
						'ID' => $articleID 
				] )->one ();
				
				if (! empty ( $article->ID )) {
					$menuItem = new MenuItem ();
					$menuItem->MenuID = $MenuID;
					$menuItem->ArtcileID = $article->ID;
					$menuItem->ParentID = 0;
					$menuItem->Position = 999999;
					
					if ($menuItem->save ()) {
						foreach ( Yii::$app->params ['siteLanguages'] as $langID ) {
							$menuItemLang = new MenuItemLang ();
							$menuItemLang->MenuItemID = $menuItem->ID;
							$menuItemLang->LangID = $langID;
							$menuItemLang->Title = isset ( $article->langs [$langID] ) ? $article->langs [$langID]->Title : 'No title';
							$menuItemLang->save ();
						}
					}
				}
			}
		}
		
		TagDependency::invalidate ( Yii::$app->cache, 'MenuWidget' );
	}
	public function actionMenuItemDelete() {
		$menuItemID = Yii::$app->request->post ( 'menuItemId' );
		
		$model = MenuItem::findOne ( $menuItemID );
		$model->delete ();
		
		MenuItem::deleteAll ( [ 
				'ParentID' => $model->ID 
		] );
		
		TagDependency::invalidate ( Yii::$app->cache, 'MenuWidget' );
	}
	public function actionMenuItemSort() {
		$list = ( array ) Yii::$app->request->post ( 'list' );
		
		$position = [ ];
		foreach ( $list as $itemID => $parentID ) {
			$parentID = ( int ) $parentID;
			$position [$parentID] ++;
			
			MenuItem::updateAll ( [ 
					'ParentID' => $parentID,
					'Position' => $position [$parentID] 
			], [ 
					'ID' => $itemID 
			] );
		}
		
		TagDependency::invalidate ( Yii::$app->cache, 'MenuWidget' );
	}
	public function actionSaveItemLangs($itemID) {
		MenuItemLang::deleteAll ( [ 
				'MenuItemID' => $itemID 
		] );
		
		$langs = \Yii::$app->request->post ( 'langs' );
		
		foreach ( $langs as $langID => $title ) {
			$model = new MenuItemLang ();
			$model->LangID = $langID;
			$model->MenuItemID = $itemID;
			$model->Title = $title;
			$model->save ();
		}
		
		TagDependency::invalidate ( Yii::$app->cache, 'MenuWidget' );
	}
}