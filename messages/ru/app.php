<?php

return [
    'Setări generale' => 'Основные настройки',
    'Limba de bază' => 'Язык по умолчанию',
    'Contacte' => 'Контакты',
    'Data' => 'Дата',
    'Status' => 'Статус',
    'Titlu' => 'Заголовок',
    'Text' => 'Текст',
    'Imagine-preview' => 'Превью',
    'Image' => 'Изображение',
    'Position' => 'Позиция',
    'Seo Titlu' => 'SEO - заголовок',
    'Cuvinte-cheie' => 'Ключевые слова',
    'Descriere' => 'SEO - описание',
    'Activ' => 'Активный',
    'Inactiv' => 'Неактивный',
    'Name' => 'Имя',
    'Telefon' => 'Телефон',
    'Fax' => 'Факс',
    'Email' => 'Эл. почта',
    'Mesaj' => 'Сообщение',
    'Link' => 'Ссылка',
    'Menu Name' => 'Наименование',
    'Slider Name' => 'Наименование',
    'Meniu' => 'Меню',
    'Slider' => 'Слайдер',
    'Galerii' => 'Фотогалереи',
    'Feedback' => 'Обратная связь',
    'Setările' => 'Настройки',
    'Logout' => 'Выход',
    'Dashboard' => 'Панель',
    'Quick add' => 'Создать',
    
    'Ianuarie' => 'Январь',
    'Februarie' => 'Февраль',
    'Martie' => 'Март',
    'Aprilie' => 'Апрель',
    'Mai' => 'Май',
    'Iunie' => 'Июнь',
    'Iulie' => 'Июль',
    'August' => 'Август',
    'Septembrie' => 'Сентябрь',
    'Octombrie' => 'Октябрь',
    'Noiembrie' => 'Ноябрь',
    'Decembrie' => 'Декабрь',
    
    'Anul' => 'Год',
    'Lună' => 'Месяц',
    'Zi' => 'День',
    
    /* HOME PAGE */
    
    'vezi detalii' => 'подробности',
    'Contactează-ne' => 'Свяжитесь с нами',
    'Noutăți' =>'Новости',
    'Vezi toate noutățile' =>'все новости',
    'ANUNȚURI' =>'ОБЪЯВЛЕНИЯ',
    'Vezi toate anunțurile' =>'все объявления',
    'Poze' =>'изображения',
    'Vezi toate pozele' =>'все изображения',
    'Video' =>'Видео',
    'vezi toate video' =>'Все видео',
    
    /* RIGHT BLOCK */
    'CERERI' =>'ЗАЯВКИ',
    
    /* ANNOUNCENENT */
    'Distribuie mai departe' =>'Разместить на',
    'Alte Anunturi' =>'Другие объявления',
    /* CONTACT */
    'Trimite' =>'Отправить',
    /* GALLERY */
    'poze' =>'изображений',   
    /* SEARCH */
    'Căutare' =>'Поиск',
    'Mai mult' =>'Дальше',
    
    /* MODULES SECTION */
    /*
     * ADMIN MENU WIDGET 
     */
    'Panel' =>'Панель',
    'Adaugă rapid' =>'Быстрое добавление',
    'Foto galerie' =>'Фотогаллерея',
    'Video galerie' =>'Видеогаллерея',
    'Conținut' => 'Контент',
    /*
     * ARTICLES 
     */
    'Părinte' =>'Родитель',
    'Selectează părinţi ...' =>'Выберите родителей ...',
    
    'Pagină' =>'Страница',
    'Pagini' =>'Страницы',
    'Adaugă pagina' =>'Добавить страницу',
    'Pagina a fost salvată' =>'Страница была сохранена',
    
    'Noutate' =>'Новость',
    'Adaugă noutate' =>'Добавить новость',
    'Noutatea a fost salvată' =>'Новость была сохранена',
    
    'Eveniment' =>'Событие',
    'Evenimente' =>'',
    'Adaugă eveniment' =>'Добавить событие',
    'Evenimentul a fost salvat' =>'Событие было сохранено',
    
    'Anunț' =>'Объявление',
    'Anunțuri' =>'Объявления',
    'Adaugă anunț' =>'Добавить объявление',
    'Anunțul a fost salvat' =>'Объявление было сохранено',
    
    'Informație publică' =>'Публичная информация',
    'Informații publice' =>'Публичная информация',
    'Adaugă informații publice' =>'Добавить публичную информацию',
    'Informație publică a fost salvată' =>'Публичная информация была сохранена',
    
    'Decizie' =>'Решение',
    'Decizii' =>'Решения',
    'Adaugă decizie' =>'Добавить решение',
    'Decizia a fost salvată' =>'Решение было сохранено',
    
    'Media' =>'СМИ статья',
    'Articole Media' =>'СНИ статьи',
    'Adaugă articol Media' =>'Добавить СМИ статью',
    'Media articol a fost salvat' =>'Статья СМИ была сохранена',
    
    /* 
     * CATEGORY 
     */
    'Categoria a fost salvată' =>'Категория была сохранена',
    'Creează' =>'Создать',
    'Editează' =>'Редактировать',
    'Editare' =>'Редактирование',
    'Șterge' => 'Удалить',
    'Caută' =>'Искать',
    'Resetează' =>'Очистить',
    'Categorii' =>'Категории',
    'Creează categorie' =>'Создать категорию',
    'Nume' =>'Имя',
    'Editare categorie' =>'Редактирование категории',
    'Sunteți sigur că doriți să ștergeți această categorie?' =>'Вы уверены что хотите удалить данную категорию?',
    
    'Creează limba p/u categorie' =>'Cоздать язык категорий',
    'Limbile categoriilor' =>'Языки категорий',
    'Editează limba p/u categorii' =>'Редактировать язык категорий',
    'Sunteți sigur că doriți să ștergeți această limbă p/u categorie?' =>'Вы уверены что хотите удалить данный язык категории?',
    
    /* COUNTRY */
    'Țara a fost salvată' =>'Страна была сохранена',
    'General' =>'Основное',
    'Regiuni' =>'Регионы',
    'Adaugă regiune' =>'Добавить регион',
    'Editare regiune' =>'Редактирование региона',
    'Creează țară' =>'Создать страну',
    'Țări' =>'Страны',
    'Flag' =>'Флаг',
    'Editează țară' =>'Редактировать страну',
    'Sunteți sigur că doriți să ștergeți această țară?' =>'Вы уверены что хотите удалить данную страну?',
    
	/* DASHBOARD */
	'Acces in panel' =>'Доступ к панели',
    'Panel' =>'Панель',

    /* FEEDBACK */
    'Feedback-uri' =>'Обратная связь',
    
    /* GALLERY */
    'Galeria a fost salvată' =>'Галлерея была сохранена',
    'Datele galeriei' =>'Данные галлереи',
    'Itemii galeriei' =>'Единицы в галлерее',
    'Rămîne' =>'Остаться',
    'Înapoi la listă' =>'Назад к списку',
    'Creează galerie' =>'Создать галлерею',
    'Creează album' =>'Создать альбом',
    'Editează galeria : ' =>'Редактировать галлерею : ',
    'Sunteți sigur că doriți să ștergeți această galerie?' =>'Вы уверены что хотите удалить данную галлерею?',
	
    /* LOCATION */
    'Locaţia a fost salvată' =>'Локация была сохранена',
    '<span id="location-picker-hack">Locaţie pe hartă</span>' =>'<span id="location-picker-hack">Локация на карте</span>',
    'Selectează...' =>'Выберите...',
    'Creează locaţie' =>'Создать локацию',
    'Locaţii' =>'Локации',
    'Editează locaţie' =>'Редактировать локацию',
    'Sunteți sigur că doriți să ștergeți această locaţie?' =>'Вы уверены что хотите удалить данную локацию?',
    
	/* MENU */
	'Meniul a fost salvat' =>'Меню было сохранено',
	'Adaugă în meniu' =>'Добавить в меню',
	'Creează meniu' =>'Создать меню',
	'Meniuri' =>'Меню',
	'Editează meniu' =>'Редактировать меню',
	
	/* NEWS */
	'Noutatea a fost salvată' =>'Новость была сохранена',
	'Noutatea a fost ştearsă' =>'Новость была удалена',
	'Creează noutate' =>'Создать новость',
	'Noutăţi' =>'Новости',		
	'Editează noutate' =>'Редактировать новость',
	
	/* PAGES */
	'Pagina a fost ştearsă' =>'Страница была удалена',
	'Creează pagină' =>'Создать страницу',
	'Editează pagină' =>'Редактировать страницу',

	/* PROJECTS */
	'Vezi proiecte' =>'Просмотр проектов',
	'Proiectul a fost creat' =>'Проект был создан',
	'Proiectul a fost editat' =>'Проект был отредактирован',
    'Creează proiect' =>'Создать проект',
    'Proiecte' =>'Проекты',
    'Editează proiect' =>'Редактировать проект',
    'Articole pe pagină' =>'Статей на странице',

	/* SETTINGS */
    'Slogan' =>'Слоган',
    'Adresă' =>'Адрес',
    'Grafic' =>'График',
    'Hartă' =>'Карта',
    'Sistem' =>'Система',
    'Modul de mentenanţă' =>'Режим тех.обслуживания',
    'Setări au fost modificate' =>'Настройки были изменены',
    'Modifică setări' =>'Изменить настройки',
	
    /* SLIDER */
    'Sliderul a fost salvat' => 'Слайдер был сохранен',
    'Adaugă item în slider' => 'Добавить единицу в слайдер',
    'Creare slider' =>'Создание слайдера',
    'Slidere' =>'',
    'Creează slider' =>'Создать слайдер',
    'Editare slider' =>'Редактирование слайдера',
	
    /* USERS */
    'Setare permisiuni' =>'Настройки доступа',
    'Permisiuni au fost salvate' =>'Разрешения были сохранены',
    'Vezi lista utilizatori' =>'См. список пользователей',
    'Şterge utilizatori' =>'Удалить пользователей',
    'Сreează utilizatori' =>'Создать пользователей',
    'Salvează' =>'Сохранить',
    'Creeare utilizator' =>'Создание пользователя',
    'Utilizatori' =>'Пользователи',
    'Modificare utilizator' =>'Редактирование пользователя',
    'Sunteți sigur că doriți să ștergeți acest utilizator?' =>'Вы уверены что хотите удалить данного пользователя?',
    
    /* CATEGORY MODEL ATTR */
    'Imagine' =>'Изображение',
    /* FEEDBACK MODEL ATTR */
    'Subiect' =>'Тема',
    /* GALLERY MODEL ATTR */
    'Părinte ID' =>'ID Родителя',
    'Poziție' =>'Позиция',
    'Galerie Album ID' =>'ID Альбома галлереи',
    'Galerie ID' =>'ID галлереи',
    'Tipul' =>'Тип',
    'Galerie Item ID' =>'ID единицы галлереи',
    /* LINK MODEL ATTR */
    'Articol ID' =>'ID Статьи',
    /* LOCTION MODEL ATTR */
    'Țară ID' =>'ID Страны',
    'ID Regiunea țării' =>'ID Региона страны',
    'Longitudine' =>'Долгота',
    'Latitudine' =>'Широта',
    'ID Locație' =>'ID Локации',
    'ID Categorie' =>'ID Категории',
    'ID Imagine' =>'ID Изображения',
    'Lang ID' =>'ID Языка',
    
    /* MENU MODEL ATTR */
    'ID Meniu' =>'ID Меню',
    'ID Itemului Meniu' =>'ID единицы меню',
    'Numele meniului' =>'Название меню',
    
    /* NEWS MODEL ATTR */
    'ID Noutate' =>'ID Новости',
    /* PAGE MODEL ATTR */
    'ID Pagină' =>'ID Страницы',
    /* PROJECT MODEL ATTR */
    'Creator' =>'Создатель',
    'Client' =>'Клиент',
    'Creat' =>'Создан',
    'Data începerii' =>'Дата начала',
    'Data sfîrșitului' =>'Дата окончания',
    
    /* SETTINGS MODEL ATTR */
    'Valoare' =>'Значение',
    /* SLIDER MODEL ATTR */
    'Nume Slider' => 'Имя слайдера',
    'ID Sliderului' =>'ID Слейдера',
    'ID Itemului Slider' =>'ID единицы слайдера',
    /* USER MODEL ATTR */
    'Parolă' =>'Пароль',
    'Ține mă minte' =>'Запомнить меня',
    'Pin' =>'Пин',
    'Auth-cheie' =>'Auth-Ключ',
    'Utilizator ID' =>'ID Пользователя',
    'Permisiuni' =>'Разрешения',
    'Codul de verificare' =>'Проверочный код',
    '' =>'',
    '' =>'',
    '' =>'',
    '' =>'',
    '' =>'',
    '' =>'',
    
    
    
];