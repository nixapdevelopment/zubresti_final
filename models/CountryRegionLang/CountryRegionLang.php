<?php

namespace app\models\CountryRegionLang;

use Yii;

/**
 * This is the model class for table "CountryRegionLang".
 *
 * @property integer $ID
 * @property integer $CountryRegionID
 * @property string $LangID
 * @property string $Name
 *
 * @property CountryRegion $countryRegion
 */
class CountryRegionLang extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'CountryRegionLang';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Name' 
						],
						'required' 
				],
				[ 
						[ 
								'Name' 
						],
						'string',
						'max' => 255 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'CountryRegionID' => 'Country Region ID',
				'LangID' => 'Lang ID',
				'Name' => Yii::t("app", "Nume") 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCountryRegion() {
		return $this->hasOne ( CountryRegion::className (), [ 
				'ID' => 'CountryRegionID' 
		] );
	}
}
