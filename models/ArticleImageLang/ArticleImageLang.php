<?php

namespace app\models\ArticleImageLang;

use Yii;
use app\models\ArticleImage\ArticleImage;

/**
 * This is the model class for table "ArticleImageLang".
 *
 * @property integer $ID
 * @property integer $ArticleImageID
 * @property string $LangID
 * @property string $Title
 *
 * @property ArticleImage $articleImage
 */
class ArticleImageLang extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'ArticleImageLang';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'ArticleImageID',
								'LangID' 
						],
						'required' 
				],
				[ 
						[ 
								'ArticleImageID' 
						],
						'integer' 
				],
				[ 
						[ 
								'LangID' 
						],
						'string',
						'max' => 2 
				],
				[ 
						[ 
								'Title' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'ArticleImageID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => ArticleImage::className (),
						'targetAttribute' => [ 
								'ArticleImageID' => 'ID' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'ArticleImageID' => 'Article Image ID',
				'LangID' => 'Lang ID',
				'Title' => Yii::t ( 'app', 'Title' ) 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getArticleImage() {
		return $this->hasOne ( ArticleImage::className (), [ 
				'ID' => 'ArticleImageID' 
		] );
	}
}
