<?php

namespace app\models\Slider;

use Yii;

/**
 * This is the model class for table "Slider".
 *
 * @property integer $ID
 * @property string $Name
 *
 * @property SliderItem[] $sliderItems
 */
class Slider extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'Slider';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Name' 
						],
						'required' 
				],
				[ 
						[ 
								'Name' 
						],
						'string',
						'max' => 255 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'Name' => Yii::t ( 'app', 'Nume Slider' ) 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getSliderItems() {
		return $this->hasMany ( SliderItem::className (), [ 
				'SliderID' => 'ID' 
		] );
	}
}
