<?php

namespace app\models\LocationImage;

use Yii;
use app\models\Image\Image;
use app\models\Location\Location;

/**
 * This is the model class for table "LocationImage".
 *
 * @property integer $ID
 * @property integer $LocationID
 * @property integer $ImageID
 *
 * @property Image $image
 * @property Location $location
 */
class LocationImage extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'LocationImage';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'LocationID',
								'ImageID' 
						],
						'required' 
				],
				[ 
						[ 
								'LocationID',
								'ImageID' 
						],
						'integer' 
				],
				[ 
						[ 
								'ImageID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => Image::className (),
						'targetAttribute' => [ 
								'ImageID' => 'ID' 
						] 
				],
				[ 
						[ 
								'LocationID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => Location::className (),
						'targetAttribute' => [ 
								'LocationID' => 'ID' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'LocationID' => Yii::t("app", 'ID Locație'),
				'ImageID' => Yii::t("app", 'ID Imagine') 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getImage() {
		return $this->hasOne ( Image::className (), [ 
				'ID' => 'ImageID' 
		] );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLocation() {
		return $this->hasOne ( Location::className (), [ 
				'ID' => 'LocationID' 
		] );
	}
}
