<?php

namespace app\models\GalleryAlbumLang;

use Yii;

/**
 * This is the model class for table "GalleryAlbumLang".
 *
 * @property integer $ID
 * @property integer $GalleryAlbumID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property GalleryAlbum $galleryAlbum
 */
class GalleryAlbumLang extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'GalleryAlbumLang';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'GalleryAlbumID',
								'LangID' 
						],
						'required' 
				],
				[ 
						[ 
								'GalleryAlbumID' 
						],
						'integer' 
				],
				[ 
						[ 
								'Text' 
						],
						'string' 
				],
				[ 
						[ 
								'LangID' 
						],
						'string',
						'max' => 2 
				],
				[ 
						[ 
								'Title' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'GalleryAlbumID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => GalleryAlbum::className (),
						'targetAttribute' => [ 
								'GalleryAlbumID' => 'ID' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => Yii::t ( 'app', 'ID' ),
				'GalleryAlbumID' => Yii::t ( 'app', 'Galerie Album ID' ),
				'LangID' => Yii::t ( 'app', 'Lang ID' ),
				'Title' => Yii::t ( 'app', 'Titlu' ),
				'Text' => Yii::t ( 'app', 'Text' ) 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getGalleryAlbum() {
		return $this->hasOne ( GalleryAlbum::className (), [ 
				'ID' => 'GalleryAlbumID' 
		] );
	}
}
