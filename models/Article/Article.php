<?php

namespace app\models\Article;

use Yii;
use app\models\ArticleLang\ArticleLang;
use app\models\ArticleImage\ArticleImage;
use app\models\ArticleFile\ArticleFile;
use app\models\Link\Link;
use app\models\ArticleRelation\ArticleRelation;
use yii\helpers\Url;

/**
 * This is the model class for table "Article".
 *
 * @property integer $ID
 * @property string $Date
 * @property string $Type
 * @property string $Status
 * @property integer $IsHome
 *
 * @property ArticleLang[] $articleLangs
 * @property Link $seoLink
 */
class Article extends \yii\db\ActiveRecord {
	const StatusActive = 'Active';
	const StatusInactive = 'Inactive';
	public static function getStatusList() {
		return [ 
				self::StatusActive => 'Active',
				self::StatusInactive => 'Inactive' 
		];
	}
	public static function getStatusLabel($status) {
		$statusList = self::getStatusList ();
		return $statusList [$status];
	}
	public function beforeSave($insert) {
		$this->Date = date ( 'c', strtotime ( $this->Date ) );
		
		return parent::beforeSave ( $insert );
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'Article';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Date' 
						],
						'safe' 
				],
				[ 
						[ 
								'Status',
								'Type' 
						],
						'required' 
				],
				[ 
						[ 
								'Status',
								'Type' 
						],
						'string',
						'max' => 20 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'Date' => \Yii::t ( 'app', "Data" ),
				'Status' => \Yii::t ( 'app', "Status" ) 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLangs() {
		return $this->hasMany ( ArticleLang::className (), [ 
				'ArticleID' => 'ID' 
		] )->indexBy ( 'LangID' );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLang() {
		return $this->hasOne ( ArticleLang::className (), [ 
				'ArticleID' => 'ID' 
		] )->where ( [ 
				'LangID' => Yii::$app->language 
		] );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getImages() {
		return $this->hasMany ( ArticleImage::className (), [ 
				'ArticleID' => 'ID' 
		] )->with ( [ 
				'langs',
				'lang' 
		] )->orderBy ( 'Position' );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getFiles() {
		return $this->hasMany ( ArticleFile::className (), [ 
				'ArticleID' => 'ID' 
		] )->orderBy ( 'Position' );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLink() {
		return $this->hasOne ( Link::className (), [ 
				'ArticleID' => 'ID' 
		] );
	}
	public function getSeoLink() {
		return $this->Template == 'home' ? Url::to ( [ 
				'/' 
		] ) . '/' : Url::to ( [ 
				'/' . $this->link->Link 
		] );
	}
	public function getParents() {
		return $this->hasMany ( ArticleRelation::className (), [ 
				'ChildArticleID' => 'ID' 
		] )->with ( 'parentArticle' );
	}
	public function getChilds() {
		return $this->hasMany ( ArticleRelation::className (), [ 
				'ParentArticleID' => 'ID' 
		] )->with ( 'childArticle' );
	}
	public function getMainImage() {
		return $this->hasOne ( ArticleImage::className (), [ 
				'ArticleID' => 'ID' 
		] )->orderBy ( 'Position' )->limit ( 1 );
	}
	public function getMainImageUrl() {
		return '/uploads/article/' . $this->mainImage->Image;
	}
	public function getMainThumbUrl() {
		return '/uploads/article/' . $this->mainImage->Thumb;
	}
	public function getdMYDate() {
		return date ( 'd M Y', strtotime ( $this->Date ) );
	}
	public function getShortText($length = 80) {
		return mb_substr ( strip_tags ( html_entity_decode ( $this->lang->Text ) ), 0, $length );
	}
}