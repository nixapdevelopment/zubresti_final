<?php

namespace app\models\Article;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Article\Article;

/**
 * ArticleSearch represents the model behind the search form about `app\models\Article\Article`.
 */
class ArticleSearch extends Article {
	public $Type;
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ ];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params, $articleConfig) {
		$query = Article::find ()->with ( 'lang' )->where ( [ 
				'Type' => $this->Type 
		] );
		
		// add conditions that should always apply here
		if ($articleConfig ['hasChronology']) {
			$query->addOrderBy ( [ 
					'Date' => SORT_DESC 
			] );
		}
		
		$query->addOrderBy ( [ 
				'NotRemovable' => SORT_DESC 
		] );
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'pagination' => [ 
						'pageSize' => 10 
				] 
		] );
		
		$this->load ( $params );
		
		return $dataProvider;
	}
}