<?php

namespace app\models\Gallery;

use yii\data\ActiveDataProvider;
use app\models\Gallery\Gallery;

/**
 * GallerySearch represents the model behind the search form about `app\models\Gallery\Gallery`.
 */
class GallerySearch extends Gallery {
	public function search($type) {
		$query = Gallery::find ()->with ( [ 
				'lang',
				'parent',
				'parent.lang',
				'items' 
		] )->orderBy ( 'Position' )->where ( [ 
				'Type' => $type 
		] );
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query 
		] );
		
		return $dataProvider;
	}
}
