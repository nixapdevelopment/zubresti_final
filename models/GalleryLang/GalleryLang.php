<?php

namespace app\models\GalleryLang;

use Yii;
use app\models\Gallery\Gallery;

/**
 * This is the model class for table "GalleryLang".
 *
 * @property integer $ID
 * @property integer $GalleryID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property Gallery $gallery
 */
class GalleryLang extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'GalleryLang';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'LangID',
								'Title' 
						],
						'required' 
				],
				[ 
						[ 
								'GalleryID' 
						],
						'integer' 
				],
				[ 
						[ 
								'Text' 
						],
						'string' 
				],
				[ 
						[ 
								'LangID' 
						],
						'string',
						'max' => 2 
				],
				[ 
						[ 
								'Title' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'GalleryID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => Gallery::className (),
						'targetAttribute' => [ 
								'GalleryID' => 'ID' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => Yii::t ( 'app', 'ID' ),
				'GalleryID' => Yii::t ( 'app', 'Galerie ID' ),
				'LangID' => Yii::t ( 'app', 'Lang ID' ),
				'Title' => Yii::t ( 'app', 'Titlu' ),
				'Text' => Yii::t ( 'app', 'Text' ) 
		];
	}
}