<?php

namespace app\models\Page;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Page\Page;

/**
 * PageSearch represents the model behind the search form about `app\models\Page\Page`.
 */
class PageSearch extends Page {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'ID' 
						],
						'integer' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Page::find ()->with ( 'currentLang' );
		
		// add conditions that should always apply here
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query 
		] );
		
		$this->load ( $params );
		
		if (! $this->validate ()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		// grid filtering conditions
		$query->andFilterWhere ( [ 
				'ID' => $this->ID 
		] );
		
		$query->andFilterWhere ( [ 
				'Status' => $this->Status 
		] );
		
		return $dataProvider;
	}
	public static function getByLink($link) {
		$model = Page::find ()->with ( 'currentLang' )->where ( [ 
				'Link' => $link 
		] )->one ();
		
		if ($model !== null) {
			return $model;
		}
		
		return new \yii\web\NotFoundHttpException ( 'Page not fount' );
	}
}
