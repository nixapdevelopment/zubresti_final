<?php

namespace app\models\Page;

use Yii;
use app\models\PageLang\PageLang;

/**
 * This is the model class for table "Page".
 *
 * @property integer $ID
 * @property string $Status
 *
 * @property PageLang[] $pageLangs
 * @property PageLang $currentLang
 */
class Page extends \yii\db\ActiveRecord {
	const StatusActive = 'Active';
	const StatusInactive = 'Inactive';
	public static function getStatusList() {
		return [ 
				self::StatusActive => 'Active',
				self::StatusInactive => 'Inactive' 
		];
	}
	public static function getStatusLabel($status) {
		$statusList = self::getStatusList ();
		return $statusList [$status];
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'Page';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Status' 
						],
						'string',
						'max' => 20 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'Status' => Yii::t("app",'Status') 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getPageLangs() {
		return $this->hasMany ( PageLang::className (), [ 
				'PageID' => 'ID' 
		] )->indexBy ( 'LangID' );
	}
	public function getCurrentLang() {
		return $this->hasOne ( PageLang::className (), [ 
				'PageID' => 'ID' 
		] )->where ( [ 
				'LangID' => Yii::$app->language 
		] );
	}
}
