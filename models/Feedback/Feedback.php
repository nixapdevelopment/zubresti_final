<?php

namespace app\models\Feedback;

use Yii;

/**
 * This is the model class for table "Feedback".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Phone
 * @property string $Email
 * @property string $Subject
 * @property string $Message
 * @property string $Date
 */
class Feedback extends \yii\db\ActiveRecord {
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'Feedback';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Name',
								'Phone',
								'Email',
								'Subject',
								'Message' 
						],
						'required' 
				],
				[ 
						[ 
								'Message' 
						],
						'string' 
				],
				[ 
						[ 
								'Date' 
						],
						'safe' 
				],
				[ 
						[ 
								'Name',
								'Phone',
								'Email',
								'Subject' 
						],
						'string',
						'max' => 255 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'Name' => Yii::t ( 'app', 'Nume' ),
				'Phone' => Yii::t ( 'app', 'Telefon' ),
				'Email' => Yii::t ( 'app', 'Email' ),
				'Subject' => Yii::t ( 'app', 'Subiect' ),
				'Message' => Yii::t ( 'app', 'Mesaj' ),
				'Date' => Yii::t ( 'app', 'Data' ) 
		];
	}
}
