<?php

namespace app\models\Feedback;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Feedback\Feedback;

/**
 * FeedbackSearch represents the model behind the search form about `app\models\Feedback\Feedback`.
 */
class FeedbackSearch extends Feedback {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'ID' 
						],
						'integer' 
				],
				[ 
						[ 
								'Name',
								'Phone',
								'Email',
								'Message',
								'Date' 
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Feedback::find ()->orderBy ( 'Date DESC' );
		
		// add conditions that should always apply here
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'pagination' => [ 
						'pageSize' => 10 
				] 
		] );
		
		$this->load ( $params );
		
		if (! $this->validate ()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		// grid filtering conditions
		$query->andFilterWhere ( [ 
				'ID' => $this->ID,
				'Date' => $this->Date 
		] );
		
		$query->andFilterWhere ( [ 
				'like',
				'Name',
				$this->Name 
		] )->andFilterWhere ( [ 
				'like',
				'Phone',
				$this->Phone 
		] )->andFilterWhere ( [ 
				'like',
				'Email',
				$this->Email 
		] )->andFilterWhere ( [ 
				'like',
				'Message',
				$this->Message 
		] );
		
		return $dataProvider;
	}
}
