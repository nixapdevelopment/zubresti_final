<?php

namespace app\models\ArticleFileLang;

use Yii;
use app\models\ArticleFile\ArticleFile;

/**
 * This is the model class for table "ArticleFileLang".
 *
 * @property integer $ID
 * @property integer $ArticleFileID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 * @property ArticleFile $articleFile
 */
class ArticleFileLang extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'ArticleFileLang';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'ArticleFileID',
								'LangID',
								'Title',
								'Text' 
						],
						'required' 
				],
				[ 
						[ 
								'ArticleFileID' 
						],
						'integer' 
				],
				[ 
						[ 
								'Text' 
						],
						'string' 
				],
				[ 
						[ 
								'LangID' 
						],
						'string',
						'max' => 2 
				],
				[ 
						[ 
								'Title' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'ArticleFileID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => ArticleFile::className (),
						'targetAttribute' => [ 
								'ArticleFileID' => 'ID' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => Yii::t ( 'app', 'ID' ),
				'ArticleFileID' => Yii::t ( 'app', 'Article File ID' ),
				'LangID' => Yii::t ( 'app', 'Lang ID' ),
				'Title' => Yii::t ( 'app', 'Title' ),
				'Text' => Yii::t ( 'app', 'Text' ) 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getArticleFile() {
		return $this->hasOne ( ArticleFile::className (), [ 
				'ID' => 'ArticleFileID' 
		] );
	}
	public function getTitle() {
		return empty ( trim ( $this->Title ) ) ? 'fdg' : $this->Title;
	}
}
