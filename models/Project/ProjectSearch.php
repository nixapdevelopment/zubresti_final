<?php

namespace app\models\Project;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Project\Project;

/**
 * ProjectSearch represents the model behind the search form about `app\models\Project\Project`.
 */
class ProjectSearch extends Project {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'ID',
								'Creator' 
						],
						'integer' 
				],
				[ 
						[ 
								'Name',
								'Created',
								'StartDate',
								'EndDate',
								'DueDate',
								'Priority',
								'GitRepository',
								'Status' 
						],
						'safe' 
				],
				[ 
						[ 
								'Price' 
						],
						'number' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Project::find ()->with ( 'creator', 'client' );
		
		// add conditions that should always apply here
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query 
		] );
		
		$this->load ( $params );
		
		if (! $this->validate ()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		// grid filtering conditions
		$query->andFilterWhere ( [ 
				'ID' => $this->ID,
				'Creator' => $this->Creator,
				'Created' => $this->Created,
				'StartDate' => $this->StartDate,
				'EndDate' => $this->EndDate,
				'DueDate' => $this->DueDate,
				'Price' => $this->Price 
		] );
		
		$query->andFilterWhere ( [ 
				'like',
				'Name',
				$this->Name 
		] )->andFilterWhere ( [ 
				'like',
				'Priority',
				$this->Priority 
		] )->andFilterWhere ( [ 
				'like',
				'GitRepository',
				$this->GitRepository 
		] )->andFilterWhere ( [ 
				'like',
				'Status',
				$this->Status 
		] );
		
		return $dataProvider;
	}
}
