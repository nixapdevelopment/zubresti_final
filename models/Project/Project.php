<?php

namespace app\models\Project;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\behaviors\AttributeBehavior;
use app\models\User\User;

/**
 * This is the model class for table "Project".
 *
 * @property integer $ID
 * @property integer $Creator
 * @property integer $Client
 * @property string $Name
 * @property string $Created
 * @property string $StartDate
 * @property string $EndDate
 * @property string $DueDate
 * @property string $Price
 * @property string $Priority
 * @property string $GitRepository
 * @property string $Status
 *
 * @property User $creator
 */
class Project extends \yii\db\ActiveRecord {
	const PriorityHight = 'Hight';
	const PriorityMiddle = 'Middle';
	const PriorityLow = 'Low';
	const StatusNew = 'New';
	const StatusWait = 'Wait';
	const StatusDone = 'Done';
	const StatusClosed = 'Closed';
	const StatusCanceled = 'Canceled';
	public static function getPriorityList() {
		return [ 
				Project::PriorityHight => 'Hight',
				Project::PriorityMiddle => 'Middle',
				Project::PriorityLow => 'Low' 
		];
	}
	public static function getPriorityLabel($priority) {
		$priorityList = self::getPriorityList ();
		return $priorityList [$priority];
	}
	public static function getStatusList() {
		return [ 
				Project::StatusNew => 'New',
				Project::StatusWait => 'Wait',
				Project::StatusDone => 'Done',
				Project::StatusClosed => 'Closed',
				Project::StatusCanceled => 'Canceled' 
		];
	}
	public static function getStatusLabel($status) {
		$statusList = self::getStatusList ();
		return $statusList [$status];
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'Project';
	}
	public function behaviors() {
		return [ 
				[ 
						'class' => BlameableBehavior::className (),
						'createdByAttribute' => 'Creator',
						'updatedByAttribute' => false 
				],
				[ 
						'class' => TimestampBehavior::className (),
						'createdAtAttribute' => 'Created',
						'updatedAtAttribute' => false,
						'value' => new Expression ( 'NOW()' ) 
				] 
		];
	}
	public function beforeSave($insert = true) {
		$this->StartDate = date ( 'c', strtotime ( $this->StartDate ) );
		$this->EndDate = date ( 'c', strtotime ( $this->EndDate ) );
		$this->DueDate = date ( 'c', strtotime ( $this->DueDate ) );
		return parent::beforeSave ( $insert );
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Name',
								'StartDate',
								'EndDate',
								'Client',
								'Priority',
								'Status' 
						],
						'required' 
				],
				[ 
						[ 
								'Creator',
								'Client' 
						],
						'integer' 
				],
				[ 
						[ 
								'Created',
								'StartDate',
								'EndDate',
								'DueDate' 
						],
						'safe' 
				],
				[ 
						[ 
								'DueDate' 
						],
						'default',
						'value' => $this->EndDate 
				],
				[ 
						[ 
								'Price' 
						],
						'number' 
				],
				[ 
						[ 
								'Price' 
						],
						'default',
						'value' => 0 
				],
				[ 
						[ 
								'Priority' 
						],
						'default',
						'value' => self::PriorityMiddle 
				],
				[ 
						[ 
								'Name' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'Priority',
								'Status' 
						],
						'string',
						'max' => 20 
				],
				[ 
						[ 
								'GitRepository' 
						],
						'string',
						'max' => 500 
				],
				[ 
						[ 
								'GitRepository' 
						],
						'default',
						'value' => '' 
				],
				[ 
						[ 
								'Creator' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => User::className (),
						'targetAttribute' => [ 
								'Creator' => 'ID' 
						] 
				],
				[ 
						[ 
								'Client' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => User::className (),
						'targetAttribute' => [ 
								'Client' => 'ID' 
						] 
				],
				[ 
						[ 
								'Status' 
						],
						'default',
						'value' => self::StatusNew 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'Creator' => Yii::t("app", 'Creator'),
				'Client' => Yii::t("app",'Client'),
				'Name' => Yii::t("äpp",'Nume'),
				'Created' => Yii::t("app",'Creat'),
				'StartDate' => Yii::t("app", 'Data începerii'),
				'EndDate' => Yii::t("app", 'Data sfîrșitului'),
				'DueDate' => 'Due Date',
				'Price' => 'Price',
				'Priority' => 'Priority',
				'GitRepository' => 'Git Repository',
				'Status' => 'Status' 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCreator() {
		return $this->hasOne ( User::className (), [ 
				'ID' => 'Creator' 
		] );
	}
	public function getClient() {
		return $this->hasOne ( User::className (), [ 
				'ID' => 'Client' 
		] );
	}
}
