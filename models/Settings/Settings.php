<?php

namespace app\models\Settings;

use Yii;

/**
 * This is the model class for table "Settings".
 *
 * @property integer $ID
 * @property string $Name
 * @property string $Value
 */
class Settings extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'Settings';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Value' 
						],
						'string' 
				],
				[ 
						[ 
								'Name' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'Name' 
						],
						'unique' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'Name' => Yii::t("app", 'Nume'),
				'Value' => Yii::t("app", 'Valoare') 
		];
	}
}
