<?php

namespace app\models\Menu;

use Yii;
use app\models\MenuLang\MenuLang;

/**
 * This is the model class for table "Menu".
 *
 * @property integer $ID
 *
 * @property MenuLang[] $langs
 * @property MenuLang $lang
 */
class Menu extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'Menu';
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID' 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLangs() {
		return $this->hasMany ( MenuLang::className (), [ 
				'MenuID' => 'ID' 
		] )->indexBy ( 'LangID' );
	}
	public function getLang() {
		return $this->hasOne ( MenuLang::className (), [ 
				'MenuID' => 'ID' 
		] )->where ( [ 
				'LangID' => Yii::$app->language 
		] );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getItems() {
		return $this->hasMany ( \app\models\MenuItem\MenuItem::className (), [ 
				'MenuID' => 'ID' 
		] )->orderBy ( 'ParentID, Position' )->indexBy ( 'ID' );
	}
}
