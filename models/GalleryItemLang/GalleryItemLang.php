<?php

namespace app\models\GalleryItemLang;

use Yii;
use app\models\GalleryItem\GalleryItem;

/**
 * This is the model class for table "GalleryItemLang".
 *
 * @property integer $ID
 * @property integer $GalleryItemID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 *
 */
class GalleryItemLang extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'GalleryItemLang';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'GalleryItemID',
								'LangID' 
						],
						'required' 
				],
				[ 
						[ 
								'GalleryItemID' 
						],
						'integer' 
				],
				[ 
						[ 
								'Text' 
						],
						'string' 
				],
				[ 
						[ 
								'LangID' 
						],
						'string',
						'max' => 2 
				],
				[ 
						[ 
								'Title' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'GalleryItemID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => GalleryItem::className (),
						'targetAttribute' => [ 
								'GalleryItemID' => 'ID' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => Yii::t ( 'app', 'ID' ),
				'GalleryItemID' => Yii::t ( 'app', 'Galerie Item ID' ),
				'LangID' => Yii::t ( 'app', 'Lang ID' ),
				'Title' => Yii::t ( 'app', 'Titlu' ),
				'Text' => Yii::t ( 'app', 'Text' ) 
		];
	}
	public function getTitle() {
		return empty ( $this->Title ) ? '<br />' : $this->Title;
	}
}