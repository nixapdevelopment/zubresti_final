<?php

namespace app\models\ArticleImage;

use Yii;
use app\models\Article\Article;
use app\models\ArticleImageLang\ArticleImageLang;

/**
 * This is the model class for table "ArticleImage".
 *
 * @property integer $ID
 * @property integer $ArticleID
 * @property string $Thumb
 * @property string $Image
 * @property integer $Position
 *
 * @property Article $article
 */
class ArticleImage extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'ArticleImage';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'ArticleID',
								'Thumb',
								'Image',
								'Position' 
						],
						'required' 
				],
				[ 
						[ 
								'ArticleID',
								'Position' 
						],
						'integer' 
				],
				[ 
						[ 
								'Thumb',
								'Image' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'ArticleID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => Article::className (),
						'targetAttribute' => [ 
								'ArticleID' => 'ID' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'ArticleID' => 'Article ID',
				'Thumb' => Yii::t ( 'app', 'Thumb' ),
				'Image' => Yii::t ( 'app', 'Image' ),
				'Position' => Yii::t ( 'app', 'Position' ) 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLang() {
		return $this->hasOne ( ArticleImageLang::className (), [ 
				'ArticleImageID' => 'ID' 
		] )->where ( [ 
				'LangID' => Yii::$app->language 
		] );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLangs() {
		return $this->hasMany ( ArticleImageLang::className (), [ 
				'ArticleImageID' => 'ID' 
		] )->indexBy ( 'LangID' );
	}
	public function getThumbLink() {
		return Yii::getAlias ( '@web/uploads/article/' . $this->Thumb );
	}
	public function getImageLink() {
		return Yii::getAlias ( '@web/uploads/article/' . $this->Image );
	}
}