<?php

namespace app\models\LocationLang;

use Yii;

/**
 * This is the model class for table "LocationLang".
 *
 * @property integer $ID
 * @property integer $LocationID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 * @property string $SeoTitle
 * @property string $Keywords
 * @property string $Description
 *
 * @property Location $location
 */
class LocationLang extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'LocationLang';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Title',
								'Text',
								'SeoTitle',
								'Keywords',
								'Description' 
						],
						'required' 
				],
				[ 
						[ 
								'Text' 
						],
						'string' 
				],
				[ 
						[ 
								'Title',
								'SeoTitle' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'Keywords',
								'Description' 
						],
						'string',
						'max' => 500 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'LocationID' => Yii::t("app", 'ID Locație'),
				'LangID' => Yii::t("app", 'Lang ID'),
				'Title' => Yii::t("app", 'Titlu'),
				'Text' => Yii::t("app", 'Text'),
				'SeoTitle' => Yii::t("app", 'Seo Titlu'),
				'Keywords' => Yii::t("app", 'Cuvinte-cheie'),
				'Description' => Yii::t("app", 'Descriere')
        ];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLocation() {
		return $this->hasOne ( Location::className (), [ 
				'ID' => 'LocationID' 
		] );
	}
}
