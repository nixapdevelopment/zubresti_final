<?php

namespace app\models\SliderItem;

use Yii;
use app\models\Slider\Slider;
use app\models\SliderItemLang\SliderItemLang;

/**
 * This is the model class for table "SliderItem".
 *
 * @property integer $ID
 * @property integer $SliderID
 * @property string $Link
 * @property string $Image
 * @property integer $Position
 *
 * @property Slider $slider
 * @property SliderItemLang[] $sliderItemLangs
 */
class SliderItem extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'SliderItem';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'SliderID' 
						],
						'required' 
				],
				[ 
						[ 
								'SliderID',
								'Position' 
						],
						'integer' 
				],
				[ 
						[ 
								'Image' 
						],
						'string',
						'max' => 255 
				],
				[ 
						'Image',
						'default',
						'value' => $this->Image 
				],
				[ 
						'Link',
						'default',
						'value' => '' 
				],
				[ 
						[ 
								'SliderID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => Slider::className (),
						'targetAttribute' => [ 
								'SliderID' => 'ID' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'SliderID' => Yii::t("app", 'ID Sliderului'),
				'Image' => Yii::t ( 'app', 'Imagine' ),
				'Link' => 'Link',
				'Position' => Yii::t("app", 'Poziție') 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getSlider() {
		return $this->hasOne ( Slider::className (), [ 
				'ID' => 'SliderID' 
		] );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLangs() {
		return $this->hasMany ( SliderItemLang::className (), [ 
				'SliderItemID' => 'ID' 
		] )->indexBy ( 'LangID' );
	}
	public function getLang() {
		return $this->hasOne ( SliderItemLang::className (), [ 
				'SliderItemID' => 'ID' 
		] )->where ( [ 
				'LangID' => Yii::$app->language 
		] );
	}
	public function getImageUrl() {
		return '@web/uploads/slider/' . $this->Image;
	}
	public function getLink() {
		return \yii\helpers\Url::to ( [ 
				$this->Link 
		] );
	}
}
