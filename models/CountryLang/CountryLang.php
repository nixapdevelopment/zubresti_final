<?php

namespace app\models\CountryLang;

use app\models\Country\Country;

/**
 * This is the model class for table "CountryLang".
 *
 * @property integer $ID
 * @property integer $CountryID
 * @property string $LangID
 * @property string $Name
 *
 * @property Country $country
 */
class CountryLang extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'CountryLang';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Name' 
						],
						'required' 
				],
				[ 
						[ 
								'Name' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'LangID',
								'CountryID' 
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'CountryID' => 'Country ID',
				'LangID' => 'Lang ID',
				'Name' => Yii::t("app", 'Nume') 
		];
	}
}