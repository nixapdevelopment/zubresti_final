<?php

namespace app\models\ArticleLang;

use Yii;

/**
 * This is the model class for table "ArticleLang".
 *
 * @property integer $ID
 * @property integer $ArticleID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 * @property string $SeoTitle
 * @property string $Keywords
 * @property string $Description
 *
 * @property Article $article
 */
class ArticleLang extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'ArticleLang';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'LangID',
								'Title' 
						],
						'required' 
				],
				[ 
						[ 
								'ArticleID' 
						],
						'integer' 
				],
				[ 
						[ 
								'Text' 
						],
						'string' 
				],
				[ 
						[ 
								'LangID' 
						],
						'string',
						'max' => 2 
				],
				[ 
						[ 
								'Title',
								'SeoTitle',
								'Keywords',
								'Description' 
						],
						'string',
						'max' => 255 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'ArticleID' => 'Article ID',
				'LangID' => 'Lang ID',
				'Title' => Yii::t ( 'app', 'Titlu' ),
				'Text' => Yii::t ( 'app', 'Text' ),
				'SeoTitle' => Yii::t ( 'app', 'Seo Titlu' ),
				'Keywords' => Yii::t ( 'app', 'Cuvinte-cheie' ),
				'Description' => Yii::t ( 'app', 'Descriere' ) 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getArticle() {
		return $this->hasOne ( Article::className (), [ 
				'ID' => 'ArticleID' 
		] );
	}
	public function getText() {
		return \yii\helpers\Html::tag ( 'div', $this->Text, [ 
				'class' => 'article-content article-content-' . $this->article->Type 
		] );
	}
}
