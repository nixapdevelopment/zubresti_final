<?php

namespace app\models\Location;

use Yii;
use app\models\Country\Country;
use app\models\Image\Image;
use app\models\CountryRegion\CountryRegion;
use app\models\LocationLang\LocationLang;
use app\models\LocationCategory\LocationCategory;
use app\models\LocationImage\LocationImage;

/**
 * This is the model class for table "Location".
 *
 * @property integer $ID
 * @property integer $CountryID
 * @property integer $CountryRegionID
 * @property string $Link
 * @property float $Price
 * @property float $Long
 * @property float $Lat
 *
 * @property Country $country
 * @property CountryRegion $countryRegion
 * @property LocationLang[] $locationLangs
 */
class Location extends \yii\db\ActiveRecord {
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'Location';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'CountryID',
								'Link',
								'Price' 
						],
						'required' 
				],
				[ 
						[ 
								'Link' 
						],
						'unique' 
				],
				[ 
						[ 
								'Long',
								'Lat',
								'Price' 
						],
						'default',
						'value' => 0 
				],
				[ 
						[ 
								'CountryID' 
						],
						'integer' 
				],
				[ 
						[ 
								'CountryID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => Country::className (),
						'targetAttribute' => [ 
								'CountryID' => 'ID' 
						] 
				],
				[ 
						[ 
								'CountryRegionID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => CountryRegion::className (),
						'targetAttribute' => [ 
								'CountryRegionID' => 'ID' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'CountryID' => Yii::t("app", "Țară ID"),
				'CountryRegionID' => Yii::t("app", 'ID Regiunea țării'),
				'Long' => Yii::t("app", 'Longitudine'),
				'Lat' => Yii::t("app", 'Latitudine') 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCountry() {
		return $this->hasOne ( Country::className (), [ 
				'ID' => 'CountryID' 
		] )->with ( 'countryLangs' );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCountryRegion() {
		return $this->hasOne ( CountryRegion::className (), [ 
				'ID' => 'CountryRegionID' 
		] )->with ( 'countryRegionLangs' );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLocationLangs() {
		return $this->hasMany ( LocationLang::className (), [ 
				'LocationID' => 'ID' 
		] )->orderBy ( 'ID' );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getImage() {
		return $this->hasOne ( LocationImage::className (), [ 
				'LocationID' => 'ID' 
		] )->with ( 'image' );
	}
	public function getLocationImages() {
		return $this->hasMany ( LocationImage::className (), [ 
				'LocationID' => 'ID' 
		] )->with ( 'image' );
	}
	public function getLocationCategories() {
		return $this->hasMany ( LocationCategory::className (), [ 
				'LocationID' => 'ID' 
		] )->with ( 'category' );
	}
	public static function getCurrentLang($lang = 'ro') {
		$_this = new Location ();
		return $_this->hasOne ( LocationLang::className (), [ 
				'LocationID' => 'ID' 
		] )->where ( [ 
				'LangID' => Yii::$app->language 
		] );
	}
	public function getFullLink() {
		return \yii\helpers\Url::to ( '/' . reset ( $this->locationCategories )->category->Link . '/' . $this->country->Link . '/' . $this->Link . '-offer' );
	}
}
