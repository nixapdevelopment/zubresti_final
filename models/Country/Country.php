<?php

namespace app\models\Country;

use app\models\CountryLang\CountryLang;
use herroffizier\yii2tv\TranslitValidator;

/**
 * This is the model class for table "Country".
 *
 * @property integer $ID
 * @property string $Code
 * @property string $Link
 *
 * @property CountryLang[] $countryLangs
 * @property CountryRegion[] $countryRegions
 */
class Country extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'Country';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Code',
								'Link' 
						],
						'required' 
				],
				[ 
						[ 
								'Code' 
						],
						'string',
						'max' => 2 
				],
				[ 
						[ 
								'Link' 
						],
						'string',
						'max' => 255 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'Code' => 'Code',
				'Link' => 'Link' 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCountryLangs() {
		return $this->hasMany ( CountryLang::className (), [ 
				'CountryID' => 'ID' 
		] )->indexBy ( 'LangID' );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCurrentLang() {
		return $this->hasOne ( CountryLang::className (), [ 
				'CountryID' => 'ID' 
		] )->where ( [ 
				'LangID' => \Yii::$app->language 
		] );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCountryRegions() {
		return $this->hasMany ( \app\models\CountryRegion\CountryRegion::className (), [ 
				'CountryID' => 'ID' 
		] )->with ( 'countryRegionLangs' );
	}
	public static function listForDropdown($addEmpty = true, $lang = null) {
		$models = Country::find ()->joinWith ( 'currentLang', true, 'LEFT JOIN' )->where ( [ 
				'LangID' => \Yii::$app->language 
		] )->all ();
		
		$return = $addEmpty ? [ 
				'' => '-' 
		] : [ ];
		foreach ( $models as $model ) {
			$return [$model->ID] = $model->currentLang->Name;
		}
		
		return $return;
	}
}
