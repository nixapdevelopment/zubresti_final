<?php

namespace app\models\Country;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Country\Country;

/**
 * CountrySearch represents the model behind the search form about `app\models\Country\Country`.
 */
class CountrySearch extends Country {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'ID' 
						],
						'integer' 
				],
				[ 
						[ 
								'Code' 
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Country::find ();
		
		// add conditions that should always apply here
		$query->joinWith ( 'countryLangs', true, 'LEFT JOIN' ); // this added
		$query->where ( [ 
				'LangID' => reset ( \Yii::$app->params ['siteLanguages'] ) 
		] );
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'pagination' => [ 
						'pageSize' => 10 
				] 
		] );
		
		$this->load ( $params );
		
		if (! $this->validate ()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		// grid filtering conditions
		$query->andFilterWhere ( [ 
				'ID' => $this->ID 
		] );
		
		$query->andFilterWhere ( [ 
				'like',
				'Code',
				$this->Code 
		] );
		
		return $dataProvider;
	}
}
