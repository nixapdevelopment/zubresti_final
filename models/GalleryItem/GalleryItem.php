<?php

namespace app\models\GalleryItem;

use Yii;
use app\models\Gallery\Gallery;
use app\models\GalleryItemLang\GalleryItemLang;

/**
 * This is the model class for table "GalleryItem".
 *
 * @property integer $ID
 * @property integer $GalleryID
 * @property string $Thumb
 * @property string $Value
 * @property string $Type
 * @property integer $Position
 *
 * @property Gallery $gallery
 * @property GalleryItemLang $lang
 * @property GalleryItemLang[] $langs
 */
class GalleryItem extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'GalleryItem';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'GalleryID',
								'Value',
								'Type' 
						],
						'required' 
				],
				[ 
						[ 
								'GalleryID',
								'Position' 
						],
						'integer' 
				],
				[ 
						[ 
								'Value',
								'Thumb' 
						],
						'string' 
				],
				[ 
						[ 
								'Type' 
						],
						'string',
						'max' => 50 
				],
				[ 
						[ 
								'GalleryID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => Gallery::className (),
						'targetAttribute' => [ 
								'GalleryID' => 'ID' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => Yii::t ( 'app', 'ID' ),
				'GalleryID' => Yii::t ( 'app', 'Galerie ID' ),
				'Value' => Yii::t ( 'app', 'Imagine' ),
				'Type' => Yii::t ( 'app', 'Tipul' ),
				'Position' => Yii::t ( 'app', 'Poziție' ) 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getGallery() {
		return $this->hasOne ( Gallery::className (), [ 
				'ID' => 'GalleryID' 
		] );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLangs() {
		return $this->hasMany ( GalleryItemLang::className (), [ 
				'GalleryItemID' => 'ID' 
		] );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getLang() {
		return $this->hasOne ( GalleryItemLang::className (), [ 
				'GalleryItemID' => 'ID' 
		] )->where ( [ 
				'LangID' => Yii::$app->language 
		] );
	}
}
