<?php

namespace app\models\News;

use Yii;
use yii\db\ActiveRecord;
use app\models\NewsLang\NewsLang;
use yii\behaviors\AttributeBehavior;

/**
 * This is the model class for table "News".
 *
 * @property integer $ID
 * @property string $Date
 * @property string $Status
 *
 * @property NewsLang[] $newsLangs
 * @property NewsLang $currentLang
 */
class News extends ActiveRecord {
	const StatusActive = 'Active';
	const StatusInactive = 'Inactive';
	public static function getStatusList() {
		return [ 
				self::StatusActive => 'Active',
				self::StatusInactive => 'Inactive' 
		];
	}
	public static function getStatusLabel($status) {
		$statusList = self::getStatusList ();
		return $statusList [$status];
	}
	public function behaviors() {
		return [ 
				[ 
						'class' => AttributeBehavior::className (),
						'attributes' => [ 
								ActiveRecord::EVENT_BEFORE_INSERT => 'Date',
								ActiveRecord::EVENT_BEFORE_UPDATE => 'Date' 
						],
						'value' => function ($event) {
							return date ( 'Y-m-d', strtotime ( $this->Date ) );
						} 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'News';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Date',
								'Status' 
						],
						'required' 
				],
				[ 
						[ 
								'Status' 
						],
						'string',
						'max' => 20 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'Date' => Yii::t("app", "Data"),
				'Status' => Yii::t("app", 'Status') 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getNewsLangs() {
		return $this->hasMany ( NewsLang::className (), [ 
				'NewsID' => 'ID' 
		] )->indexBy ( 'LangID' );
	}
	public function getCurrentLang() {
		return $this->hasOne ( NewsLang::className (), [ 
				'NewsID' => 'ID' 
		] )->where ( [ 
				'LangID' => Yii::$app->language 
		] );
	}
}