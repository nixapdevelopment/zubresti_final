<?php

namespace app\models\News;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\News\News;
use yii\db\Expression;

/**
 * PageSearch represents the model behind the search form about `app\models\News\News`.
 */
class NewsSearch extends News {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'ID' 
						],
						'integer' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = News::find ()->with ( 'currentLang' );
		
		// add conditions that should always apply here
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query 
		] );
		
		$this->load ( $params );
		
		if (! $this->validate ()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		// grid filtering conditions
		if (! empty ( $this->Date )) {
			$query->andFilterWhere ( [ 
					'Date' => date ( 'Y-m-d', strtotime ( $this->Date ) ) 
			] );
		}
		
		$query->andFilterWhere ( [ 
				'Status' => $this->Status 
		] );
		
		return $dataProvider;
	}
	public static function getByLink($link) {
		$model = Page::find ()->with ( 'currentLang' )->where ( [ 
				'Link' => $link 
		] )->one ();
		
		if ($model !== null) {
			return $model;
		}
		
		return new \yii\web\NotFoundHttpException ( 'Article not fount' );
	}
}
