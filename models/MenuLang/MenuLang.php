<?php

namespace app\models\MenuLang;

use Yii;

/**
 * This is the model class for table "MenuLang".
 *
 * @property integer $ID
 * @property integer $MenuID
 * @property string $LangID
 * @property string $Name
 *
 * @property Menu $menu
 */
class MenuLang extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'MenuLang';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'LangID',
								'Name' 
						],
						'required' 
				],
				[ 
						[ 
								'LangID' 
						],
						'string',
						'max' => 2 
				],
				[ 
						[ 
								'Name' 
						],
						'string',
						'max' => 255 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'MenuID' => Yii::t("app", 'ID Meniu'),
				'LangID' => Yii::t("app", 'Lang ID'),
				'Name' => Yii::t ( 'app', 'Numele meniului' ) 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getMenu() {
		return $this->hasOne ( Menu::className (), [ 
				'ID' => 'MenuID' 
		] );
	}
}
