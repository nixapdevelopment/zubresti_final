<?php

namespace app\models\CountryRegion;

use Yii;
use app\modules\Country\Country;
use app\models\CountryRegionLang\CountryRegionLang;

/**
 * This is the model class for table "CountryRegion".
 *
 * @property integer $ID
 * @property integer $CountryID
 *
 * @property Country $country
 * @property CountryRegionLang[] $countryRegionLangs
 */
class CountryRegion extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'CountryRegion';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'CountryID' 
						],
						'required' 
				],
				[ 
						[ 
								'CountryID' 
						],
						'integer' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'CountryID' => 'Country ID' 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCountry() {
		return $this->hasOne ( Country::className (), [ 
				'ID' => 'CountryID' 
		] );
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCountryRegionLangs() {
		return $this->hasMany ( CountryRegionLang::className (), [ 
				'CountryRegionID' => 'ID' 
		] )->orderBy ( 'ID' );
	}
	public function getCurrentLang() {
		return $this->hasOne ( CountryRegionLang::className (), [ 
				'CountryRegionID' => 'ID' 
		] )->where ( [ 
				'LangID' => Yii::$app->language 
		] );
	}
	public static function listForDropdown($countryID, $addEmpty = true, $lang = null) {
		if (empty ( $lang )) {
			$lang = reset ( \Yii::$app->params ['siteLanguages'] );
		}
		
		$models = CountryRegion::find ()->joinWith ( 'countryRegionLangs', true, 'LEFT JOIN' )->where ( [ 
				'CountryID' => $countryID,
				'LangID' => $lang 
		] )->all ();
		
		$return = $addEmpty ? [ 
				'' => '-' 
		] : [ ];
		foreach ( $models as $model ) {
			$return [$model->ID] = $model->countryRegionLangs [0]->Name;
		}
		
		return $return;
	}
}
