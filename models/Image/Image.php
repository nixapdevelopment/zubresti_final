<?php

namespace app\models\Image;

use Yii;

/**
 * This is the model class for table "Image".
 *
 * @property integer $ID
 * @property string $Thumb
 * @property string $Image
 */
class Image extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'Image';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Thumb',
								'Image' 
						],
						'required' 
				],
				[ 
						[ 
								'Thumb',
								'Image' 
						],
						'string',
						'max' => 255 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'Thumb' => Yii::t ( 'app', 'Imagine-preview' ),
				'Image' => Yii::t ( 'app', 'Imagine' ) 
		];
	}
}
