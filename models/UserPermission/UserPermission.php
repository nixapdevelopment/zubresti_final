<?php

namespace app\models\UserPermission;

use Yii;
use app\models\User\User;

/**
 * This is the model class for table "UserPermission".
 *
 * @property integer $UserID
 * @property string $Permission
 *
 * @property User $user
 */
class UserPermission extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'UserPermission';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'UserID',
								'Permission' 
						],
						'required' 
				],
				[ 
						[ 
								'UserID' 
						],
						'integer' 
				],
				[ 
						[ 
								'Permission' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'UserID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => User::className (),
						'targetAttribute' => [ 
								'UserID' => 'ID' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'UserID' => Yii::t("app", 'Utilizator ID'),
				'Permission' => Yii::t("app", 'Permisiuni') 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne ( User::className (), [ 
				'ID' => 'UserID' 
		] );
	}
}
