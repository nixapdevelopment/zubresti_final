<?php

namespace app\models\Category;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Category\Category;

/**
 * CategorySearch represents the model behind the search form about `app\models\Category\Category`.
 */
class CategorySearch extends Category {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'ID' 
						],
						'integer' 
				],
				[ 
						[ 
								'Image',
								'Status' 
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = Category::find ()->with ( 'categoryLangs' );
		
		// add conditions that should always apply here
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query 
		] );
		
		$this->load ( $params );
		
		if (! $this->validate ()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		// grid filtering conditions
		$query->andFilterWhere ( [ 
				'ID' => $this->ID 
		] );
		
		$query->andFilterWhere ( [ 
				'like',
				'Image',
				$this->Image 
		] )->andFilterWhere ( [ 
				'like',
				'Status',
				$this->Status 
		] );
		
		return $dataProvider;
	}
	public static function getByLink($link) {
		$model = Category::find ()->with ( 'currentLang' )->where ( [ 
				'Link' => $link 
		] )->one ();
		
		if ($model !== null) {
			return $model;
		}
		
		return new \yii\web\NotFoundHttpException ( 'Category not fount' );
	}
}
