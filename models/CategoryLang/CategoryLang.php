<?php

namespace app\models\CategoryLang;

use Yii;
use app\models\Category\Category;

/**
 * This is the model class for table "CategoryLang".
 *
 * @property integer $ID
 * @property integer $CategoryID
 * @property string $LangID
 * @property string $Title
 * @property string $Text
 * @property string $SeoTitle
 * @property string $Keywords
 * @property string $Description
 *
 * @property Category $category
 */
class CategoryLang extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'CategoryLang';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'LangID',
								'Title',
								'Text' 
						],
						'required' 
				],
				[ 
						[ 
								'Text' 
						],
						'string' 
				],
				[ 
						[ 
								'LangID' 
						],
						'string',
						'max' => 2 
				],
				[ 
						[ 
								'Title',
								'SeoTitle' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'Keywords',
								'Description' 
						],
						'string',
						'max' => 500 
				],
				[ 
						[ 
								'SeoTitle',
								'Keywords',
								'Description' 
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'CategoryID' => 'Category ID',
				'LangID' => 'Lang ID',
				'Title' => Yii::t("app", 'Titlu'),
				'Text' => Yii::t("app", 'Text'),
				'SeoTitle' => Yii::t("app", 'Seo Titlu'),
				'Keywords' => Yii::t("app", 'Cuvinte-cheie'),
				'Description' => Yii::t("app", 'Descriere') 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getCategory() {
		return $this->hasOne ( Category::className (), [ 
				'ID' => 'CategoryID' 
		] );
	}
}
