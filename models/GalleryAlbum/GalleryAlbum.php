<?php

namespace app\models\GalleryAlbum;

use Yii;

/**
 * This is the model class for table "GalleryAlbum".
 *
 * @property integer $ID
 * @property integer $Position
 * @property string $Image
 *
 * @property GalleryAlbumLang[] $galleryAlbumLangs
 */
class GalleryAlbum extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'GalleryAlbum';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Position' 
						],
						'integer' 
				],
				[ 
						[ 
								'Image' 
						],
						'string',
						'max' => 255 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => Yii::t ( 'app', 'ID' ),
				'Position' => Yii::t ( 'app', 'Poziție' ),
				'Image' => Yii::t ( 'app', 'Imagine' ) 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getGalleryAlbumLangs() {
		return $this->hasMany ( GalleryAlbumLang::className (), [ 
				'GalleryAlbumID' => 'ID' 
		] );
	}
}
