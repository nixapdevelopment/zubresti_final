<?php

namespace app\models\GalleryAlbum;

use yii\data\ActiveDataProvider;
use app\models\GalleryAlbum\GalleryAlbum;

/**
 * GallerySearch represents the model behind the search form about `app\models\GalleryAlbum\GalleryAlbum`.
 */
class GalleryAlbumSearch extends GalleryAlbum {
	public function search() {
		$query = GalleryAlbum::find ()->with ( [ 
				'lang',
				'items' 
		] )->orderBy ( 'Position' );
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query 
		] );
		
		return $dataProvider;
	}
}
