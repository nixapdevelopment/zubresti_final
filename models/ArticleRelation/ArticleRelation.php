<?php

namespace app\models\ArticleRelation;

use Yii;
use app\models\Article\Article;

/**
 * This is the model class for table "ArticleRelation".
 *
 * @property integer $ID
 * @property integer $ParentArticleID
 * @property integer $ChildArticleID
 */
class ArticleRelation extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'ArticleRelation';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'ParentArticleID',
								'ChildArticleID' 
						],
						'required' 
				],
				[ 
						[ 
								'ParentArticleID',
								'ChildArticleID' 
						],
						'integer' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'ParentArticleID' => 'Parent Article ID',
				'ChildArticleID' => 'Child Article ID' 
		];
	}
	public function getParentArticle() {
		return $this->hasOne ( Article::className (), [ 
				'ID' => 'ParentArticleID' 
		] );
	}
	public function getChildArticle() {
		return $this->hasOne ( Article::className (), [ 
				'ID' => 'ChildArticleID' 
		] );
	}
}
