<?php

namespace app\models\MenuItemLang;

use Yii;
use app\models\MenuItem\MenuItem;

/**
 * This is the model class for table "MenuItemLang".
 *
 * @property integer $ID
 * @property integer $MenuItemID
 * @property string $LangID
 * @property string $Title
 *
 * @property MenuItem $menuItem
 */
class MenuItemLang extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'MenuItemLang';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'MenuItemID',
								'LangID',
								'Title' 
						],
						'required' 
				],
				[ 
						[ 
								'MenuItemID' 
						],
						'integer' 
				],
				[ 
						[ 
								'LangID' 
						],
						'string',
						'max' => 2 
				],
				[ 
						[ 
								'Title' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'MenuItemID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => MenuItem::className (),
						'targetAttribute' => [ 
								'MenuItemID' => 'ID' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'MenuItemID' => Yii::t("äpp", 'ID Itemului Meniu'),
				'LangID' => Yii::t("äpp", 'Lang ID'),
				'Title' => Yii::t ( 'app', 'Titlu' ) 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getMenuItem() {
		return $this->hasOne ( MenuItem::className (), [ 
				'ID' => 'MenuItemID' 
		] );
	}
}
