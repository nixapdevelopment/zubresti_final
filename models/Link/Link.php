<?php

namespace app\models\Link;

use Yii;
use app\models\Article\Article;
use yii\behaviors\SluggableBehavior;

/**
 * This is the model class for table "Link".
 *
 * @property integer $ID
 * @property integer $ArticleID
 * @property string $Link
 *
 * @property Article $article
 */
class Link extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'Link';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'ArticleID',
								'Link' 
						],
						'required' 
				],
				[ 
						[ 
								'ArticleID' 
						],
						'integer' 
				],
				[ 
						[ 
								'Link' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'Link' 
						],
						'unique' 
				],
				[ 
						[ 
								'ArticleID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => Article::className (),
						'targetAttribute' => [ 
								'ArticleID' => 'ID' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'ArticleID' => Yii::t("app", "Articol ID"),
				'Link' => Yii::t ( 'app', 'Link' ) 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getArticle() {
		return $this->hasOne ( Article::className (), [ 
				'ID' => 'ArticleID' 
		] );
	}
	public function behaviors() {
		return [ 
				[ 
						'class' => SluggableBehavior::className (),
						'slugAttribute' => 'Link',
						'attribute' => 'Link',
						'ensureUnique' => true 
				] 
		];
	}
}
