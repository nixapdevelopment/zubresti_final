<?php

namespace app\models\ArticleFile;

use Yii;
use app\models\Article\Article;
use app\models\ArticleFileLang\ArticleFileLang;

/**
 * This is the model class for table "ArticleFile".
 *
 * @property integer $ID
 * @property integer $ArticleID
 * @property string $File
 * @property string $Type
 * @property integer $Position
 *
 * @property Article $article
 */
class ArticleFile extends \yii\db\ActiveRecord {
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'ArticleFile';
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'ArticleID',
								'File',
								'Type',
								'Position' 
						],
						'required' 
				],
				[ 
						[ 
								'ArticleID',
								'Position' 
						],
						'integer' 
				],
				[ 
						[ 
								'File',
								'Type' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'Date' 
						],
						'default',
						'value' => date ( 'c' ) 
				],
				[ 
						[ 
								'ArticleID' 
						],
						'exist',
						'skipOnError' => true,
						'targetClass' => Article::className (),
						'targetAttribute' => [ 
								'ArticleID' => 'ID' 
						] 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'ArticleID' => 'Article ID',
				'File' => 'File',
				'Type' => 'Type',
				'Position' => 'Position' 
		];
	}
	
	/**
	 *
	 * @return \yii\db\ActiveQuery
	 */
	public function getArticle() {
		return $this->hasOne ( Article::className (), [ 
				'ID' => 'ArticleID' 
		] );
	}
	public function getFullLink() {
		return Yii::getAlias ( '@web/uploads/article/' . $this->File );
	}
	public function getLang() {
		return $this->hasOne ( ArticleFileLang::className (), [ 
				'ArticleFileID' => 'ID' 
		] )->where ( [ 
				'LangID' => Yii::$app->language 
		] );
	}
	public function getLangs() {
		return $this->hasMany ( ArticleFileLang::className (), [ 
				'ArticleFileID' => 'ID' 
		] )->indexBy ( 'LangID' );
	}
}
