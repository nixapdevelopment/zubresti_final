<?php

namespace app\models\User;

use Yii;
use yii\base\Model;
use app\models\User\User;

class PinForm extends Model {
	public $Pin;
	public function rules() {
		return [ 
				[ 
						[ 
								'Pin' 
						],
						'required' 
				],
				[ 
						'Pin',
						'validatePin' 
				] 
		];
	}
	public function validatePin($attribute, $params) {
		$user = $this->getUser ();
		
		if (! $user || ! $user->validatePin ( $this->Pin )) {
			$this->addError ( $attribute, 'Incorrect pin.' );
		} else {
			Yii::$app->session->set ( '_pin', true );
		}
	}
	public function getUser() {
		return Yii::$app->user->isGuest ? false : Yii::$app->user->getIdentity ();
	}
	public function attributeLabels() {
		return [ 
				'Pin' => Yii::t("app", "Pin") 
		];
	}
}
