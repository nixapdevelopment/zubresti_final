<?php

namespace app\models\User;

use Yii;
use yii\base\Model;
use app\models\User\User;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *          
 */
class LoginForm extends Model {
	public $Email;
	public $Password;
	public $RememberMe = true;
	private $_user = false;
	public function rules() {
		return [ 
				[ 
						[ 
								'Email',
								'Password' 
						],
						'required' 
				],
				[ 
						'RememberMe',
						'boolean' 
				],
				[ 
						'Password',
						'validatePassword' 
				] 
		];
	}
	public function validatePassword($attribute, $params) {
		if (! $this->hasErrors ()) {
			$user = $this->getUser ();
			
			if (! $user || ! $user->validatePassword ( $this->Password )) {
				$this->addError ( $attribute, 'Incorrect email or password.' );
			}
		}
	}
	public function login() {
		if ($this->validate ()) {
			return Yii::$app->user->login ( $this->getUser (), $this->RememberMe ? 3600 * 24 * 30 : 0 );
		}
		return false;
	}
	public function getUser() {
		if ($this->_user === false) {
			$this->_user = User::findByUsername ( $this->Email );
		}
		
		return $this->_user;
	}
	public function attributeLabels() {
		return [ 
				'Email' => Yii::t("app", 'Email'),
				'Password' => Yii::t("app", 'Parolă'),
				'RememberMe' => Yii::t("app", 'Ține mă minte') 
		];
	}
}
