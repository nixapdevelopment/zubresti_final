<?php

namespace app\models\User;

use Yii;
use app\models\UserPermission\UserPermission;

/**
 * This is the model class for table "User".
 *
 * @property integer $ID
 * @property string $Email
 * @property string $Password
 * @property string $AuthKey
 * @property string $Name
 * @property string $Status
 */
class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface {
	const StatusActive = 'Active';
	const StatusInactive = 'Inactive';
	public static function getStatusList() {
		return [ 
				'' => '-',
				User::StatusActive => Yii::t("app", 'Activ'),
				User::StatusInactive => Yii::t("app", 'Inactiv') 
		];
	}
	public static function getStatusLabel($status) {
		$statusList = self::getStatusList ();
		return $statusList [$status];
	}
	
	/**
	 * @inheritdoc
	 */
	public static function tableName() {
		return 'User';
	}
	public function scenarios() {
		$scenarios = parent::scenarios ();
		
		$scenarios ['add'] = [ 
				'Email',
				'Password',
				'Name',
				'Status' 
		];
		$scenarios ['edit'] = [ 
				'Email',
				'Name',
				'Status' 
		];
		
		return $scenarios;
	}
	
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'Email',
								'Password',
								'Name',
								'Status' 
						],
						'required' 
				],
				[ 
						[ 
								'Email',
								'Password',
								'Name' 
						],
						'string',
						'max' => 255 
				],
				[ 
						[ 
								'AuthKey' 
						],
						'string',
						'max' => 50 
				],
				[ 
						[ 
								'Status' 
						],
						'string',
						'max' => 20 
				],
				[ 
						[ 
								'Email' 
						],
						'unique' 
				],
				[ 
						[ 
								'Email' 
						],
						'email' 
				],
				[ 
						[ 
								'Pin' 
						],
						'default',
						'value' => null 
				],
				[ 
						[ 
								'AuthKey' 
						],
						'default',
						'value' => null 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function attributeLabels() {
		return [ 
				'ID' => 'ID',
				'Email' => Yii::t("app", 'Email'),
				'Password' => Yii::t("app", "Parolă"),
				'AuthKey' => Yii::t("app", 'Auth-cheie'),
				'Name' => Yii::t("app", 'Nume'),
				'Status' => Yii::t("app", 'Status') 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id) {
		return User::find ()->where ( [ 
				'ID' => $id 
		] )->with ( [ 
				'permissions' 
		] )->one ();
	}
	
	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null) {
		return User::findOne ( [ 
				'AuthKey' => $token 
		] );
	}
	
	/**
	 * Finds user by username
	 *
	 * @param string $username        	
	 * @return static|null
	 */
	public static function findByUsername($username) {
		return User::findOne ( [ 
				'Email' => $username 
		] );
	}
	
	/**
	 * @inheritdoc
	 */
	public function getId() {
		return $this->ID;
	}
	
	/**
	 * @inheritdoc
	 */
	public function getAuthKey() {
		return $this->AuthKey;
	}
	
	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey) {
		return $this->AuthKey === $authKey;
	}
	
	/**
	 * Validates password
	 *
	 * @param string $password
	 *        	password to validate
	 * @return bool if password provided is valid for current user
	 */
	public function validatePassword($password) {
		return $this->Password === md5 ( $password );
	}
	public function validatePin($pin) {
		return $this->Pin === $pin;
	}
	public function getPermissions() {
		return $this->hasMany ( UserPermission::className (), [ 
				'UserID' => 'ID' 
		] );
	}
	public function getPermissionsList() {
		return \yii\helpers\ArrayHelper::map ( $this->permissions, 'Permission', 'Permission' );
	}
}
