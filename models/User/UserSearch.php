<?php

namespace app\models\User;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\User\User;

/**
 * UserSearch represents the model behind the search form about `app\models\User\User`.
 */
class UserSearch extends User {
	/**
	 * @inheritdoc
	 */
	public function rules() {
		return [ 
				[ 
						[ 
								'ID' 
						],
						'integer' 
				],
				[ 
						[ 
								'Email',
								'Password',
								'AuthKey',
								'Name',
								'Status' 
						],
						'safe' 
				] 
		];
	}
	
	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios ();
	}
	
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params        	
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = User::find ();
		
		// add conditions that should always apply here
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'pagination' => [ 
						'pageSize' => 10 
				] 
		] );
		
		$this->load ( $params );
		
		if (! $this->validate ()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		
		// grid filtering conditions
		$query->andFilterWhere ( [ 
				'ID' => $this->ID 
		] );
		
		$query->andFilterWhere ( [ 
				'like',
				'Email',
				$this->Email 
		] )->andFilterWhere ( [ 
				'like',
				'Name',
				$this->Name 
		] )->andFilterWhere ( [ 
				'Status' => $this->Status 
		] );
		
		return $dataProvider;
	}
	public static function getList($addEmpty = false) {
		$users = \yii\helpers\ArrayHelper::map ( User::find ()->orderBy ( 'Name' )->all (), 'ID', 'Name' );
		
		return $addEmpty ? [ 
				'' => '-' 
		] + $users : $users;
	}
}
