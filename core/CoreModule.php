<?php

namespace app\core;

use Yii;
use yii\base\Module;

class CoreModule extends Module {
	public $isCustomModule = true;
	public function getName() {
		return isset ( $this->name ) ? $this->name : ucfirst ( $this->id );
	}
}