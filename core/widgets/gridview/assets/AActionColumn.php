<?php

namespace template\widgets\gridview\assets;

use engine\components\asset\eAssetBundle\EAssetBundle;

class AActionColumn extends EAssetBundle {
	public $sourcePath = '@app/modules/template/widgets/gridview/assets';
	public $js = [ 
			"js/TActionColumn.js" 
	];
}