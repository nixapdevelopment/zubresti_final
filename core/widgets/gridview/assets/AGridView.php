<?php

namespace template\widgets\gridview\assets;

use engine\components\asset\eAssetBundle\EAssetBundle;

class AGridView extends EAssetBundle {
	public $sourcePath = '@app/modules/template/widgets/gridview/assets';
	public $css = [ 
			"css/gridview.less" 
	];
	public $js = [ 
			'js/TGridView.js' 
	];
	public $depends = [ 
			'yii\web\JqueryAsset',
			'yii\web\YiiAsset' 
	];
}