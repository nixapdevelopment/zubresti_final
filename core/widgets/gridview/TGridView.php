<?php

namespace template\widgets\gridview;

use yii\bootstrap\Html;
use yii\data\ArrayDataProvider;
use kartik\grid\GridView;
use template\widgets\gridview\assets\AGridView;
use template\widgets\modal\TModal;

class TGridView extends GridView {
	/**
	 *
	 * @var array the setting for gridview
	 *      params:
	 *      $title - string grid title
	 *      $btns - array of header btns
	 *      $searchForm - string search form
	 */
	public $header = [ ];
	public $hover = true;
	/**
	 *
	 * @var bool to enable treeview
	 */
	public $isTree = false;
	public $changePosition = false;
	public $positionModalID = 'position-modal';
	
	/**
	 * neasted kartik $pjax
	 * 
	 * @var boolean whether the grid view will be rendered within a pjax container.
	 */
	public $pjax = true;
	public $pjaxID = "manage-gridview";
	public $pjaxSettings = [ 
			'options' => [ 
					'timeout' => false,
					'enablePushState' => false,
					'clientOptions' => [ 
							'method' => 'GET' 
					] 
			] 
	];
	
	/**
	 *
	 * @var boolean whether to allow resizing of columns
	 */
	public $resizableColumns = false;
	
	/**
	 * neasted kartik $panelHeadingTemplate
	 * 
	 * @var string the template for rendering the panel heading.
	 */
	public $panelHeadingTemplate = <<< HTML
	<div class="row">
		<div class="col-xs-6">
			{heading}
		</div>
		<div class="col-xs-6">
			<div class="pull-right">
				{headerBtns}
			</div>
		</div>
    </div>
HTML;
	
	/**
	 * neasted kartik $panelFooterTemplate
	 * 
	 * @var string the template for rendering the panel footer.
	 */
	public $panelFooterTemplate = <<< HTML
	<div class="row">
		<div class="col-xs-10 kv-panel-pager">
			{pager}
		</div>
		<div class="col-xs-2 tg-summary">
			{summary}
		</div>
    </div>
HTML;
	
	/**
	 * neasted kartik $panelBeforeTemplate
	 * 
	 * @var string the template for rendering the `{before} part in the layout templates.
	 */
	public $panelBeforeTemplate = <<< HTML
	<div class="row">
		<div class="col-xs-12">
			{before}
		</div>
    </div>
HTML;
	public function init() {
		// register assets
		$this->registerAsset ();
		
		// register position button
		$this->preparePositionData ();
		
		// prepare settings
		$this->prepareSettings ();
		
		if ($this->isTree) {
			$this->dataProvider = new \yii\data\ArrayDataProvider ( [ 
					'allModels' => $this->prepareTreeData (),
					'sort' => false,
					'pagination' => false 
			] );
			
			$this->rowOptions = function ($model, $index, $widget, $grid) {
				return [ 
						'class' => 'parent-tr tr-level-' . $model->level 
				];
			};
		}
		
		parent::init ();
	}
	private function preparePositionData() {
		if ($this->changePosition) {
			$content = TPosition::run ( $this->dataProvider->getModels () );
			$this->header ['beforeBtns'] = TModal::widget ( [ 
					'modalSettings' => [ 
							'id' => $this->positionModalID,
							'header' => 'Update position',
							'headerOptions' => [ 
									'class' => 'bold' 
							],
							'toggleButton' => [ 
									'label' => 'Position',
									'class' => 'btn btn-success' 
							],
							'closeButton' => [ 
							],
							'clientOptions' => false 
					],
					'content' => $content 
			] );
		}
	}
	private function prepareSettings() {
		$this->pjaxSettings ['options'] ['id'] = $this->pjaxID;
		
		if (count ( $this->header ) > 0) {
			// prepare params
			$title = isset ( $this->header ['title'] ) ? $this->header ['title'] : '';
			$searchForm = isset ( $this->header ['searchForm'] ) ? $this->header ['searchForm'] : false;
			$btns = [ ];
			if (isset ( $this->header ['beforeBtns'] )) {
				$btns [] = $this->header ['beforeBtns'];
			}
			
			if (count ( $this->header ['btns'] ) > 0) {
				foreach ( $this->header ['btns'] as $btn ) {
					$options = $btn ['options'];
					$options ['data-pjax'] = 0;
					
					$btns [] = Html::a ( $btn ['label'], $btn ['url'], $options );
				}
			}
			
			// set panel params
			$this->panel = [ 
					'type' => 'info',
					'heading' => Html::tag ( "h3", $title, [ 
							'class' => 'panel-title tg-title' 
					] ),
					'before' => $searchForm,
					'after' => false 
			];
			
			// set replace tags
			$this->replaceTags ['{headerBtns}'] = Html::tag ( "div", join ( '', $btns ), [ 
					'class' => 'btn-group-sm' 
			] );
		}
	}
	
	// register asset
	private function registerAsset() {
		$view = $this->getView ();
		
		$str = sprintf ( "TGridView.init('#%s','#%s',%s);", $this->pjaxID, $this->positionModalID, ($this->changePosition ? 'true' : 'false') );
		
		$view->registerJs ( $str );
		
		AGridView::register ( $view );
	}
	private $rows = [ ];
	private function prepareTreeData() {
		$this->rows = [ ];
		
		$data = $this->dataProvider->getModels ();
		foreach ( $data as $model ) {
			$this->prepareTreeChild ( $model );
		}
		
		return $this->rows;
	}
	private function prepareTreeChild($model) {
		$this->rows [] = $model;
		if ($model->hasProperty ( 'childs' )) {
			$level = $model->level + 1;
			if (count ( $model->childs ) > 0) {
				foreach ( $model->childs as $child ) {
					$child->level = $level;
					$this->prepareTreeChild ( $child );
				}
			}
		}
	}
}
