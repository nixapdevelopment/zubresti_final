<?php

namespace template\widgets\gridview\grid;

use yii\grid\DataColumn;
use yii\helpers\Html;

class ESortableColumn extends DataColumn {
	public $action;
	public $child_action;
	public function init() {
		// $this->assetESortableColumn();
		parent::init ();
	}
	public function assetESortableColumn() {
		$view = $this->grid->getView ();
		ESortableColumnAsset::register ( $view );
		
		$this->child_action = ! empty ( $this->child_action ) ? $this->child_action : $this->action;
		
		$js [] = " ESortableColumn.action = '" . $this->action . "';";
		$js [] = " ESortableColumn.child_action = '" . $this->child_action . "';";
		$js [] = " ESortableColumn.init();";
		
		$view->registerJs ( implode ( "\n", $js ) );
	}
	public function renderDataCell($model, $key, $index) {
		$out = Html::tag ( 'i', '', [ 
				'class' => 'glyphicon glyphicon-option-vertical e-sortable-handle' 
		] );
		return Html::tag ( 'td', $out, [ 
				'width' => 10,
				'class' => 'e-sortable-column' 
		] );
	}
}