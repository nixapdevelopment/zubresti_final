<?php

namespace template\widgets\gridview\grid;

use yii\helpers\Url;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use kartik\grid\GridView;
use kartik\grid\ActionColumn;
use template\helpers\Glyphicon;
use app\modules\template\dependencies\User;
use template\widgets\gridview\assets\AActionColumn;

class TActionColumn extends ActionColumn {
	public $header = '';
	public $disableHeader = false;
	public $headerLink = '';
	public $template = '{add} {save} {delete}';
	public $delBtnSelector = 'ajaxDelete';
	public $vAlign = GridView::ALIGN_TOP;
	public function init() {
		// filter action template by allowed permission
		$this->template = User::getActionTemplateGridView ( $this->template );
		
		$this->registerAsset ();
		
		if (! $this->disableHeader) {
			$link = empty ( $this->headerLink ) ? Url::toRoute ( [ 
					'save' 
			] ) : $this->headerLink;
			
			$this->header = Html::a ( Glyphicon::Add . ' Add', $link, [ 
					'class' => 'btn-sm btn-success',
					'data-pjax' => '0' 
			] );
		}
		
		$this->hAlign = $this->grid->isTree ? GridView::ALIGN_RIGHT : GridView::ALIGN_CENTER;
		
		parent::init ();
	}
	protected function initDefaultButtons() {
		if (! isset ( $this->buttons ['delete'] )) {
			$this->buttons ['delete'] = function ($url, $model) {
				$btn = $model->Btns ['delete'];
				if ($btn ['enabled']) {
					return Html::a ( Glyphicon::Trash, false, [ 
							'class' => $this->delBtnSelector,
							'delete-url' => $btn ['url'],
							'pjax-container' => $this->grid->pjaxID,
							'title' => 'Delete' 
					] );
				}
			};
		}
		
		if (! isset ( $this->buttons ['save'] )) {
			$this->buttons ['save'] = function ($url, $model) {
				$btn = $model->Btns ['save'];
				if ($btn ['enabled']) {
					$options = $this->updateOptions;
					$options = ArrayHelper::merge ( [ 
							'title' => $title,
							'data-pjax' => '0' 
					], $options );
					
					return Html::a ( Glyphicon::Pencil, $btn ['url'], $options );
				}
			};
		}
		
		if (! isset ( $this->buttons ['add'] )) {
			$this->buttons ['add'] = function ($url, $model) {
				$btn = $model->Btns ['add'];
				if ($btn ['enabled']) {
					$options = $this->updateOptions;
					$options = ArrayHelper::merge ( [ 
							'title' => $title,
							'data-pjax' => '0' 
					], $options );
					
					return Html::a ( Glyphicon::Add, $btn ['url'], $options );
				}
			};
		}
		
		if (! isset ( $this->buttons ['modalsave'] )) {
			$this->buttons ['modalsave'] = function ($url, $model) {
				$modalID = isset ( $model ['ModalID'] ) ? $model ['ModalID'] : $model->ModalID;
				$icon = '<span class="glyphicon glyphicon-pencil modalsave-btn" modal-id="' . $modalID . '"></span>';
				
				return Html::a ( $icon, '#' );
			};
		}
	}
	
	// register asset
	private function registerAsset() {
		$view = $this->grid->getView ();
		;
		
		AActionColumn::register ( $view );
		$js [] = " TActionColumn.delete_button.selector = '." . $this->delBtnSelector . "';";
		$js [] = " TActionColumn.init();";
		
		$view->registerJs ( implode ( "\n", $js ) );
	}
}