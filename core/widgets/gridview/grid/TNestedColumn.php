<?php

namespace template\widgets\gridview\grid;

use yii\bootstrap\Html;
use yii\grid\DataColumn;

class TNestedColumn extends DataColumn {
	public $format = 'raw';
	public $contentOptions = [ 
			'class' => 'neast-td' 
	];
	public function init() {
		parent::init ();
	}
}