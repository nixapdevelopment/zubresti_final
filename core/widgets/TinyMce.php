<?php

namespace app\core\widgets;

use dosamigos\tinymce\TinyMce as DTinyMce;

class TinyMce extends DTinyMce {
	public $language = 'ru';
	public $clientOptions = [ 
			'relative_urls' => false,
			'plugins' => [ 
					"advlist autolink lists link charmap print preview anchor",
					"searchreplace visualblocks code fullscreen",
					"insertdatetime media table contextmenu paste image responsivefilemanager filemanager" 
			],
			'image_advtab' => true,
			'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media",
			'height' => 400,
			'external_filemanager_path' => '/plugins/responsivefilemanager/filemanager/',
			'filemanager_title' => 'Responsive Filemanager',
			'external_plugins' => [ 
					'filemanager' => '/plugins/responsivefilemanager/filemanager/plugin.min.js',
					'responsivefilemanager' => '/plugins/responsivefilemanager/tinymce/plugins/responsivefilemanager/plugin.min.js' 
			] 
	];
}
