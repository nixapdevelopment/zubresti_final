<?php

namespace app\components;

use yii\web\UploadedFile as YiiUploadedFile;

class UploadedFile extends YiiUploadedFile {
	private static $_files;
	public static function getInstancesByName($name) {
		$files = self::loadFiles ();
		if (isset ( $files [$name] )) {
			return [ 
					new static ( $files [$name] ) 
			];
		}
		$results = [ ];
		foreach ( $files as $key => $file ) {
			if (strpos ( $key, "{$name}[" ) === 0) {
				preg_match ( '/Settings\[(.*)\]/', $key, $matches );
				if (! empty ( $matches [1] )) {
					$results [$matches [1]] = new static ( $file );
				}
			}
		}
		return $results;
	}
	private static function loadFiles() {
		if (self::$_files === null) {
			self::$_files = [ ];
			if (isset ( $_FILES ) && is_array ( $_FILES )) {
				foreach ( $_FILES as $class => $info ) {
					self::loadFilesRecursive ( $class, $info ['name'], $info ['tmp_name'], $info ['type'], $info ['size'], $info ['error'] );
				}
			}
		}
		return self::$_files;
	}
	private static function loadFilesRecursive($key, $names, $tempNames, $types, $sizes, $errors) {
		if (is_array ( $names )) {
			foreach ( $names as $i => $name ) {
				self::loadFilesRecursive ( $key . '[' . $i . ']', $name, $tempNames [$i], $types [$i], $sizes [$i], $errors [$i] );
			}
		} elseif (( int ) $errors !== UPLOAD_ERR_NO_FILE) {
			self::$_files [$key] = [ 
					'name' => $names,
					'tempName' => $tempNames,
					'type' => $types,
					'size' => $sizes,
					'error' => $errors 
			];
		}
	}
}