<?php

namespace app\assets;

use yii\web\AssetBundle;

class FrontAssets extends AssetBundle {
	public $sourcePath = '@app/views/themes/default';
	public $baseUrl = '@web/themes/default';
	public $css = [ 
	];
	public $js = [ 
	];
	public $depends = [ 
			'yii\web\YiiAsset',
			'yii\bootstrap\BootstrapAsset',
			'rmrevin\yii\fontawesome\AssetBundle' 
	];
}