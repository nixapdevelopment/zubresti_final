<?php

namespace app\assets;

use yii\web\AssetBundle;

class ZubrestiFrontAssets extends AssetBundle {
	public $sourcePath = '@app/views/themes/zubresti/assets';
	public $css = [ 
			'https://fonts.googleapis.com/css?family=Satisfy',
			'css/uikit.min.css',
			'css/uikit.almost-flat.min.css',
			'css/animate.css',
			'css/font-awesome.min.css',
			'js/owlcarousel/assets/owl.carousel.min.css',
			'js/owlcarousel/assets/owl.theme.default.min.css',
			'css/magnific-popup.css',
			'css/slick.css',
			'css/slick-theme.css',
			'css/jquery.bxslider.css',
			'css/main.css' 
	];
	public $js = [ 
			// 'js/jquery.js',
			'js/uikit.min.js',
			'js/jquery.magnific-popup.min.js',
			'js/wow.min.js',
			'js/owlcarousel/owl.carousel.min.js',
			'js/jquery.bxslider.min.js',
			'js/slick.min.js',
			'js/scripts.js' 
	];
	public $depends = [ 
			'yii\web\YiiAsset',
			'yii\web\JqueryAsset' 
	];
}