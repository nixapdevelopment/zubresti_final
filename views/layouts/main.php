<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use kartik\growl\Growl;
use app\modules\Admin\components\AdminMenuWidget\AdminMenuWidget;

AppAsset::register ( $this );
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    
    <?= AdminMenuWidget::widget() ?>

    <div class="container">
        
        <?=Breadcrumbs::widget ( [ 'homeLink' => [ 'label' => 'Dashboard','url' => [ '/admin' ] ],'links' => isset ( $this->params ['breadcrumbs'] ) ? $this->params ['breadcrumbs'] : [ ] ] )?>
        
        <?php
								if (Yii::$app->session->getFlash ( 'success' )) {
									echo Growl::widget ( [ 
											'type' => Growl::TYPE_SUCCESS,
											'icon' => 'glyphicon glyphicon-ok-sign',
											'title' => 'Success',
											'showSeparator' => true,
											'body' => Yii::$app->session->getFlash ( 'success' ) 
									] );
								}
								?>
        
        <?= $content ?>
        
    </div>
	</div>

	<footer class="footer">
		<div class="container">
			<p class="pull-left">&copy; NIXAP <?= date('Y') ?></p>

			<p class="pull-right"><?= Yii::powered() ?></p>
		</div>
	</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
