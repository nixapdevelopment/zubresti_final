<?php
/* @var $this \yii\web\View */
/* @var $content string */

use yii\bootstrap\Html;
use app\assets\FrontAssets;
use app\modules\Menu\components\MenuWidget\MenuWidget;
use pceuropa\languageSelection\LanguageSelection;

FrontAssets::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?= Html::csrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <script>var SITE_URL = '<?= yii\helpers\Url::home(true) ?>';</script>
        <?php $this->head() ?>
    </head>
    <body>
        <?php $this->beginBody() ?>

        <header class="header">
            <div class="container">
                <div class="pull-left">
                    <?= MenuWidget::widget(['menuId' => 3]) ?>
                </div>
                <div class="pull-right">
                    <?=
                    LanguageSelection::widget([
                        'language' => Yii::$app->params ['siteLanguages'],
                        'languageParam' => 'language',
                        'container' => 'div',
                        'classContainer' => 'dropdown-toggle'
                    ])
                    ?>
                </div>
                <div class="clearfix"></div>
            </div>
        </header>

        <?= $content ?>

        <footer style="height: 100px; background-color: #ddd;">
            <div class="container">FOOTER</div>
        </footer>

        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>
