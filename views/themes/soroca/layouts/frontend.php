<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\bootstrap\Html;
use app\assets\SorocaFrontAssets;
use app\modules\Menu\components\MenuWidget\MenuWidget;
use yii\helpers\Url;
use app\modules\Settings\Settings;
use modernkernel\flagiconcss\Flag;
use app\models\Article\Article;

$bundle = SorocaFrontAssets::register ( $this );

$contact_page = Article::find ()->with ( [ 
		'lang',
		'link' 
] )->where ( [ 
		'ID' => 34 
] )->one ();

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon"
	href="<?= Url::to('@web/uploads/settings/' . Settings::getByName('favicon')) ?>"
	type="image/ico">
<title><?= Html::encode($this->title) ?></title>
<script>var SITE_URL = '<?= yii\helpers\Url::home(true) ?>';</script>
    <?php $this->head() ?>
</head>
<body class="soroca-front">
<?php $this->beginBody() ?>
    
<header>
		<div class="container">
			<div class="top-bar">
				<div class="pull-left text-muted">
                <?= Settings::getByName('topBarText', true) ?>
            </div>
				<div class="pull-right">
					<ul>
						<li class="text-danger"><a href="<?= $contact_page->seoLink ?>">Feedback</a>
						</li>
						<li class="text-danger">
                        <?php foreach (Yii::$app->params['siteLanguages'] as $lang) { ?>
                        <a
							href="<?= Url::current(['language' => $lang]) ?>">
                            <?=Flag::widget ( [ 'tag' => 'span','country' => $lang,'squared' => false,'options' => [ 'style' => 'box-shadow: 0 0 4px black; margin: 0 5px;' ] ] )?>
                        </a>
                        <?php } ?>
                    </li>
						<li><a href="<?= Url::to(['/admin']) ?>"> <i class="fa fa-sign-in"
								aria-hidden="true"></i> Logare
						</a></li>
					</ul>
				</div>
				<div class="clearfix"></div>
			</div>
			<div class="header-bar">
				<div class="row">
					<div class="col-md-3">
						<a href="<?= Url::to(['/']) ?>/">
                        <?= Html::img('@web/uploads/settings/' . Settings::getByName('logo')) ?>
                    </a>
					</div>
					<div class="col-md-9">
                    <?=MenuWidget::widget ( [ 'menuId' => 3,'options' => [ 'class' => 'nav navbar-nav top-menu' ] ] )?>
                </div>
				</div>
			</div>
		</div>
	</header>

<?= $content ?>
    
<br />
	<br />
	<br />

	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<table>
						<tr>
							<td><a href="<?= Url::to(['/']) ?>/">
                                <?= Html::img('@web/uploads/settings/' . Settings::getByName('footerLogo')) ?>
                            </a></td>
							<td style="padding-left: 30px;">
                            <?= Settings::getByName('footerText', true) ?>
                        </td>
						</tr>
					</table>
				</div>
				<div class="col-md-5">
					<div class="social-wrap text-center">
                    <?php if (Settings::getByName('facebookLink')) { ?>
                    <a target="_blank"
							href="<?= Settings::getByName('facebookLink') ?>">
                        <?= Html::img($bundle->baseUrl . '/images/social/fb.png') ?>
                    </a>
                    <?php } ?>
                    <?php if (Settings::getByName('twitterLink')) { ?>
                    <a target="_blank"
							href="<?= Settings::getByName('twitterLink') ?>">
                        <?= Html::img($bundle->baseUrl . '/images/social/tw.png') ?>
                    </a>
                    <?php } ?>
                    <?php if (Settings::getByName('okLink')) { ?>
                    <a target="_blank"
							href="<?= Settings::getByName('okLink') ?>">
                        <?= Html::img($bundle->baseUrl . '/images/social/ok.png') ?>
                    </a>
                    <?php } ?>
                    <?php if (Settings::getByName('instagramLink')) { ?>
                    <a target="_blank"
							href="<?= Settings::getByName('instagramLink') ?>">
                        <?= Html::img($bundle->baseUrl . '/images/social/in.png') ?>
                    </a>
                    <?php } ?>
                    <?php if (Settings::getByName('gplusLink')) { ?>
                    <a target="_blank"
							href="<?= Settings::getByName('gplusLink') ?>">
                        <?= Html::img($bundle->baseUrl . '/images/social/g+.png') ?>
                    </a>
                    <?php } ?>
                    <?php if (Settings::getByName('vkLink')) { ?>
                    <a target="_blank"
							href="<?= Settings::getByName('vkLink') ?>">
                        <?= Html::img($bundle->baseUrl . '/images/social/vk.png') ?>
                    </a>
                    <?php } ?>
                </div>
				</div>
				<div class="col-md-2 text-center">
					Copyright &COPY; 2017<br> Creat de Nixap
				</div>
			</div>
		</div>
	</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
