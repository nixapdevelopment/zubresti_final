$(document).ready(function(){
    
    var recentSlider = $('#recent-slider').owlCarousel({
        singleItem: true,
        items: 1,
        navigation : false,
        dots: false,
        loop: true
    });

    $('#recent-slider-wrap .fa-angle-left').click(function() {
        recentSlider.trigger('prev.owl.carousel');
    });

    $('#recent-slider-wrap .fa-angle-right').click(function() {
        recentSlider.trigger('next.owl.carousel');
    });
    
    
    var mainSlider = $('#main-slider').owlCarousel({
        singleItem: true,
        items: 1,
        loop: true
    });
    
});