<a href="?id=<?= $model->ID ?>" data-pjax="0">
	<div class="col-md-2">
		<h3><?= $model->lang->Title ?></h3>
		<div>
            <?=yii\helpers\Html::img ( '@web/uploads/gallery/' . $model->Thumb, [ 'class' => 'img-responsive' ] )?>
        </div>
	</div>
</a>