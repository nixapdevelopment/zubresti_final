<a href="<?= $model->seoLink ?>" data-pjax="0">
	<div class="col-md-3">
		<h3><?= $model->lang->Title ?></h3>
		<div>
            <?=yii\helpers\Html::img ( $model->MainThumbUrl, [ 'class' => 'img-responsive' ] )?>
        </div>
		<div>
            <?= $model->shortText ?>
        </div>
	</div>
</a>