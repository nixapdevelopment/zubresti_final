<?php
use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;

?>

<div class="container">
	<h1><?= $article->lang->Title ?></h1>
	<h5><?= $article->Date ?></h5>
	<div>
        <?= $article->lang->Text ?>
    </div>
	<br />
	<div>
        <?=ArticleImageWidget::widget ( [ 'article' => $article ] )?>
        <?=ArticleFileWidget::widget ( [ 'article' => $article ] )?>
    </div>
</div>