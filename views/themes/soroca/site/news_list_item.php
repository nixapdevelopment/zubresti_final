<div>
	<h3><?= $model->lang->Title ?></h3>
	<div>
        <?= $model->getShortText(300) ?>...
    </div>
	<div>
		<a style="color: #e74c3c" href="<?= $model->seoLink ?>" data-pjax="0"><?= Yii::t('app', 'Details') ?> >></a>
	</div>
</div>
<br />