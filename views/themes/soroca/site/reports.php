<?php
use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;

?>

<div class="container">

	<h1><?= $article->lang->Title ?></h1>

	<div>
        <?= $article->lang->Text ?>
    </div>

	<div>
        <?=ArticleFileWidget::widget ( [ 'article' => $article,'useDateFilter' => true ] )?>
    </div>

</div>