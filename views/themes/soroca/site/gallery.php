<?php
use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Gallery\Gallery;
use app\models\GalleryItem\GalleryItem;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

?>

<?php if (Yii::$app->request->get('id', false)) { ?>

    <?php
	
	echo newerton\fancybox\FancyBox::widget ( [ 
			'target' => "a[rel=gallery-images]",
			'helpers' => true,
			'mouse' => true,
			'config' => [ 
					'maxWidth' => '90%',
					'maxHeight' => '90%',
					'playSpeed' => 7000,
					'padding' => 0,
					'fitToView' => false,
					'width' => '90%',
					'height' => '90%',
					'autoSize' => false,
					'closeClick' => false,
					'openEffect' => 'elastic',
					'closeEffect' => 'elastic',
					'prevEffect' => 'elastic',
					'nextEffect' => 'elastic',
					'closeBtn' => false,
					'openOpacity' => true,
					'helpers' => [ 
							'buttons' => [ ],
							'thumbs' => [ 
									'width' => 68,
									'height' => 60 
							],
							'overlay' => [ 
									'css' => [ 
											'background' => 'rgba(0, 0, 0, 0.8)' 
									] 
							] 
					] 
			] 
	] );
	
	?>
<br />
<div class="container">
	<div class="row">
            <?php foreach (GalleryItem::find()->where(['GalleryID' => Yii::$app->request->get('id')])->orderBy('Position')->all() as $image) { ?>
            <div class="col-md-2">
                <?=Html::a ( Html::img ( '@web/uploads/gallery/' . $image->Thumb, [ 'class' => 'img-responsive img-thumbnail','style' => 'margin-bottom:30px;' ] ), '@web/uploads/gallery/' . $image->Value, [ 'rel' => 'gallery-images','class' => 'fancybox' ] )?>
            </div>
            <?php } ?>
        </div>
</div>

<?= $this->registerCss("#fancybox-thumbs.bottom{bottom:20px;}") ?>

<?php } else { ?>

    <?php
	
	$query = Gallery::getDb ()->cache ( function ($db) {
		return Gallery::find ()->with ( [ 
				'lang',
				'items' 
		] )->orderBy ( 'Position' );
	}, 60 );
	
	$dataProvider = new ActiveDataProvider ( [ 
			'query' => $query,
			'pagination' => [ 
					'pageSize' => 10 
			] 
	] );
	
	?>

<div class="container">
	<h1><?= $article->lang->Title ?></h1>
	<div>
            <?= $article->lang->Text ?>
        </div>
	<div class="row">
            <?php Pjax::begin(); ?>
                <?=ListView::widget ( [ 'dataProvider' => $dataProvider,'itemView' => 'gallery_list_item','layout' => "{summary}\n{items}\n<div class=\"clearfix\"></div>{pager}" ] );?>
            <?php Pjax::end(); ?>
        </div>
</div>

<?php } ?>