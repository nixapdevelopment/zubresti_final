<?php
use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Article\Article;
use yii\data\ActiveDataProvider;

$query = Article::getDb ()->cache ( function ($db) {
	return Article::find ()->where ( [ 
			'Type' => 'News' 
	] )->with ( [ 
			'lang' 
	] )->orderBy ( 'Date DESC' );
}, 60 );

$dataProvider = new ActiveDataProvider ( [ 
		'query' => $query,
		'pagination' => [ 
				'pageSize' => 4 
		] 
] );

?>

<div class="container">
	<h1><?= $article->lang->Title ?></h1>
	<div>
        <?= $article->lang->Text ?>
    </div>
	<div>
        <?php Pjax::begin(); ?>
            <?=ListView::widget ( [ 'dataProvider' => $dataProvider,'itemView' => 'news_list_item','layout' => "{summary}\n{items}\n<div class=\"clearfix\"></div>{pager}" ] );?>
        <?php Pjax::end(); ?>
    </div>
</div>