<?php
use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Article\Article;
use yii\data\ActiveDataProvider;
use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;

$dataProvider = new ActiveDataProvider ( [ 
		'query' => Article::find ()->where ( [ 
				'Type' => 'Anouncment' 
		] )->orderBy ( 'Date DESC' ),
		'pagination' => [ 
				'pageSize' => 2 
		] 
] );

?>

<div class="container">
	<h1><?= $article->lang->Title ?></h1>
	<div>
        <?= $article->lang->Text ?>
    </div>
	<div class="row">
        <?php Pjax::begin(); ?>
            <?=ListView::widget ( [ 'dataProvider' => $dataProvider,'itemView' => 'anouncment_list_item','layout' => "{summary}\n{items}\n<div class=\"clearfix\"></div>{pager}" ] );?>
        <?php Pjax::end(); ?>
    </div>
    
    <?=ArticleImageWidget::widget ( [ 'article' => $article ] )?>
    
</div>