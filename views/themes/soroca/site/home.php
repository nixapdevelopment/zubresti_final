<?php
use yii\helpers\Html;
use app\assets\SorocaFrontAssets;
use app\models\SliderItem\SliderItem;
use app\models\Article\Article;
use yii\caching\TagDependency;
use app\models\ArticleFile\ArticleFile;

$bundle = SorocaFrontAssets::register ( $this );

$lastArticles = Article::getDb ()->cache ( function ($db) {
	return Article::find ()->with ( [ 
			'lang',
			'link' 
	] )->where ( [ 
			'Type' => [ 
					'News',
					'Events' 
			] 
	] )->orderBy ( 'Date DESC' )->limit ( 5 )->all ();
}, 30 );

$slides = SliderItem::getDb ()->cache ( function ($db) {
	return SliderItem::find ()->with ( 'lang' )->where ( [ 
			'SliderID' => 1 
	] )->all ();
}, 30, new TagDependency ( [ 
		'tags' => 'slider' 
] ) );

$imageNews = Article::getDb ()->cache ( function ($db) {
	return Article::find ()->with ( [ 
			'lang',
			'link' 
	] )->where ( [ 
			'Type' => 'News' 
	] )->orderBy ( 'Date DESC' )->limit ( 4 )->all ();
}, 30, new TagDependency ( [ 
		'tags' => 'news' 
] ) );

$noImageNews = Article::getDb ()->cache ( function ($db) {
	return Article::find ()->with ( [ 
			'lang',
			'link' 
	] )->where ( [ 
			'Type' => 'News' 
	] )->orderBy ( 'Date DESC' )->limit ( 4 )->all ();
}, 30, new TagDependency ( [ 
		'tags' => 'news' 
] ) );

$anouncment = Article::getDb ()->cache ( function ($db) {
	return Article::find ()->with ( [ 
			'lang',
			'link' 
	] )->where ( [ 
			'Type' => 'Anouncment' 
	] )->orderBy ( 'Date DESC' )->limit ( 4 )->all ();
}, 30, new TagDependency ( [ 
		'tags' => 'anouncment' 
] ) );

$decisionsFiles = ArticleFile::getDb ()->cache ( function ($db) {
	return ArticleFile::find ()->with ( 'lang' )->where ( [ 
			'ArticleID' => 38 
	] )->orderBy ( 'Date DESC' )->limit ( 10 )->all ();
}, 30, new TagDependency ( [ 
		'tags' => 'files' 
] ) );

$medias = Article::getDb ()->cache ( function ($db) {
	return Article::find ()->with ( [ 
			'lang',
			'link' 
	] )->where ( [ 
			'Type' => 'Media' 
	] )->orderBy ( 'Date DESC' )->limit ( 6 )->all ();
}, 30, new TagDependency ( [ 
		'tags' => 'decision' 
] ) );

$news_page = Article::findOne ( 9 );
$anounce_page = Article::findOne ( 20 );
$event_page = Article::findOne ( 24 );

?>

<?php if (count($lastArticles) > 0) { ?>
<section class="recent-posts">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div style="font-size: 14px; line-height: 45px;"
					class="col-md-2 col-sm-3 bg-red text-white text-center">CELE MAI
					RECENTE</div>
				<div class="col-md-10 col-sm-9 bg-white">
					<div id="recent-slider-wrap">
						<div id="recent-slider" class="owl-carousel owl-theme">
                            <?php foreach ($lastArticles as $la) { ?>
                            <div class="item">
								<a style="color: inherit;" href="<?= $la->seoLink ?>"><?= $la->lang->Title ?></a>
							</div>
                            <?php } ?>
                        </div>
						<div class="slider-controls">
							<i class="fa fa-angle-left" aria-hidden="true"></i> <i
								class="fa fa-angle-right" aria-hidden="true"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>

<section>
	<div class="container">
		<div class="row">
			<div class="col-md-8">
                <?php if (count($slides) > 0) { ?>
                <div id="main-slider-wrap">
					<div id="main-slider" class="owl-carousel owl-theme">
                        <?php foreach ($slides as $slide) { ?>
                        <div class="item">
							<div class="main-slider-item">
                                <?= Html::img($slide->imageUrl) ?>
                                <div class="main-slider-item-text">
									<h3>
                                        <?= $slide->lang->Title ?>
                                    </h3>
								</div>
							</div>
						</div>
                        <?php } ?>
                    </div>
				</div>
				<br />
                <?php } ?>
                <div class="last-events-wrap">
                    <?php foreach ($imageNews as $in) { ?>
                    <a style="color: inherit;"
						href="<?= $in->seoLink ?>">
						<div data-news-id="<?= $in->ID ?>"
							class="last-events-item <?= reset($imageNews) == $in ? '' : 'item-inline' ?>">
							<div class="last-events-item-header">
								<h4><?= $in->lang->Title ?></h4>
								<time>
									<i class="fa fa-calendar"></i> &nbsp;<?= $in->dMYDate ?></time>
							</div>
							<div class="last-events-item-image" style="background-image:  url(<?= $in->mainImageUrl ?>);">

							</div>
							<div class="clearfix"></div>
						</div>
					</a>
                    <?php } ?>
                </div>
			</div>
			<div class="col-md-4">
				<div class="bg-white">
					<div class="tabs-widget">
						<ul class="tabs" role="tablist">
							<li role="presentation" class="active"><a href="#tab-info"
								aria-controls="tab-info" role="tab" data-toggle="tab">Informatii<br>publice
							</a></li>
							<li role="presentation"><a href="#tab-decizii"
								aria-controls="tab-decizii" role="tab" data-toggle="tab">Decizii</a></li>
							<li role="presentation"><a href="#tab-media"
								aria-controls="tab-media" role="tab" data-toggle="tab">Media</a></li>
						</ul>
						<div class="tab-content">
							<div role="tabpanel" class="tab-pane active" id="tab-info">
                                <?=""; // app\modules\Menu\components\MenuWidget\MenuWidget::widget([// 'menuId' => 4// ]) ?>
                            </div>
							<div role="tabpanel" class="tab-pane" id="tab-decizii">
								<ol>
                                    <?php foreach ($decisionsFiles as $df) { ?>
                                    <a style="color: inherit;"
										href="<?= $df->fullLink ?>">
										<li>
                                            <?= $df->lang->Title ?>
                                        </li>
									</a>
                                    <?php } ?>
                                </ol>
							</div>
							<div role="tabpanel" class="tab-pane" id="tab-media">
								<ol>
                                    <?php foreach ($medias as $pi) { ?>
                                    <li><a style="color: inherit;"
										href="<?= $pi->seoLink ?>">
                                            <?= $pi->lang->Title ?>
                                        </a></li>
                                    <?php } ?>
                                </ol>
							</div>
						</div>
					</div>
				</div>
				<br />
				<div class="bg-white">
					<div class="last-news-wrap">
                        <?php foreach ($noImageNews as $nin) { ?>
                        <a style="color: inherit;"
							href="<?= $nin->seoLink ?>">
							<div class="last-news-item">
								<h5><?= $nin->lang->Title ?></h5>
								<time>
									<i class="fa fa-calendar"></i> <?= $nin->dMYDate ?>
                                </time>
							</div>
						</a>
                        <?php } ?>
                        <a href="<?= $news_page->seoLink ?>"
							class="btn btn-block btn-red">Vezi toate stirile</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<br />
<section>
	<div class="container">
		<div class="row">
            <?php foreach ($anouncment as $an) { ?>
            <div class="col-md-6">
				<div class="anounce-item">
					<h4><?= $an->lang->Title ?></h4>
					<div>
                        <?= $an->shortText ?>
                    </div>
					<div style="line-height: 3.5">
						<div class="pull-left">
							<time>
								<i class="fa fa-calendar"></i> <?= $an->dMYDate ?>
                            </time>
						</div>
						<div class="pull-right">
							<a href="<?= $an->seoLink ?>" class="btn btn-block btn-default">Vezi
								detalii</a>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
			</div>
            <?php } ?>
        </div>
		<div class="col-md-12">
			<a href="<?= $anounce_page->seoLink ?>"
				class="btn btn-red col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-12">Vezi
				toate anunturile</a>
		</div>
	</div>
</section>
<br />
<br />
<section>
	<div class="container">
		<h3><?= $article->lang->Title ?></h3>
		<div>
            <?= $article->lang->Text ?>
        </div>
	</div>
</section>
<br />
<br />