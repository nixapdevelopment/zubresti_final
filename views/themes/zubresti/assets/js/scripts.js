

/*  INIT MAP    */


function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 47.0321659, lng: 28.804726},
        zoom: 17,
        zoomControl: false,
        scrollwheel: false,
        disableDoubleClickZoom: true,
        disableDefaultUI: true,
        styles: [
            {
                "featureType": "administrative",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#686868"
                    },
                    {
                        "weight": "0.01"
                    }
                ]
            },
            {
                "featureType": "administrative",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "color": "#686868"
                    }
                ]
            },
            {
                "featureType": "landscape",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#e7e7e7"
                    }
                ]
            },
            {
                "featureType": "poi",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "weight": "0.01"
                    },
                    {
                        "color": "#686868"
                    }
                ]
            },
            {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dedede"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#dedede"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "geometry.fill",
                "stylers": [
                    {
                        "color": "#ffffff"
                    }
                ]
            },
            {
                "featureType": "road",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "gamma": "0.00"
                    },
                    {
                        "color": "#686868"
                    },
                    {
                        "weight": "0.01"
                    }
                ]
            },
            {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dedede"
                    }
                ]
            },
            {
                "featureType": "road.arterial",
                "elementType": "geometry",
                "stylers": [
                    {
                        "color": "#dedede"
                    }
                ]
            },
            {
                "featureType": "transit",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#dedede"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "all",
                "stylers": [
                    {
                        "color": "#cfcfcf"
                    }
                ]
            },
            {
                "featureType": "water",
                "elementType": "labels.text",
                "stylers": [
                    {
                        "color": "#e7e7e7"
                    }
                ]
            }
        ]
    });
    var image = 'images/placeholder-map.png';
    var beachMarker = new google.maps.Marker({
        position: {lat: 47.0321659, lng: 28.804726},
        map: map,
        icon: image
    });
}


$(document).ready(function(){

    /* OWL SLIDER ACASA PAGE */

//    $('.owl-carousel').owlCarousel({
//        items:1,
//        nav:true,
//        loop: true,
//        dots: true,
//        autoplay: 7000,
//        //navText: ["<img src='/images/chevron-left.png'/>","<img src='/images/chevron-right.png'/>"]
//    });

    /* BXSLIDER ACASA PAGE */

    $('.bxslider').bxSlider({
        adaptiveHeight: true,
        mode: 'fade',
        auto: true,
        speed: 5000
    });

    /* MAGNIFIC POPUP WITH SLICK SLIDER INIT AFTER OPEN */

    $(".zoom").magnificPopup({
        type:"inline",
        midClick: true,
        mainClass: 'default',
        callbacks: {
            open: function () {
                
                var mp = $.magnificPopup.instance,
                t = $(mp.currItem.el[0]);
                var popupID = t.attr("href");
                
                $(popupID).find('.foto_slider').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    fade: true,
                    asNavFor: popupID+' .foto_nav'
                });

                $(popupID).find('.foto_nav').slick({
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    asNavFor: popupID+' .foto_slider',
                    dots: true,
                    centerMode: true,
                    focusOnSelect: true
                });
            },
            close: function () {

            }
        }
    });
    $(document).on('click', '.popup-modal-dismiss', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });


    /* MAGNIFIC POPUP WITH SLICK SLIDER INIT AFTER OPEN */

    $(".zoom2").magnificPopup({
        type:"inline",
        midClick: true,
        mainClass: 'default',
        callbacks: {
            open: function () {
                var mp = $.magnificPopup.instance,
                t = $(mp.currItem.el[0]);
                var popupID = t.attr("href");
                
                $(popupID).find('.video_slider').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    fade: true,
                    asNavFor: '.video_nav'
                });

                $(popupID).find('.video_nav').slick({
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    asNavFor: '.video_slider',
                    dots: true,
                    centerMode: true,
                    focusOnSelect: true
                });
            },
            close: function () {

            }
        }
    });
    $(document).on('click', '.popup-modal-dismiss2', function (e) {
        e.preventDefault();
        $.magnificPopup.close();
    });


    /* TOGGLE UL AFTER CLICK ON TAG A */

    $('ul:not(.drop-down) > li > a').on("mouseenter", function(e){
        //e.preventDefault();
        $("ul.drop-down").not($(this).closest("li").find("ul")).slideUp();
        $(this).closest("li").find("ul").slideToggle();
    });
    
    
    // add arrow to menu that has submenu childrens
    $('.nav ul li').has('ul').children('a').append('<i class="uk-icon-chevron-down"></i>');
    $('.mobile-nav ul li').has('ul').children('a').append('<i class="uk-icon-chevron-down"></i>');

    /* TOGGLE UL IN UK OFFCANVAS */



});


