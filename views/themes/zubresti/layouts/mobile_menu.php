<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\ZubrestiFrontAssets;
use app\modules\Menu\components\MenuWidget\MenuWidget;
use app\modules\Settings\Settings;

$bundle = ZubrestiFrontAssets::register ( $this );

?>


<div id="mobile_menu" class="uk-offcanvas">
	<div class="uk-offcanvas-bar mobile-nav">
		<div class="logo mt20">
			<a href="<?= Url::to(['/']) ?>">
                <?= Html::img('@web/uploads/settings/' . Settings::getByName('logo')) ?> 
            </a>
		</div>
        
        <?=MenuWidget::widget ( [ 'menuId' => 3,'submenuTemplate' => "<ul class='drop-down'>\n{items}\n</ul>" ] )?>  
        
    </div>
</div>