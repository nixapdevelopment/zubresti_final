<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\bootstrap\Html;
use app\assets\ZubrestiFrontAssets;
use app\modules\Menu\components\MenuWidget\MenuWidget;
use yii\helpers\Url;
use app\modules\Settings\Settings;
use modernkernel\flagiconcss\Flag;
use app\models\Article\Article;

$bundle = ZubrestiFrontAssets::register ( $this );

$contact_page = Article::find ()->with ( [ 
		'lang',
		'link' 
] )->where ( [ 
		'ID' => 34 
] )->one ();

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon"
	href="<?= Url::to('@web/uploads/settings/' . Settings::getByName('favicon')) ?>"
	type="image/ico">
<title><?= Html::encode($this->title) ?></title>
<script>var SITE_URL = '<?= Url::home(true) ?>';</script>

<!-- Redirect to http://browsehappy.com/ on IE 8-  -->
<!--[if lte IE 8]>
    <style type="text/css">
        body{display:none!important;}
    </style>
    <meta http-equiv="refresh" content="0; url=http://browsehappy.com/">
    <![endif]-->
    
    <?php $this->head() ?>
</head>
<body>
 
<?php $this->beginBody() ?>
    
    <div class="uk-clearfix header home-header">
		<div class="uk-container-center uk-container">
			<div class="uk-grid">
				<div class="uk-width-1-2">
					<div class="left-box">
						<p>
                            <?= Settings::getByName('topBarText', true) ?>
                        </p>
						<ul>
                            <?php foreach (Yii::$app->params['siteLanguages'] as $lang) { ?>
                            <li><a
								href="<?= Url::current(['language' => $lang]) ?>">
                                    <?= $lang ?>
                                </a></li>
                            <?php } ?>
                        </ul>
					</div>
				</div>
				<div class="uk-width-1-2">
					<div class="right-box">
                        <?=MenuWidget::widget ( [ 'menuId' => 4 ] )?>                        
                    </div>
				</div>
			</div>
		</div>
	</div>

	<div class="uk-clearfix header-content home-header-content">
		<div class="uk-container-center uk-container">
			<div class="uk-grid">
				<div class="uk-width-medium-3-10">
					<div class="logo">
						<a href="<?= Url::to(['/']) ?>">
                            <?= Html::img('@web/uploads/settings/' . Settings::getByName('logo')) ?>
                        </a>
					</div>
				</div>
				<div class="uk-width-medium-7-10">
					<div class="other-box">
						<div class="uk-grid">
							<div class="uk-width-medium-4-10">
								<div class="call-box">
									<div class="uk-grid">
										<div class="uk-width-1-2">
											<div class="img">
                                                <?= Html::img($bundle->baseUrl . '/images/phone-call.png') ?>
                                            </div>
										</div>
										<div class="uk-width-1-2">
											<div class="link">
												<a href="tel:<?= Settings::getByName('phone') ?>">
                                                    <?= Settings::getByName('phone') ?>
                                                </a> <a
													href="mail:<?= Settings::getByName('email') ?>">
                                                    <?= Settings::getByName('email') ?>
                                                </a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-width-medium-4-10">
								<div class="adress-box">
									<div class="uk-grid">
										<div class="uk-width-1-2">
											<div class="img">
                                                <?= Html::img($bundle->baseUrl . '/images/placeholder.png') ?>
                                            </div>
										</div>
										<div class="uk-width-1-2">
											<div class="description">
                                                <?= Settings::getByName('address', true) ?>                                               
                                            </div>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-width-medium-2-10">
								<div class="search-box">
									<a href="#">
                                        <?= Html::img($bundle->baseUrl . '/images/loupe.png') ?>
                                    </a>
									<div class="search-input uk-animation-slide-top">
										<form action="<?= Url::to(['/search']) ?>" method="get">
											<div class="uk-grid">
												<label for="search"> <input id="search" type="text"
													name="search" required="required"
													value="<?= Yii::$app->request->get('search', '') ?>"
													placeholder="Cauta...">
												</label>
												<button type="submit" class="btn-submit">
													<i class="uk-icon-search"></i>
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="uk-clearfix header-nav home-header-nav">
		<div class="uk-container-center uk-container nav-bg">
			<div class="uk-grid padding-top-bottom">
				<div class="uk-width-1-10">
					<div class="home-link">
						<a href="<?= Url::to(['/']) ?>">
                            <?= Html::img($bundle->baseUrl . '/images/home.png') ?>
                        </a>
					</div>
				</div>
				<div class="uk-width-medium-7-10">
					<div class="nav">
						<div class="mobile">
							<a href="#mobile_menu" data-uk-offcanvas> <i
								class="uk-icon-navicon"></i>
							</a>
						</div>                       
                            
                        <?=MenuWidget::widget ( [ 'menuId' => 3,'submenuTemplate' => "<ul class='drop-down'>\n{items}\n</ul>" ] )?>
                            
                        
                    </div>
				</div>
				<div class="uk-width-medium-2-10">
					<div class="socio-link">
                        
                        <?= $this->render("social_links") ?>
                        
                    </div>
				</div>
			</div>
		</div>
	</div>
	<div class="title-section-big-box">
		<div class="uk-container-center uk-container uk-text-center">
			<span>
                <?= $this->title ?>
            </span>
			<h3>
                <?= $this->title ?>
            </h3>
		</div>
	</div>



	<div class="uk-clearfix our-posts">
		<div class="uk-container-center uk-container">
			<div class="uk-grid">
                   
                <?= $content ?>
                
                <div class="uk-width-medium-3-10 mt20">
                   
                    <?= $this->render("right_block") ?>
                    
                </div>
			</div>
		</div>
	</div>

	<div class="uk-clearfix footer footer-home">
		<div class="uk-container-center uk-container">
			<div class="uk-grid">
				<div class="uk-width-1-2">
					<div class="copyright">
						<p>
                            Copyright &copy; <?= date("Y") ?>  <span></span>
							<a href="http://nixap.com">Nixap</a>
						</p>
					</div>
				</div>
				<div class="uk-width-1-2">
					<div class="socio-link">
                        
                        <?= $this->render("social_links") ?>
                        
                    </div>
				</div>
			</div>
		</div>
	</div>
	</div>

<?= $this->render("mobile_menu") ?>
    

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
