<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\ZubrestiFrontAssets;
use app\models\Article\Article;

$bundle = ZubrestiFrontAssets::register ( $this );

$news_page = Article::findOne ( 9 );

?>

<div class="request-post">
	<div class="title-section">
		<h4>
            <?= Yii::t("app", "CERERI") ?>
        </h4>
	</div>
	<div class="request-box">
		<div class="img">
			<a href="<?= Url::to(['/ajutor-social']) ?>">
                <?= Html::img($bundle->baseUrl . '/images/Layer-5.png') ?>
            </a>
			<div class="description">
				<h5>
					<a href="<?= Url::to(['/ajutor-social']) ?>">
                        <?= Yii::t("app", "Ajutor Social") ?>
                    </a>
				</h5>
			</div>
		</div>
	</div>
</div>
<div class="request-post mt20">
	<div class="request-box">
		<div class="img">
			<a href="<?= Url::to(['/inregistrarea-casatorie']) ?>">
                <?= Html::img($bundle->baseUrl . '/images/Layer-6.png') ?>
            </a>
			<div class="description">
				<h5>
					<a href="<?= Url::to(['/inregistrarea-casatorie']) ?>">
                        <?= Yii::t("app", "Înregistrarea căsătorie") ?>
                    </a>
				</h5>
			</div>
		</div>
	</div>
</div>
<div class="request-post mt20">
	<div class="request-box">
		<div class="img">
			<a href="<?= Url::to(['/certificate']) ?>">
                <?= Html::img($bundle->baseUrl . '/images/Layer-7.png') ?>
            </a>
			<div class="description">
				<h5>
					<a href="<?= Url::to(['/certificate']) ?>">
                        <?= Yii::t("app", "Certificate") ?>
                    </a>
				</h5>
			</div>
		</div>
	</div>
	<br />
</div>

