<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\ZubrestiFrontAssets;
use app\models\Article\Article;
use yii\caching\TagDependency;

$bundle = ZubrestiFrontAssets::register ( $this );

$popularArticles = Article::getDb ()->cache ( function ($db) {
	return Article::find ()->with ( [ 
			'lang',
			'link' 
	] )->where ( [ 
			'Type' => [ 
					'News' 
			] 
	] )->orderBy ( 'Viewed DESC' )->limit ( 10 )->all ();
}, 600, new TagDependency ( [ 
		'tags' => 'article' 
] ) );

$anouncment = Article::getDb ()->cache ( function ($db) {
	return Article::find ()->with ( [ 
			'lang',
			'link' 
	] )->where ( [ 
			'Type' => 'Anouncment' 
	] )->orderBy ( 'Date DESC' )->limit ( 4 )->all ();
}, 600, new TagDependency ( [ 
		'tags' => 'article' 
] ) );

$news_page = Article::findOne ( 9 );
$anounce_page = Article::findOne ( 20 );

?>

<div class="request-post">
	<div class="title-section">
		<h4>
            <?= Yii::t("app", "CERERI") ?>
        </h4>
	</div>
	<div class="request-box">
		<div class="img">
			<a href="<?= Url::to(['/ajutor-social']) ?>">
                <?= Html::img($bundle->baseUrl . '/images/Layer-5.png') ?>
            </a>
			<div class="description">
				<h5>
					<a href="<?= Url::to(['/ajutor-social']) ?>">
                        <?= Yii::t("app", "Ajutor Social") ?>
                    </a>
				</h5>
			</div>
		</div>
	</div>
</div>
<div class="request-post mt20">
	<div class="request-box">
		<div class="img">
			<a href="<?= Url::to(['/inregistrarea-casatorie']) ?>">
                <?= Html::img($bundle->baseUrl . '/images/Layer-6.png') ?>
            </a>
			<div class="description">
				<h5>
					<a href="<?= Url::to(['/inregistrarea-casatorie']) ?>">
                        <?= Yii::t("app", "Înregistrarea căsătorie") ?>
                    </a>
				</h5>
			</div>
		</div>
	</div>
</div>
<div class="request-post mt20">
	<div class="request-box">
		<div class="img">
			<a href="<?= Url::to(['/certificate']) ?>">
                <?= Html::img($bundle->baseUrl . '/images/Layer-7.png') ?>
            </a>
			<div class="description">
				<h5>
					<a href="<?= Url::to(['/certificate']) ?>">
                        <?= Yii::t("app", "Certificate") ?>
                    </a>
				</h5>
			</div>
		</div>
	</div>
</div>
<div class="recent-popular-post mt20">
	<div class="title-section">
		<h4>
            <?= Yii::t("app", "CELE MAI POPULARE ȘTIRI") ?>
        </h4>
	</div>

    <?php if (count($popularArticles) > 0) { ?>

        <?php foreach ($popularArticles as $pa) { ?>
        <div class="popular-post-box">
		<div class="uk-grid">
			<div class="uk-width-medium-3-10">
				<div class="img">
					<a href="#"> <span>
                                <?= Html::img($pa->MainThumbUrl) ?>
                            </span>
					</a>
				</div>
			</div>
			<div class="uk-width-medium-7-10">
				<div class="description">
					<h4>
						<a href="<?= $pa->seoLink ?>">
                                <?= $pa->lang->Title ?>
                            </a>
					</h4>
					<a href="#"> <i class="uk-icon-calendar-o"></i> <span>
                            <?= date("d F Y", strtotime($pa->Date)) ?>
                        </span>
					</a>
				</div>
			</div>
		</div>
	</div>
        <?php } ?>

    <?php } ?>               

    <div class="link">
		<a href="<?= $news_page->seoLink ?>">
             <?= Yii::t("app", "Vezi toate știrile") ?>
        </a>
	</div>
</div>

