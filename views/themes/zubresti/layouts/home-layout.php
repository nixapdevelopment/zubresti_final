<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\bootstrap\Html;
use app\assets\ZubrestiFrontAssets;
use app\modules\Menu\components\MenuWidget\MenuWidget;
use yii\helpers\Url;
use app\modules\Settings\Settings;
use app\models\Article\Article;
use app\models\SliderItem\SliderItem;
use yii\caching\TagDependency;
use app\models\Gallery\Gallery;
use lesha724\youtubewidget\Youtube;

$bundle = ZubrestiFrontAssets::register ( $this );

$slides = SliderItem::getDb ()->cache ( function ($db) {
	return SliderItem::find ()->with ( 'lang' )->where ( [ 
			'SliderID' => 1 
	] )->all ();
}, 600, new TagDependency ( [ 
		'tags' => 'slider' 
] ) );

$miniSlides = SliderItem::getDb ()->cache ( function ($db) {
	return SliderItem::find ()->with ( 'lang' )->where ( [ 
			'SliderID' => 2 
	] )->all ();
}, 600, new TagDependency ( [ 
		'tags' => 'slider' 
] ) );

$lastArticles = Article::getDb ()->cache ( function ($db) {
	return Article::find ()->with ( [ 
			'lang',
			'link',
			'mainImage' 
	] )->where ( [ 
			'Type' => 'News',
			'Status' => 'Active' 
	] )->orderBy ( 'Date DESC' )->limit ( 2 )->offset ( 1 )->all ();
}, 600, new TagDependency ( [ 
		'tags' => 'article' 
] ) );

$lastAnounces = Article::getDb ()->cache ( function ($db) {
	return Article::find ()->with ( [ 
			'lang',
			'link',
			'mainImage' 
	] )->where ( [ 
			'Type' => 'Anouncment',
			'Status' => 'Active' 
	] )->orderBy ( 'Date DESC' )->limit ( 2 )->offset ( 1 )->all ();
}, 600, new TagDependency ( [ 
		'tags' => 'article' 
] ) );

$lastGalleries = Gallery::getDb ()->cache ( function ($db) {
	return Gallery::find ()->with ( [ 
			'lang',
			'items',
			'items.lang' 
	] )->orderBy ( 'Position' )->where ( [ 
			'Type' => Gallery::TypeDefault 
	] )->limit ( 3 )->all ();
}, 60 );

$lastVideoGaleries = Gallery::getDb ()->cache ( function ($db) {
	return Gallery::find ()->with ( [ 
			'lang',
			'items',
			'items.lang' 
	] )->orderBy ( 'Position' )->where ( [ 
			'Type' => Gallery::TypeVideo 
	] )->limit ( 3 )->all ();
}, 60 );

$lastArticle = Article::find ()->with ( [ 
		'lang',
		'link' 
] )->where ( [ 
		'Type' => 'News',
		'Status' => 'Active' 
] )->orderBy ( 'Date DESC' )->limit ( 1 )->one ();
$lastAnounce = Article::find ()->with ( [ 
		'lang',
		'link' 
] )->where ( [ 
		'Type' => 'Anouncment',
		'Status' => 'Active' 
] )->orderBy ( 'Date DESC' )->limit ( 1 )->one ();
$contact_page = Article::find ()->with ( [ 
		'lang',
		'link' 
] )->where ( [ 
		'ID' => 34 
] )->one ();
$home_page = Article::findOne ( 1 );
$video_page = Article::find ()->with ( [ 
		'lang',
		'link' 
] )->where ( [ 
		'ID' => 46 
] )->one ();
$news_page = Article::find ()->with ( [ 
		'lang',
		'link' 
] )->where ( [ 
		'ID' => 9 
] )->one ();

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
<meta charset="<?= Yii::$app->charset ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <link rel="shortcut icon"
	href="<?= Url::to('@web/uploads/settings/' . Settings::getByName('favicon')) ?>"
	type="image/ico">
<title><?= Html::encode($this->title) ?></title>
<script>var SITE_URL = '<?= Url::home(true) ?>';</script>

<!-- Redirect to http://browsehappy.com/ on IE 8-  -->
<!--[if lte IE 8]>
    <style type="text/css">
        body{display:none!important;}
    </style>
    <meta http-equiv="refresh" content="0; url=http://browsehappy.com/">
    <![endif]-->
    
    <?php $this->head() ?>
</head>
<body>
 
<?php $this->beginBody() ?>
    
    <?php foreach ($lastVideoGaleries as $vg) { ?>
    <div id="video-popup-<?= $vg->ID ?>" class="gallery-video mfp-hide">
		<p>
			<a class="popup-modal-dismiss2" href="#">X</a>
		</p>
		<div class="content">
			<div class="slider video_slider">
                <?php foreach ($vg->items as $vgItem) { ?>
                <div>
					<div class="title-slide">
						<h4>
                            <?= $vgItem->lang->Title ?>
                        </h4>
					</div>
                    <?=Youtube::widget ( [ 'video' => $vgItem->Value,'height' => 360,'width' => 640 ] )?>
                    <div class="description-slide">
						<p>
                            <?= $vgItem->lang->Title ?>
                        </p>
					</div>
				</div>
                <?php } ?>
            </div>
			<div class="slider video_nav">
                <?php foreach ($vg->items as $vgItem) { ?>
                <div>
					<span>
                        <?=Youtube::widget ( [ 'video' => $vgItem->Value,'height' => 360,'width' => 640 ] )?>
                    </span>
					<div class="description-slide">
						<p>
                            <?= $vgItem->lang->Title ?>
                        </p>
					</div>
				</div>
                <?php } ?>
            </div>
		</div>
	</div>
    <?php } ?>
    
    <div class="uk-clearfix header home-header">
		<div class="uk-container-center uk-container">
			<div class="uk-grid">
				<div class="uk-width-1-2">
					<div class="left-box">
						<p>
                            <?= Settings::getByName('topBarText', true) ?>
                        </p>
						<ul>
                            <?php foreach (Yii::$app->params['siteLanguages'] as $lang) { ?>
                            <li><a
								href="<?= Url::current(['language' => $lang]) ?>">
                                    <?= $lang ?>
                                </a></li>
                            <?php } ?>
                        </ul>
					</div>
				</div>
				<div class="uk-width-1-2">
					<div class="right-box">
                        <?=MenuWidget::widget ( [ 'menuId' => 4 ] )?>                        
                    </div>
				</div>
			</div>
		</div>
	</div>

	<div class="uk-clearfix header-content home-header-content">
		<div class="uk-container-center uk-container">
			<div class="uk-grid">
				<div class="uk-width-medium-3-10">
					<div class="logo">
						<a href="<?= Url::to(['/']) ?>">
                            <?= Html::img('@web/uploads/settings/' . Settings::getByName('logo')) ?>
                        </a>
					</div>
				</div>
				<div class="uk-width-medium-7-10">
					<div class="other-box">
						<div class="uk-grid">
							<div class="uk-width-medium-4-10">
								<div class="call-box">
									<div class="uk-grid">
										<div class="uk-width-1-2">
											<div class="img">
                                                <?= Html::img($bundle->baseUrl . '/images/phone-call.png') ?>
                                            </div>
										</div>
										<div class="uk-width-1-2">
											<div class="link">
												<a href="tel:<?= Settings::getByName('phone') ?>">
                                                    <?= Settings::getByName('phone') ?>
                                                </a> <a
													href="mail:<?= Settings::getByName('email') ?>">
                                                    <?= Settings::getByName('email') ?>
                                                </a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-width-medium-4-10">
								<div class="adress-box">
									<div class="uk-grid">
										<div class="uk-width-1-2">
											<div class="img">
                                                <?= Html::img($bundle->baseUrl . '/images/placeholder.png') ?>
                                            </div>
										</div>
										<div class="uk-width-1-2">
											<div class="description">
                                                <?= Settings::getByName('address', true) ?>                                               
                                            </div>
										</div>
									</div>
								</div>
							</div>
							<div class="uk-width-medium-2-10">
								<div class="search-box">
									<a href="#">
                                        <?= Html::img($bundle->baseUrl . '/images/loupe.png') ?>
                                    </a>
									<div class="search-input uk-animation-slide-top">
										<form action="<?= Url::to(['/search']) ?>" method="get">
											<div class="uk-grid">
												<label for="search"> <input id="search" type="text"
													name="search" required="required" placeholder="Cauta...">
												</label>
												<button type="submit" class="btn-submit">
													<i class="uk-icon-search"></i>
												</button>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="uk-clearfix header-nav home-header-nav">
		<div class="uk-container-center uk-container nav-bg">
			<div class="uk-grid padding-top-bottom">
				<div class="uk-width-1-10">
					<div class="home-link">
						<a href="<?= Url::to(['/']) ?>">
                            <?= Html::img($bundle->baseUrl . '/images/home.png') ?>
                        </a>
					</div>
				</div>
				<div class="uk-width-medium-7-10">
					<div class="nav">
						<div class="mobile">
							<a href="#mobile_menu" data-uk-offcanvas> <i
								class="uk-icon-navicon"></i>
							</a>
						</div>                       
                            
                        <?=MenuWidget::widget ( [ 'menuId' => 3,'submenuTemplate' => "<ul class='drop-down'>\n{items}\n</ul>" ] )?>
                            
                        
                    </div>
				</div>
				<div class="uk-width-medium-2-10">
					<div class="socio-link">
                        
                        <?= $this->render("social_links") ?>
                        
                    </div>
				</div>
			</div>
		</div>
	</div>
    
    <?php if (count($slides) > 0) { ?>
        <div class="uk-clearfix slider1">
		<div class="owl-carousel owl-theme">
                
                <?php foreach ($slides as $slide) { ?>
                <div class="item">
                    <?= Html::img($slide->imageUrl) ?>
                    <div
					class="uk-container-center uk-container caption">
					<div class="uk-grid">
						<div class="uk-width-medium-7-10">
							<h3>
                                    <?= $slide->lang->Title ?>
                                </h3>
						</div>
						<div class="uk-width-medium-3-10">
							<div class="button">
								<a class="more-btn" href="<?= $slide->Link ?>">
                                        <?= Yii::t("app", "vezi detalii") ?>
                                    </a>
							</div>
						</div>
					</div>
				</div>
			</div>
                <?php } ?>
                                   
            </div>
	</div>
    <?php } ?>
    
    <div class="uk-clearfix about-they">
		<div class="uk-container-center uk-container">
			<div class="uk-grid">
				<div class="uk-width-1-2">
                    
                    <?= $content ?>
                    
                </div>
				<div class="uk-width-1-2 mt60">
					<div class="uk-grid">
						<div class="uk-width-1-2">   

                            <?php if (count($miniSlides) > 0) { ?>        
                                <ul class="bxslider">
                                <?php foreach ($miniSlides as $mslide) { ?>
                                    <li>
                                        <?= Html::img($mslide->imageUrl) ?>
                                    </li>
                                <?php } ?>
                                </ul>
                            <?php } ?>

                        </div>
						<div class="uk-width-1-2">
							<div class="info-box">
								<div class="img-box-logo">
                                    <?= Html::img('@web/uploads/settings/' . Settings::getByName('contactLogo')) ?>
                                    <div class="logo">
                                        <?= Html::img('@web/uploads/settings/' . Settings::getByName('logo')) ?>
                                    </div>
									<div class="more-box">
										<a href="<?= $contact_page->seoLink ?>">
                                            <?= Yii::t("app", "Contactează-ne") ?>
                                        </a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="uk-clearfix our-posts home mt20">
		<div class="uk-container-center uk-container">
			<div class="uk-grid">

				<div class="uk-container-center uk-container">
					<div class="uk-grid">
						<div class="uk-width-medium-7-10">
                            
                            <?php if (count($lastArticles) > 0) { ?>
                            
                            <div class="uk-grid">
								<div class="uk-width-medium-7-10">
									<div class="title-section">
										<h4 class="mt5">
                                            <?= Yii::t("app", "Noutăți") ?>
                                        </h4>
									</div>
								</div>
								<div class="uk-width-medium-3-10">
									<div class="button">
										<a class="btn" href="<?= $news_page->seoLink ?>">
                                            <?= Yii::t("app", "Vezi toate noutățile") ?>
                                        </a>
									</div>
								</div>
							</div>

							<div class="news-post mt20">
								<div class="date-post">
									<span class="date">
                                        <?= date('d', strtotime($lastArticle->Date)) ?>
                                    </span> <span class="month">
                                        <?= date('F', strtotime($lastArticle->Date)) ?>
                                    </span> <span class="year">
                                        <?= date('Y', strtotime($lastArticle->Date)) ?>
                                    </span>
								</div>
								<div class="image-post">
									<a href="<?= $lastArticle->seoLink ?>">
                                        <?= Html::img($lastArticle->mainImageUrl) ?>
                                    </a>
								</div>
								<div class="title-post">
									<h4>
										<a href="<?= $lastArticle->seoLink ?>">
                                            <?= $lastArticle->lang->Title ?>
                                        </a>
									</h4>
								</div>
								<div class="description-post">
									<p>
                                        <?= $lastArticle->getShortText(300) ?>
                                    </p>
								</div>
							</div>

							<div class="uk-grid">
                                
                                <?php foreach ($lastArticles as $la) { ?>
                            
                                <div class="uk-width-1-2">
									<div class="news-post mt20">
										<div class="date-post">
											<span class="date">
                                                <?= date('d', strtotime($la->Date)) ?>
                                            </span> <span class="month">
                                                <?= date('F', strtotime($la->Date)) ?>
                                            </span> <span class="year">
                                                <?= date('Y', strtotime($la->Date)) ?>
                                            </span>
										</div>
										<div class="image-post">
											<a href="<?= $la->seoLink ?>">
                                                <?= Html::img($la->MainThumbUrl) ?>
                                            </a>
										</div>
										<div class="title-post">
											<h4>
												<a href="<?= $la->seoLink ?>">
                                                    <?= $la->lang->Title ?>
                                                </a>
											</h4>
										</div>
										<div class="description-post">
											<p>
                                                <?= $la->getShortText(300) ?>
                                            </p>
										</div>
									</div>
								</div>
                                
                                <?php } ?>
                                
                            </div>
                            
                            <?php } ?>
                            
                            
                            <?php if (count($lastAnounces) > 0) { ?>
                            
                            <div class="uk-grid">
								<div class="uk-width-medium-7-10">
									<div class="title-section">
										<h4>
                                            <?= Yii::t("app", "ANUNȚURI") ?>
                                        </h4>
									</div>
								</div>
								<div class="uk-width-medium-3-10">
									<div class="button">
										<a class="btn" href="<?= Url::to(['/anunturi']) ?>">
                                            <?= Yii::t("app", "Vezi toate anunțurile") ?>
                                        </a>
									</div>
								</div>
							</div>

							<div class="news-post mt30">
								<div class="title-post">
									<h4>
										<a href="<?= $lastAnounce->seoLink ?>">
                                            <?= $lastAnounce->lang->Title ?>
                                        </a>
									</h4>
								</div>
								<div class="description-post">
									<p>
                                        <?= $lastAnounce->getShortText(300) ?>
                                    </p>
								</div>
							</div>
							<div class="uk-grid">
                                
                                <?php foreach ($lastAnounces as $lan) { ?>
                                
                                <div class="uk-width-1-2">
									<div class="news-post mt30">
										<div class="title-post">
											<h4>
												<a href="<?= $lan->seoLink ?>">
                                                    <?= $lan->lang->Title ?>
                                                </a>
											</h4>
										</div>
										<div class="description-post">
											<p>
                                                <?= $lan->getShortText(300) ?>
                                            </p>
										</div>
									</div>
								</div>
                                
                                <?php } ?>
                                
                            </div>
                            
                            <?php } ?>
                        </div>
						<div class="uk-width-medium-3-10">
                            <?= $this->render("right_block") ?>
                        </div>
					</div>
					<div class="uk-grid photo-and-video">
						<div class="uk-width-1-2">
                            
                            <?php if (count($lastGalleries) > 0) { ?>
                            
                            <div class="uk-grid">
								<div class="uk-width-medium-7-10">
									<div class="title-section">
										<h4>
                                            <?= Yii::t("app", "Poze") ?>
                                        </h4>
									</div>
								</div>
								<div class="uk-width-medium-3-10">
									<div class="button">
										<a class="btn" href="<?= Url::to(['/galerie']) ?>">
                                            <?= Yii::t("app", "Vezi toate pozele") ?>
                                        </a>
									</div>
								</div>
                                
                                
                                
                                <?php foreach ($lastGalleries as $lg) { ?>
                                
                                <div class="uk-width-1-1 mt5">
									<div class="photo-galerry-box">
										<div class="img-bg">
                                            <?= Html::img('@web/uploads/gallery/' . $lg->Thumb) ?>
                                            <div class="content-box">
												<div class="uk-grid">
													<div class="uk-width-1-2">
														<h5>
															<a href="#photo-popup-<?= $lg->ID ?>" class="zoom">
                                                                <?= $lg->lang->Title ?>
                                                            </a>
														</h5>
														<span> <a href="#photo-popup-<?= $lg->ID ?>" class="zoom">
                                                                <?= $lg->CountItems ?> poze
                                                            </a>
														</span>
													</div>
													<div class="uk-width-1-2">
														<a href="#photo-popup-<?= $lg->ID ?>">
															<div class="icon">
																<a href="#photo-popup-<?= $lg->ID ?>" class="zoom">
                                                                    <?= Html::img($bundle->baseUrl . '/images/galerry-icon.png') ?>
                                                                </a>
															</div>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>

								<div id="photo-popup-<?= $lg->ID ?>"
									class="gallery-photo mfp-hide">
									<p>
										<a class="popup-modal-dismiss" href="#">X</a>
									</p>
									<div class="content">
										<div class="slider foto_slider">
                                        <?php foreach($lg->items as $item){ ?>        
                                            <div>
												<div class="title-slide">
													<h4>
                                                        <?= $item->lang->Title ?>
                                                    </h4>
												</div>
                                                <?= Html::img('@web/uploads/gallery/' . $item->Value) ?>
                                                <div
													class="description-slide">
													<p>
                                                        <?= $item->lang->Text ?>
                                                    </p>
												</div>
											</div>        
                                        <?php } ?>
                                        </div>
										<div class="slider foto_nav">
                                        <?php foreach($lg->items as $item){ ?>        
                                            <div>
												<span>
                                                    <?= Html::img('@web/uploads/gallery/' . $item->Thumb) ?>
                                                </span> <i
													class="uk-icon-eye"></i>
												<div class="description-slide">
													<p>
                                                        <?= $item->lang->Title ?>
                                                    </p>
												</div>
											</div>        
                                        <?php } ?>
                                        </div>
									</div>
								</div>
                                
                                <?php } ?>
                                
                                <div class="uk-width-1-1 mt20">
									<div class="button">
										<a class="btn" href="<?= Url::to(['/galerie']) ?>">
                                            <?= Yii::t("app", "Vezi toate pozele") ?>
                                        </a>
									</div>
								</div>
							</div>
                            
                            <?php } ?>
                            
                        </div>
						<div class="uk-width-1-2">
							<div class="uk-grid">
								<div class="uk-width-medium-7-10">
									<div class="title-section">
										<h4>
                                            <?= Yii::t("app", "Video") ?>
                                        </h4>
									</div>
								</div>
								<div class="uk-width-medium-3-10">
									<div class="button">
										<a class="btn" href="<?= $video_page->seoLink ?>">
                                           <?= Yii::t("app", "Vezi toate video") ?> 
                                        </a>
									</div>
								</div>
                                <?php foreach ($lastVideoGaleries as $vg) { ?>
                                <div class="uk-width-1-1 mt20">
									<div class="photo-galerry-box video">
										<div class="img-bg">
                                            <?= Html::img($vg->Thumb) ?>
                                            <div class="content-box">
												<div class="uk-grid">
													<div class="uk-width-1-2">
														<h5>
															<a href="#video-popup-<?= $vg->ID ?>" class="zoom2">
                                                                <?= $vg->lang->Title ?>
                                                            </a>
														</h5>
														<span> <a href="#video-popup-<?= $vg->ID ?>" class="zoom2">
                                                                <?= $vg->countItems ?> video
                                                            </a>
														</span>
													</div>
													<div class="uk-width-1-2">
														<a href="#video-popup-<?= $vg->ID ?>" class="zoom2">
															<div class="icon">
                                                                <?= Html::img($bundle->baseUrl . '/images/play.png') ?>
                                                            </div>
														</a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
                                <?php } ?>
                                <div class="uk-width-1-1 mt20">
									<div class="button">
										<a class="btn" href="<?= $video_page->seoLink ?>">
                                           <?= Yii::t("app", "Vezi toate video") ?>
                                        </a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>


	<div class="uk-clearfix footer footer-home">
		<div class="uk-container-center uk-container">
			<div class="uk-grid">
				<div class="uk-width-1-2">
					<div class="copyright">
						<p>
                            Copyright &copy; <?= date("Y") ?>  <span></span>
							<a href="http://nixap.com">Nixap</a>
						</p>
					</div>
				</div>
				<div class="uk-width-1-2">
					<div class="socio-link">
                        
                        <?= $this->render("social_links") ?>
                        
                    </div>
				</div>
			</div>
		</div>
	</div>
	</div>

<?= $this->render("mobile_menu") ?>   

<?= $this->registerJs("$('.owl-carousel').owlCarousel({items:1,nav:true,loop: true,dots: true,autoplay: 7000,navText: ['".Html::img($bundle->baseUrl . '/images/chevron-left.png') ."','".Html::img($bundle->baseUrl . '/images/chevron-right.png') ."']});") ?> 

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
