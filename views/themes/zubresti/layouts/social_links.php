<?php
use yii\helpers\Html;
use app\assets\ZubrestiFrontAssets;
use app\modules\Settings\Settings;

$bundle = ZubrestiFrontAssets::register ( $this );

?>


<?php if (Settings::getByName('facebookLink')) { ?>
<a target="_blank" href="<?= Settings::getByName('facebookLink') ?>">
    <?= Html::img($bundle->baseUrl . '/images/facebook-(1).png') ?>
</a>
<?php } ?>
<?php if (Settings::getByName('okLink')) { ?>
<a target="_blank" href="<?= Settings::getByName('okLink') ?>">
    <?= Html::img($bundle->baseUrl . '/images/odnoklassniki-logo-(1).png') ?>
</a>
<?php } ?>
<?php if (Settings::getByName('instagramLink')) { ?>
<a target="_blank" href="<?= Settings::getByName('instagramLink') ?>">
    <?= Html::img($bundle->baseUrl . '/images/instagram.png') ?>
</a>
<?php } ?>
<?php if (Settings::getByName('ytLink')) { ?>
<a target="_blank" href="<?= Settings::getByName('ytLink') ?>">
    <?= Html::img($bundle->baseUrl . '/images/youtube.png') ?>
</a>
<?php } ?>