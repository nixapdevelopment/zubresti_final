<?php
use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Article\Article;
use yii\data\ActiveDataProvider;
use bigpaulie\social\share\Share;

$article->Viewed = $article->Viewed + 1;
$article->save ( false );

$query = Article::getDb ()->cache ( function ($db) {
	return Article::find ()->with ( [ 
			'lang',
			'link' 
	] )->where ( [ 
			'Type' => 'News',
			'Status' => 'Active' 
	] )->orderBy ( 'RAND()' )->distinct ()->limit ( 2 );
}, 60 );

$dataProvider = new ActiveDataProvider ( [ 
		'query' => $query,
		'pagination' => false 
] );

?>

<div class="uk-width-medium-7-10 news-page noutate">
	<div class="news-post mt20">
		<div class="date-post">
			<span class="date">
                                <?= date("d", strtotime($article->Date)) ?>
                            </span> <span class="month">
                                <?= date("F", strtotime($article->Date)) ?>
                            </span> <span class="year">
                                <?= date("Y", strtotime($article->Date)) ?>
                            </span>
		</div>
		<div class="image-post">
                            <?=yii\helpers\Html::img ( $article->MainImageUrl, [ 'class' => 'img-responsive' ] )?>
                        </div>
		<div class="title-post">
			<h4>
				<a>
                                    <?= $article->lang->Title ?>
                                </a>
			</h4>
		</div>
		<div class="description-post">
                            
                            <?= $article->lang->Text ?> 
                                                        
                        </div>
		<div>
                            <?=ArticleImageWidget::widget ( [ 'article' => $article ] )?>
                            <?=ArticleFileWidget::widget ( [ 'article' => $article ] )?>
                        </div>
		<div class="uk-width-1-1 mt50 mb50">
			<div class="title-of-socio-icon">
				<h4>
                                    <?= Yii::t("app", "Distribuie mai departe") ?>
                                </h4>
			</div>
			<div class="uk-clearfix share-social-icons">
                                <?=Share::widget ( [ 'type' => 'small','tag' => false,'template' => '{button}' ] );?>
                            </div>

		</div>
	</div>
	<div class="uk-width-1-1">
		<div class="title-section">
			<h4>
                                <?= Yii::t("app", "Alte Noutăți") ?>
                            </h4>
		</div>
	</div>
	<div class="uk-grid">
                        
                        <?php Pjax::begin(['options' => ['tag'=>false]]); ?>
                            <?=ListView::widget ( [ 'dataProvider' => $dataProvider,'itemView' => 'news_list_item_other','layout' => '{items}','options' => [ 'tag' => false ],'itemOptions' => [ 'tag' => false ] ] );?>
                        <?php Pjax::end(); ?>                          
                        
                    </div>
</div>