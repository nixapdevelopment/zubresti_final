<div class="uk-width-1-1">
	<div class="news-post mt20">
		<div class="title-post">
			<h4>
				<a data-pjax="0" style="padding: 20px;"
					href="<?= $model->seoLink ?>">
                    <?= $model->lang->Title ?>
                </a>
			</h4>
		</div>
		<div class="description-post">
			<p>
                <?= $model->getShortText(300) ?>... 
                <a data-pjax="0" href="<?= $model->seoLink ?>"><?= Yii::t('app', 'Mai mult') ?> >>></a>
			</p>
		</div>
	</div>
</div>
<hr />