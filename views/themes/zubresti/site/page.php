<?php
use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
use app\modules\Article\components\ArticleFileWidget\ArticleFileWidget;

$this->title = $article->lang->Title;

?>

<div class="uk-width-medium-7-10 news-page mt20">
	<div>
        <?= $article->lang->Text ?>
    </div>
    <?=ArticleImageWidget::widget ( [ 'article' => $article ] )?>
    <?=ArticleFileWidget::widget ( [ 'article' => $article,'useDateFilter' => true ] )?>
</div>