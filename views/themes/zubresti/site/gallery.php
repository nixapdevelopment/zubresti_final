<?php
$this->title = $article->lang->Title;

use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Gallery\Gallery;
use yii\data\ActiveDataProvider;
use app\modules\Settings\Settings;

$articlesPerPage = Settings::getByName('articlesPerPage');

$query = Gallery::getDb()->cache(function ($db) {
    return Gallery::find()->with([
                'lang',
                'items'
            ])->where([
                'Type' => Gallery::TypeDefault
            ])->orderBy('Position');
}, 60);

$dataProvider = new ActiveDataProvider([
    'query' => $query,
    'pagination' => [
        'pageSize' => 9
    ]
        ]);
?>

<div class="uk-width-medium-7-10 news-page noutate anunt photos mt20">
    <div class="uk-grid bottom-posts">

        <?php Pjax::begin(['options' => ['tag' => false]]); ?>
        <?=
        ListView::widget(['dataProvider' => $dataProvider, 'itemView' => function ($model, $key, $index, $widget) {
                if ($index == 0) {
                    return $this->render('gallery_list_item_first', ['model' => $model]);
                }return $this->render('gallery_list_item', ['model' => $model]);
            }, 'layout' => '{items}<div class="uk-width-1-1"><div class="pagination uk-clearfix">{pager}</div></div>', 'options' => ['tag' => false], 'itemOptions' => ['tag' => false]]);
        ?>
        <?php Pjax::end(); ?>   

    </div>
</div>


