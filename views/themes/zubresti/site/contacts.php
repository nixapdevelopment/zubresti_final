<?php
use app\modules\Feedback\components\FeedbackWidget\FeedbackWidget;
use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
use app\modules\Settings\Settings;

$this->title = $article->lang->Title;
$this->context->layout = 'contact-layout';

?>

<div class="uk-width-medium-7-10 news-page contacts-page mt20">
	<div class="title-section">
		<h4 class="mt5">
           <?= Yii::t("app", "Contactează-ne") ?> 
        </h4>
	</div>
	<div class="description">
		<p>
            <?= $article->lang->Text ?>
        </p>
	</div>
	<div class="form-contacts">
        <?= FeedbackWidget::widget() ?>
    </div>
	<div class="uk-hidden">
		<br />
    <?= Settings::getByName('address', true) ?>
    <hr />
    <?= Settings::getByName('grafic', true) ?>
    <hr />
    <?= Settings::getByName('map') ?>
    <hr />
	</div>
    <?=ArticleImageWidget::widget ( [ 'article' => $article ] )?>
</div>