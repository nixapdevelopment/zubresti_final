<?php
$this->title = $article->lang->Title;

use yii\bootstrap\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Article\Article;
use yii\data\ActiveDataProvider;
use app\modules\Settings\Settings;

$query = Article::getDb ()->cache ( function ($db) {
	return Article::find ()->with ( [ 
			'lang',
			'link' 
	] )->where ( [ 
			'Type' => 'News',
			'Status' => 'Active' 
	] )->orderBy ( 'Date DESC' );
}, 60 );

$articlesPerPage = Settings::getByName ( 'articlesPerPage' );

$dataProvider = new ActiveDataProvider ( [ 
		'query' => $query,
		'pagination' => [ 
				'pageSize' => $articlesPerPage 
		] 
] );

?>


<div class="uk-width-medium-7-10 news-page">
	<div class="uk-grid">

        <?php Pjax::begin(['options' => ['tag'=>false]]); ?>
            <?=ListView::widget ( [ 'dataProvider' => $dataProvider,'itemView' => function ($model, $key, $index, $widget) {if ($index == 0) {return $this->render ( 'news_list_item_first', [ 'model' => $model ] );}return $this->render ( 'news_list_item', [ 'model' => $model ] );},'layout' => '{items}<div class="uk-width-1-1"><div class="pagination uk-clearfix">{pager}</div></div>','options' => [ 'tag' => false ],'itemOptions' => [ 'tag' => false ] ] );?>
        <?php Pjax::end(); ?>   

    </div>
</div>
