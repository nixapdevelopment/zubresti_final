<?php
$this->title = $article->lang->Title;

use yii\bootstrap\Html;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use app\models\Article\Article;
use yii\data\ActiveDataProvider;
use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
use app\modules\Settings\Settings;

$articlesPerPage = Settings::getByName ( 'articlesPerPage' );

$query = Article::getDb ()->cache ( function ($db) {
	return Article::find ()->with ( [ 
			'lang',
			'link' 
	] )->where ( [ 
			'Type' => 'Anouncment',
			'Status' => 'Active' 
	] )->orderBy ( 'Date DESC' );
}, 60 );

$dataProvider = new ActiveDataProvider ( [ 
		'query' => $query,
		'pagination' => [ 
				'pageSize' => $articlesPerPage 
		] 
] );

?>


<div class="uk-width-medium-7-10 news-page noutate anunt mt20">
	<div class="uk-grid bottom-posts">

        <?php Pjax::begin(['options' => ['tag'=>false]]); ?>
            <?=ListView::widget ( [ 'dataProvider' => $dataProvider,'itemView' => function ($model, $key, $index, $widget) {if ($index == 0) {return $this->render ( 'anouncment_list_item_first', [ 'model' => $model ] );}return $this->render ( 'anouncment_list_item', [ 'model' => $model ] );},'layout' => '{items}<div class="uk-width-1-1"><div class="pagination uk-clearfix">{pager}</div></div>','options' => [ 'tag' => false ],'itemOptions' => [ 'tag' => false ] ] );?>
        <?php Pjax::end(); ?>                       


    </div>
</div>
