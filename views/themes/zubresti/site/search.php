<?php
use yii\widgets\Pjax;
use yii\widgets\ListView;

$this->title = Yii::t ( 'app', 'Căutare' );

?>

<div class="uk-width-medium-7-10 mt20">
    <?php Pjax::begin(); ?>
        <?=ListView::widget ( [ 'dataProvider' => $dataProvider,'itemView' => 'search_list_item','layout' => "{summary}\n{items}\n<div class=\"clearfix\"></div><div>{pager}</div><br /><br /><br /><br />" ] );?>
    <?php Pjax::end(); ?>
</div>