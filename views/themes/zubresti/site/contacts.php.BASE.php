<?php
use app\modules\Feedback\components\FeedbackWidget\FeedbackWidget;
use app\modules\Article\components\ArticleImageWidget\ArticleImageWidget;
use app\modules\Settings\Settings;

$this->title = $article->lang->Title;

?>

<div class="uk-width-medium-7-10 news-page contacts-page mt20">
	<div class="title-section">
		<h4 class="mt5">Contactează-ne</h4>
	</div>
	<div class="description">
		<p>
            <?= $article->lang->Text ?>
        </p>
	</div>
	<div class="form-contacts">
		<form action="#" method="POST">
			<div class="uk-grid">
				<div class="uk-width-1-2">
					<label for="fname"> <input type="text" name="name" id="fname"
						required="required" placeholder="Nume">
					</label> <label for="phone"> <input type="text" name="phone"
						id="phone" required="required" placeholder="Număr de telefon">
					</label> <label for="email"> <input type="text" name="email"
						id="email" required="required" placeholder="E-mail">
					</label>
				</div>
				<div class="uk-width-1-2">
					<label for="subject"> <input type="text" name="subject"
						id="subject" required="required" placeholder="Subiect">
					</label>
					<textarea name="mesage" id="mesage" placeholder="Mesaj"></textarea>
				</div>
			</div>
			<div class="uk-width-1-1">
				<div class="button">
					<a class="btn" href="#"> trimite </a>
				</div>
			</div>
		</form>
	</div>
</div>

<div class="container uk-hidden">
	<h1><?= $article->lang->Title ?></h1>

	<div class="row">
		<div class="col-md-6">
            <?= $article->lang->Text ?>
            <hr />
            <?= FeedbackWidget::widget() ?>
            <hr />
            <?= Settings::getByName('address', true) ?>
            <hr />
            <?= Settings::getByName('grafic', true) ?>
        </div>
		<div class="col-md-6">
            <?= Settings::getByName('map') ?>
        </div>
	</div>
    
    <?=ArticleImageWidget::widget ( [ 'article' => $article ] )?>
    
</div>