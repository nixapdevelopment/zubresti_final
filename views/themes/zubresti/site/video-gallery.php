<?php

use app\models\Gallery\Gallery;
use yii\helpers\Html;
use app\assets\ZubrestiFrontAssets;
use lesha724\youtubewidget\Youtube;

$this->title = $article->lang->Title;

$bundle = ZubrestiFrontAssets::register($this);

$videoGaleries = Gallery::getDb()->cache(function ($db) {
    return Gallery::find()->with([
                'lang',
                'items',
                'items.lang'
            ])->orderBy('Position')->where([
                'Type' => Gallery::TypeVideo
            ])->all();
}, 60);
?>

<div class="uk-width-medium-7-10 mt20">
    <div>
        <?= $article->lang->Text ?>
    </div>
    <div class="uk-grid photo-and-video">
        <?php foreach ($videoGaleries as $vg) { ?>
            <div class="uk-width-1-2 mt20">
                <div class="photo-galerry-box video">
                    <div class="img-bg">
                        <?= Html::img($vg->Thumb) ?>
                        <div class="content-box">
                            <div class="uk-grid">
                                <div class="uk-width-1-2">
                                    <h5>
                                        <a href="#video-popup-<?= $vg->ID ?>" class="zoom2">
                                            <?= $vg->lang->Title ?>
                                        </a>
                                    </h5>
                                    <span> <a href="#video-popup-<?= $vg->ID ?>" class="zoom2">
                                            <?= $vg->countItems ?> video
                                        </a>
                                    </span>
                                </div>
                                <div class="uk-width-1-2">
                                    <a href="#video-popup-<?= $vg->ID ?>" class="zoom2">
                                        <div class="icon">
                                            <?= Html::img($bundle->baseUrl . '/images/play.png') ?>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<?php foreach ($videoGaleries as $vg) { ?>
    <div id="video-popup-<?= $vg->ID ?>" class="gallery-video mfp-hide">
        <p>
            <a class="popup-modal-dismiss2" href="#">X</a>
        </p>
        <div class="content">
            <div class="slider video_slider">
                <?php foreach ($vg->items as $vgItem) { ?>
                    <div>
                        <div class="title-slide">
                            <h4>
                                <?= $vgItem->lang->Title ?>
                            </h4>
                        </div>
                        <?= Youtube::widget(['video' => $vgItem->Value, 'height' => 360, 'width' => 640]) ?>
                        <div class="description-slide">
                            <p>
                                <?= $vgItem->lang->Title ?>
                            </p>
                        </div>
                    </div>
                <?php } ?>
            </div>
            <div class="slider video_nav">
                <?php foreach ($vg->items as $vgItem) { ?>
                    <div>
                        <span>
                            <?= Youtube::widget(['video' => $vgItem->Value, 'height' => 360, 'width' => 640]) ?>
                        </span>
                        <div class="description-slide">
                            <p>
                                <?= $vgItem->lang->Title ?>
                            </p>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
<?php } ?>