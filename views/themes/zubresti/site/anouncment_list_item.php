<div class="uk-width-1-2" data-pjax="0">
	<div class="news-post mt20">
		<div class="date-post">
			<span class="date">
            <?= date("d", strtotime($model->Date)) ?>
        </span> <span class="month">
            <?= date("F", strtotime($model->Date)) ?>
        </span> <span class="year">
            <?= date("Y", strtotime($model->Date)) ?>
        </span>
		</div>
		<div class="title-post">
			<h4>
				<a href="<?= $model->seoLink ?>">
                    <?= $model->lang->Title ?>
                </a>
			</h4>
		</div>
		<div class="description-post">
			<p>
                <?= $model->getShortText(300) ?>...
            </p>
		</div>
	</div>
</div>