<?php

use app\assets\ZubrestiFrontAssets;

$bundle = ZubrestiFrontAssets::register($this);
?>
<div class="uk-width-1-2" data-pjax="0">
    <div class="news-post mt20">
        <div class="images-post">
            <a href="#photo-popup-<?= $model->ID ?>" class="zoom">
                <?= yii\helpers\Html::img('@web/uploads/gallery/' . $model->Thumb) ?>
            </a> <a href="#photo-popup-<?= $model->ID ?>" class="zoom">
                <div class="icon">
                    <?= yii\helpers\Html::img($bundle->baseUrl . '/images/galerry-icon.png') ?>
                </div>
            </a>
            <div class="number-photo">
                <span> <a href="#photo-popup-<?= $model->ID ?>" class="zoom">
                        <?= $model->CountItems ?> <?= Yii::t("app", "poze") ?>
                    </a>
                </span>
            </div>
        </div>
        <div class="title-post">
            <h4>
                <a href="#photo-popup-<?= $model->ID ?>" class="zoom">
                    <?= $model->lang->Title ?>
                </a>
            </h4>
        </div>
    </div>
</div>

<div id="photo-popup-<?= $model->ID ?>" class="gallery-photo mfp-hide">
    <p>
        <a class="popup-modal-dismiss" href="#">X</a>
    </p>
    <div class="content">
        <div class="slider foto_slider">
            <?php foreach ($model->items as $item) { ?>        
                <div>
                    <div class="title-slide">
                        <h4>
                            <?= $item->lang->Title ?>
                        </h4>
                    </div>
                    <?= yii\helpers\Html::img('@web/uploads/gallery/' . $item->Value) ?>
                    <div class="description-slide">
                        <p>
                            <?= $item->lang->Text ?>
                        </p>
                    </div>
                </div>        
            <?php } ?>
        </div>
        <div class="slider foto_nav">
            <?php foreach ($model->items as $item) { ?>        
                <div>
                    <span>
                        <?= yii\helpers\Html::img('@web/uploads/gallery/' . $item->Thumb) ?>
                    </span> <i class="uk-icon-eye"></i>
                    <div class="description-slide">
                        <p>
                            <?= $item->lang->Title ?>
                        </p>
                    </div>
                </div>        
            <?php } ?>
        </div>
    </div>
</div>