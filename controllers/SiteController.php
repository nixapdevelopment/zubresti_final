<?php

namespace app\controllers;

use Yii;
use app\controllers\FrontController;
use app\models\Article\Article;
use app\models\Link\Link;
use yii\data\ActiveDataProvider;

class SiteController extends FrontController {
	public $layout = 'frontend';
	public function actionIndex($route) {
		return $this->resolveRoute ( $route );
	}
	public function resolveRoute($route) {
		$segments = explode ( '/', $route );
		$segments = array_filter ( $segments );
		
		$current_segment = end ( $segments );
		
		if ($current_segment) {
			$item = Link::find ()->with ( [ 
					'article',
					'article.lang' 
			] )->where ( [ 
					'Link' => $current_segment 
			] )->one ();
			
			if (! empty ( $item->article ) && $item->article != NULL) {
				$article = $item->article;
				$this->view->title = $article->lang->Title;
				
				if (empty ( $article->Template )) {
					$view = strtolower ( $article->Type );
				} else {
					$view = strtolower ( $article->Template );
				}
				
				return $this->render ( $view, [ 
						'article' => $article 
				] );
			}
		} else {
			$article = Article::find ()->with ( [ 
					'lang' 
			] )->where ( [ 
					'ID' => 8 
			] )->one ();
			
			$this->layout = "home-layout";
			
			$this->view->title = $article->lang->Title;
			
			return $this->render ( 'home', [ 
					'article' => $article 
			] );
		}
		
		return $this->actionError ();
	}
	public function actionError() {
		return $this->render ( 'error', [ 
				'message' => 'Page not found' 
		] );
	}
	public function actionSearch() {
		$search = Yii::$app->request->get ( 'search' );
		
		$query = Article::find ()->joinWith ( [ 
				'lang',
				'link' 
		] )->filterWhere ( [ 
				'like',
				'Title',
				$search 
		] )->orFilterWhere ( [ 
				'like',
				'Text',
				$search 
		] )->indexBy ( 'ID' );
		
		$dataProvider = new ActiveDataProvider ( [ 
				'query' => $query,
				'pagination' => [ 
						'pageSize' => 10 
				] 
		] );
		
		return $this->render ( 'search', [ 
				'dataProvider' => $dataProvider 
		] );
	}
}