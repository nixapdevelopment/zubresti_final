<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class BackendController extends Controller {
	public function beforeAction($action) {
		$this->checkPermissions ();
		
		return parent::beforeAction ( $action );
	}
	public function init() {
		parent::init ();
		
		// check user auth
		if (Yii::$app->user->isGuest) {
			$this->redirect ( [ 
					'/admin/user/login' 
			] );
		}
		
		Yii::$container->set ( 'yii\widgets\Pjax', [ 
				'timeout' => 15000 
		] );
	}
	public function actionIndex() {
		return $this->actionLogin ();
	}
	public function permissions() {
		return [ ];
	}
	public function checkPermissions() {
		$controllerPermissions = $this->permissions ();
		
		if (count ( $controllerPermissions ) > 0) {
			$action = Yii::$app->controller->action->id;
			
			if (isset ( $controllerPermissions [$action] )) {
				$userPermissions = Yii::$app->user->identity->permissionsList;
				
				if (! empty ( $userPermissions )) {
					if (! in_array ( $controllerPermissions [$action] ['permission'], $userPermissions )) {
						throw new ForbiddenHttpException ( 'Access denied' );
					}
				}
			}
		}
	}
}